﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Utilities
{
    public static class RandomUtilities
    {
        private static Random random = new Random();

        public static double GetNextChanceRoll()
        {
            return random.NextDouble() * 100;
        }
    }
}
