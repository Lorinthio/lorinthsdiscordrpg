﻿using Discord;
using Discord.WebSocket;
using LorinthsRpgDiscord.Bot.Cache.Interfaces;
using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Bot.Messages;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LorinthsRpgDiscord.Discord.Interfaces;

namespace LorinthsRpgDiscord.Bot.MessageHandlers.DirectMessage
{
    public class CreateCharacterMessageHandler : ICreateCharacterMessageHandler
    {
        private readonly ICharacterCreationCache _characterCreationCache;
        private readonly ICharacterService _characterService;
        private readonly ICharacterDiscordService _characterDiscordService;
        private readonly IClassService _classService;
        private readonly IInventoryService _inventoryService;
        private readonly ILocationService _locationService;
        private readonly IProfessionService _professionService;
        private readonly IRaceService _raceService;

        public CreateCharacterMessageHandler(
            ICharacterCreationCache characterCreationCache,
            ICharacterService characterService,
            ICharacterDiscordService characterDiscordService,
            IClassService classService,
            IInventoryService inventoryService,
            ILocationService locationService,
            IProfessionService professionService,
            IRaceService raceService)
        {
            _characterCreationCache = characterCreationCache;
            _characterService = characterService;
            _characterDiscordService = characterDiscordService;
            _classService = classService;
            _inventoryService = inventoryService;
            _locationService = locationService;
            _professionService = professionService;
            _raceService = raceService;
        }

        public async Task MessageReceived(SocketMessage socketMessage, Player player)
        {
            if (socketMessage.Author.IsBot)
            {
                return;
            }

            if(player == null)
            {
                return;
            }

            var user = socketMessage.Author;

            switch (await _characterCreationCache.GetCreationState(player))
            {
                case CreationStateEnum.Name:
                    await SetName(socketMessage, user, player);
                    break;
                case CreationStateEnum.Race:
                    await SetRace(socketMessage, user, player);
                    break;
                case CreationStateEnum.Class:
                    await SetClass(socketMessage, user, player);
                    break;
                case CreationStateEnum.CraftProfession:
                    await SetCraftingProfession(socketMessage, user, player);
                    break;
                case CreationStateEnum.GatherProfession:
                    await SetGatherProfession(socketMessage, user, player);
                    break;
                case CreationStateEnum.Confirmation:
                    await Confirm(socketMessage, user, player);
                    break;
                default:
                    await Task.Run(() =>
                    {
                        Console.WriteLine($"DM: {socketMessage.Author} -> {socketMessage.Content}");
                    });
                    break;
            }

            
        }

        private async Task SetName(SocketMessage socketMessage, SocketUser user, Player player)
        {
            var character = await _characterService.GetCharacter(player.CurrentCharacterId.Value);
            character.CharacterName = socketMessage.Content;
            await _characterService.UpdateCharacter(character);
            await SendRaceOptions(user, player);
        }

        #region Race Selection
        private async Task SendRaceOptions(SocketUser user, Player player)
        {
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.Race);
            await user.SendMessageAsync(await BuildRaceOptions());
            await user.SendMessageAsync(CreationMessages.RaceSelectionHelp);
        }

        private async Task<string> BuildRaceOptions()
        {
            string options = @"
__Race Options__
";
            var races = await _raceService.GetAll();
            foreach (Race r in races)
            {
                options += $@"
__{r.Name}__:
  {r.ShortDescription}
";
            }

            return options;
        }

        private async Task SetRace(SocketMessage socketMessage, SocketUser user, Player player)
        {
            var content = socketMessage.Content;
            if(await GetRaceInfo(content, user))
            {
                return;
            }

            var raceOptionId = await GetRaceOption(content);
            if(raceOptionId == null)
            {
                await user.SendMessageAsync(CreationMessages.RaceSelectionHelp);
                return;
            }

            var race = await _raceService.Get(raceOptionId.Value);
            var attributes = player.CurrentCharacter.CoreAttributes != null ?
                player.CurrentCharacter.CoreAttributes :
                new CoreAttributes();

            attributes.Strength = race.StartingCoreAttributes.Strength;
            attributes.Endurance = race.StartingCoreAttributes.Endurance;
            attributes.Dexterity = race.StartingCoreAttributes.Dexterity;
            attributes.Intelligence = race.StartingCoreAttributes.Intelligence;
            attributes.Charisma = race.StartingCoreAttributes.Charisma;

            player.CurrentCharacter.RaceId = raceOptionId.Value;
            player.CurrentCharacter.CoreAttributes = attributes;
            player.CurrentCharacter.CurrentLocationId = race.RaceStartingLocation.FirstOrDefault().LocationId;
            player.CurrentCharacter.CurrentSubLocationId = race.RaceStartingLocation.FirstOrDefault().Location.SubLocation.FirstOrDefault(sl => sl.IsStartingSubLocation)?.Id;

            await _characterService.UpdateCharacter(player.CurrentCharacter);
            await SendClassOptions(user, player);
        }

        private async Task<bool> GetRaceInfo(string message, SocketUser user)
        {
            message = message.ToLower();
            int? raceId;
            if (message.Contains("info"))
            {
                raceId = await GetRaceOption(message);
            }
            else
            {
                return false;
            }

            Race race = await _raceService.Get((int) raceId);
            await user.SendMessageAsync($@"
__{race.Name} Info__
{race.LongDescription}

__Starting Attributes__
{race.StartingCoreAttributes}
");

            return true;
        }

        private async Task<int?> GetRaceOption(string message)
        {
            var races = await _raceService.GetAll();
            message = message.ToLower();

            foreach (string word in message.Split(" "))
            {
                foreach (var r in races)
                {
                    if (r.Name.ToLower().StartsWith(word))
                    {
                        return r.Id;
                    }
                }
            }

            return null;
        }
        #endregion

        #region Class Selection
        private async Task SetClass(SocketMessage socketMessage, SocketUser user, Player player)
        {
            var content = socketMessage.Content;
            if (await GetClassInfo(content, user, player))
            {
                return;
            }

            int? classOption = await GetClassOption(content, player.CurrentCharacter.RaceId);
            if (classOption == null)
            {
                await user.SendMessageAsync(CreationMessages.ClassSelectionHelp);
                return;
            }

            player.CurrentCharacter.ClassId = (int) classOption.Value;
            await _characterService.UpdateCharacter(player.CurrentCharacter);
            await SendCraftOptions(user, player);
        }

        private async Task SendClassOptions(SocketUser user, Player player)
        {
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.Class);
            await user.SendMessageAsync(await BuildClassOptions(player.CurrentCharacter.RaceId));
            await user.SendMessageAsync(CreationMessages.ClassSelectionHelp);
        }

        private async Task<string> BuildClassOptions(int raceId)
        {
            string options = @"
__Class Options__
";
            var classes = await _raceService.GetRaceAllowedClasses(raceId);
            foreach (Class c in classes)
            {
                options += $@"
__{c.Name}__
  {c.ShortDescription}
";
            }

            return options;
        }

        private async Task<bool> GetClassInfo(string message, SocketUser user, Player player)
        {
            message = message.ToLower();
            int? classId = null;
            if (message.Contains("info"))
            {
                classId = await GetClassOption(message, player.CurrentCharacter.RaceId);
            }
            else
            {
                return false;
            }

            Class @class = await _classService.Get((int)classId);
            await user.SendMessageAsync($@"
__{@class.Name} Info__
{@class.LongDescription}
");

            return true;
        }

        private async Task<int?> GetClassOption(string message, int raceId)
        {
            var classes = await _raceService.GetRaceAllowedClasses(raceId);
            message = message.ToLower();

            foreach (string word in message.Split(" "))
            {
                foreach(var c in classes){
                    if (c.Name.ToLower().StartsWith(word))
                    {
                        return c.Id;
                    }
                }
            }

            return null;
        }
        #endregion

        #region Craft Profession Selection
        private async Task SendCraftOptions(SocketUser user, Player player)
        {
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.CraftProfession);
            await user.SendMessageAsync(await BuildCraftOptions());
            await user.SendMessageAsync(CreationMessages.CraftSelectionHelp);
        }

        private async Task<string> BuildCraftOptions()
        {
            string options = @"__Craft Professions__";
            var craftProfessions = await _professionService.GetCraftProfessions();
            foreach (var c in craftProfessions)
            {
                options += $@"
- {c.Name}";
            }

            return options;
        }

        private async Task SetCraftingProfession(SocketMessage socketMessage, SocketUser user, Player player)
        {
            int? craftProfessionOption = await GetCraftOption(socketMessage.Content);
            if (craftProfessionOption == null)
            {
                await user.SendMessageAsync(CreationMessages.CraftSelectionHelp);
                return;
            }

            player.CurrentCharacter.CraftProfessionId = (int)craftProfessionOption.Value;
            await _characterService.UpdateCharacter(player.CurrentCharacter);
            await SendGatherOptions(user, player);
        }
        private async Task<int?> GetCraftOption(string message)
        {
            var craftProfessions = await _professionService.GetCraftProfessions();
            message = message.ToLower();

            foreach (string word in message.Split(" "))
            {
                foreach (var c in craftProfessions)
                {
                    if (c.Name.ToLower().StartsWith(word))
                    {
                        return c.Id;
                    }
                }
            }

            return null;
        }
        #endregion

        #region Gather Profession Selection
        private async Task SendGatherOptions(SocketUser user, Player player)
        {
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.GatherProfession);
            await user.SendMessageAsync(await BuildGatherOptions());
            await user.SendMessageAsync(CreationMessages.GatherSelectionHelp);
        }

        private async Task<string> BuildGatherOptions()
        {
            string options = @"__Gather Professions__";
            var gatherProfessions = await _professionService.GetGatherProfessions();
            foreach (var g in gatherProfessions)
            {
                options += $@"
- {g.Name}";
            }

            return options;
        }

        private async Task SetGatherProfession(SocketMessage socketMessage, SocketUser user, Player player)
        {
            int? gatherProfessionOption = await GetGatherOption(socketMessage.Content);
            if (gatherProfessionOption == null)
            {
                await user.SendMessageAsync(CreationMessages.GatherSelectionHelp);
                return;
            }

            player.CurrentCharacter.GatherProfessionId = (int)gatherProfessionOption.Value;
            await _characterService.UpdateCharacter(player.CurrentCharacter);
            await SendConfirmation(user, player);
        }

        private async Task<int?> GetGatherOption(string message)
        {
            var gatherProfessions = await _professionService.GetGatherProfessions();
            message = message.ToLower();

            foreach (string word in message.Split(" "))
            {
                foreach (var c in gatherProfessions)
                {
                    if (c.Name.ToLower().StartsWith(word))
                    {
                        return c.Id;
                    }
                }
            }

            return null;
        }
        #endregion

        #region Start Location
        private async Task SendStartingLocations(SocketUser user, Player player)
        {
            //await _characterCreationCache.SetCreationState(player, CreationStateEnum.StartingLocation);
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = $"Pick a Starting Location"
            };

            var startingLocations = await _raceService.GetRaceStartingLocations(player.CurrentCharacter.RaceId);
            foreach (var location in startingLocations)
            {
                var breadcrumb = await _locationService.GetLocationBreadcrumb(location, null);
                var description = $@"{breadcrumb}

{location.Description}
";

                builder.AddField(location.Name, description);
            }

            await user.SendMessageAsync("", false, builder.Build());
        }

        private async Task SetStartingLocation(SocketMessage socketMessage, SocketUser user, Player player)
        {
            var content = socketMessage.Content.ToLower();

            Location selectedLocation = null;
            var startingLocations = await _raceService.GetRaceStartingLocations(player.CurrentCharacter.RaceId);
            foreach (var location in startingLocations)
            {
                if (location.Name.ToLower().StartsWith(content))
                {
                    selectedLocation = location;
                    break;
                }
            }

            if(selectedLocation == null)
            {
                await SendStartingLocations(user, player);
                return;
            }

            player.CurrentCharacter.CurrentLocationId = selectedLocation.Id;
            player.CurrentCharacter.CurrentSubLocationId = selectedLocation.SubLocation.FirstOrDefault(sl => sl.IsStartingSubLocation)?.Id;
            await SendConfirmation(user, player);
        }
        #endregion

        #region Confirmation
        private async Task SendConfirmation(SocketUser user, Player player)
        {
            await SendCharacterInfo(user, player);
            await user.SendMessageAsync(CreationMessages.ConfirmationHelp);
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.Confirmation);
        }

        private async Task SendCharacterInfo(SocketUser user, Player player)
        {
            var embed = await _characterDiscordService.GetCharacterSheet(player.CurrentCharacterId.Value);

            await user.SendMessageAsync("", false, embed);
        }

        private async Task Confirm(SocketMessage socketMessage, SocketUser user, Player player)
        {
            var message = socketMessage.Content.ToLower();
            bool? confirmed = null;
            bool quit = false;
            foreach(string word in message.Split(" "))
            {
                if (CreationMessages.ConfirmationKeywords.ContainsKey(word))
                {
                    confirmed = CreationMessages.ConfirmationKeywords[word];
                    break;
                }
                else if(CreationMessages.QuitKeywords.Contains(word))
                {
                    quit = true;
                }
            }

            if (quit)
            {
                await _characterCreationCache.SetCreationState(player, null);
                await user.SendMessageAsync(CreationMessages.QuitSuccess);
                return;
            }

            if(confirmed == null)
            {
                await user.SendMessageAsync(CreationMessages.ConfirmationHelp);
                return;
            }
            else if(confirmed == true)
            {
                player.CurrentCharacter.Created = true;
                player.CurrentCharacter.CreatedDate = DateTime.UtcNow;
                await _characterCreationCache.SetCreationState(player, null);
                await GiveStartingEquipment(player.CurrentCharacter);
                await _characterService.UpdateCharacter(player.CurrentCharacter);

                await user.SendMessageAsync(CreationMessages.ConfirmationComplete);
            }
            else if(confirmed == false)
            {
                await user.SendMessageAsync(CreationMessages.CreateName);
                await _characterCreationCache.SetCreationState(player, CreationStateEnum.Name);
            }

        }

        private async Task GiveStartingEquipment(Character character)
        {
            //Class


            //Crafting


            //Gathering
            var gatheringProfession = await _professionService.GetGatherProfession(character.GatherProfessionId);
            await _inventoryService.GiveItem(character.Id, gatheringProfession.GatheringProfessionStartingTool.StartingItemId, 1);
        }
        #endregion
    }
}
