﻿using Discord.WebSocket;
using LorinthsRpgDiscord.Bot.Cache.Interfaces;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

namespace LorinthsRpgDiscord.Bot.MessageHandlers.DirectMessage
{
    public class DirectMessageHandler : IDirectMessageHandler
    {
        private readonly ICharacterCreationCache _characterCreationCache;
        private readonly ICreateCharacterMessageHandler _createCharacterMessageHandler;
        public DirectMessageHandler(
            ICharacterCreationCache characterCreationCache,
            ICreateCharacterMessageHandler createCharacterMessageHandler)
        {
            _characterCreationCache = characterCreationCache;
            _createCharacterMessageHandler = createCharacterMessageHandler;
        }

        public async Task MessageReceived(SocketMessage socketMessage, Player player)
        {
            var message = socketMessage as SocketUserMessage;
            if (message == null) return;

            int argPos = 0;
            if ((message.HasCharPrefix('!', ref argPos)) ||
                message.Author.IsBot)
                return;

            if (socketMessage.Author.IsBot)
            {
                return;
            }

            if(_characterCreationCache.GetCreationState(player) != null)
            {
                await _createCharacterMessageHandler.MessageReceived(socketMessage, player);
                return;
            }

            await Task.Run(() =>
            {
                Console.WriteLine($"DM: {socketMessage.Author} -> {socketMessage.Content}");
            });
        }

    }
}
