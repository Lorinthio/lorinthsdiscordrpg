﻿using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Bot.MessageHandlers.DirectMessage;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.MessageHandlers
{
    public class GlobalMessageHandler : IGlobalMessageHandler
    {
        private readonly ICharacterService _characterService;
        private readonly IPlayerService _playerService;
        private readonly IDirectMessageHandler _directMessageHandler;

        public GlobalMessageHandler(
            ICharacterService characterService,
            IDirectMessageHandler directMessageHandler,
            IPlayerService playerService)
        {
            _characterService = characterService;
            _directMessageHandler = directMessageHandler;
            _playerService = playerService;
        }

        public async Task MessageReceived(SocketMessage socketMessage)
        {
            var message = socketMessage as SocketUserMessage;
            if (message == null) return;

            if (message.Author.IsBot)
            {
                return;
            }

            var userId = message.Author.Id;
            var player = await _playerService.GetOrCreatePlayer(userId.ToString());

            if (message.Channel is SocketDMChannel dmChannel)
            {
                await _directMessageHandler.MessageReceived(message, player);
                return;
            }

            int argPos = 0;
            if (message.HasCharPrefix('!', ref argPos))
            {
                return;
            }
        }
    }
}
