﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces
{
    public interface IGlobalMessageHandler
    {
        Task MessageReceived(SocketMessage socketMessage);
    }
}
