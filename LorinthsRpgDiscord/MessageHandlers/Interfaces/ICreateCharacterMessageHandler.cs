﻿using Discord.WebSocket;
using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces
{
    public interface ICreateCharacterMessageHandler
    {
        
        Task MessageReceived(SocketMessage socketMessage, Player player);
    }
}
