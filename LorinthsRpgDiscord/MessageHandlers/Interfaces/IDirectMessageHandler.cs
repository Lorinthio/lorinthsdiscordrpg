﻿using Discord.WebSocket;
using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces
{
    public interface IDirectMessageHandler
    {
        Task MessageReceived(SocketMessage socketMessage, Player player);
    }
}
