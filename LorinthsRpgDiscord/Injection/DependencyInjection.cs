﻿using LorinthsRpgDiscord.Bot.Cache;
using LorinthsRpgDiscord.Bot.Cache.Interfaces;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Bot.Discord;
using LorinthsRpgDiscord.Bot.MessageHandlers;
using LorinthsRpgDiscord.Bot.MessageHandlers.DirectMessage;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Service;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using LorinthsRpgDiscord.Bot.Tasks;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Discord;

namespace LorinthsRpgDiscord.Bot.Injection
{
    public class DependencyInjection
    {
        private static IServiceProvider _serviceProvider;
        public static void Setup()
        {
            var config = new ConfigurationBuilder()
#if DEBUG
                .AddJsonFile("appsettings.Development.json");
#else
                .AddJsonFile("appsettings.Production.json");
#endif

            IConfiguration Configuration = config.Build();

            //setup our DI
            _serviceProvider = new ServiceCollection()
                .AddSingleton<ICharacterCreationCache, CharacterCreationCache>()
                .AddTransient<ICharacterService, CharacterService>()
                .AddTransient<IClassService, ClassService>()
                .AddTransient<ICreateCharacterMessageHandler, CreateCharacterMessageHandler>()
                .AddTransient<ICharacterDiscordService, CharacterDiscordService>()
                .AddTransient<IDayNightChecker, DayNightChecker>()
                .AddTransient<IDirectMessageHandler, DirectMessageHandler>()
                .AddSingleton<IDiscordBot, DiscordBot>()
                .AddTransient<IGatherChecker, GatherChecker>()
                .AddTransient<IGatheringService, GatheringService>()
                .AddTransient<IGlobalMessageHandler, GlobalMessageHandler>()
                .AddTransient<IInventoryService, InventoryService>()
                .AddTransient<ILocationService, LocationService>()
                .AddTransient<ILootService, LootService>()
                .AddTransient<IPlayerService, PlayerService>()
                .AddTransient<IProfessionService, ProfessionService>()
                .AddTransient<IRaceService, RaceService>()
                .AddTransient<ITaskManager, TaskManager>()
                .AddTransient<ITimeService, TimeService>()
                .AddTransient<ITravelChecker, TravelChecker>()
                .AddTransient<ITravelService, TravelService>()
                .AddSingleton(Configuration)
                .AddDbContext<DiscordRpgThreadedContext>(ServiceLifetime.Transient)
                .BuildServiceProvider();

            Console.WriteLine("Dependency Injection Setup");
        }

        public static IServiceProvider GetServiceProvider()
        {
            return _serviceProvider;
        }
    }
}
