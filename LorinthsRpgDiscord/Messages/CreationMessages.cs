﻿using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Bot.Messages
{
    public class CreationMessages
    {
        public static string CreateName = "Create a name for your Character";

        public static string RaceSelectionHelp = @"
For more info on a specific Race use the phrase 'info <raceName>' to get more details or simply state a Race Name to select.";
        public static string ClassSelectionHelp = @"
For more info on a specific class use the phrase 'info <className>' to get more details or simply state a Class Name to select.";
        public static string CraftSelectionHelp = @"
Please state a Crafting Profession Name to select.";
        public static string GatherSelectionHelp = @"
Please state a Gathering Profession Name to select.";

        #region Confirmation
        public static Dictionary<string, bool> ConfirmationKeywords = new Dictionary<string, bool>()
        {
            { "yes", true },
            { "yah", true },
            { "ya", true },
            { "y", true },
            { "nah", false },
            { "no", false },
            { "n", false },
        };

        public static string ConfirmationHelp = @"
Please confirm if the character above is the one you would like to play. State 'yes' or 'no'.
Additionally, send 'quit' to quit this process and cancel creation";

        public static string ConfirmationComplete = @"
Your character has been created successfully!
";
        #endregion

        #region QUIT
        
        public static List<string> QuitKeywords = new List<string>()
        {
            "q",
            "quit"
        };

        public static string QuitSuccess = "You have quit the Character Creation Process";

        #endregion
    }
}
