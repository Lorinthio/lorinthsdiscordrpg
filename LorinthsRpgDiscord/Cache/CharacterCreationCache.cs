﻿using LorinthsRpgDiscord.Bot.Cache.Interfaces;
using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Cache
{
    public class CharacterCreationCache : ICharacterCreationCache
    {
        private readonly Dictionary<int, CreationStateEnum> _characterCreationStates;

        public CharacterCreationCache()
        {
            _characterCreationStates = new Dictionary<int, CreationStateEnum>();
        }

        public async Task SetCreationState(Player player, CreationStateEnum? value)
        {
            if (!_characterCreationStates.ContainsKey(player.Id))
            {
                _characterCreationStates.Add(player.Id, CreationStateEnum.Name);
            }
            else if(value == null)
            {
                _characterCreationStates.Remove(player.Id);
            }
            else
            {
                _characterCreationStates[player.Id] = value.Value;
            }
            await Task.CompletedTask;
        }

        public async Task<CreationStateEnum?> GetCreationState(Player player)
        {
            if (_characterCreationStates.ContainsKey(player.Id))
            {
                return await Task.FromResult(_characterCreationStates[player.Id]);
            }
            return null;
        }
    }
}
