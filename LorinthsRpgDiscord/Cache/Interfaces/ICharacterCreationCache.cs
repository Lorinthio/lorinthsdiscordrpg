﻿using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Cache.Interfaces
{
    public interface ICharacterCreationCache
    {
        Task SetCreationState(Player player, CreationStateEnum? value);
        Task<CreationStateEnum?> GetCreationState(Player player);
    }
}
