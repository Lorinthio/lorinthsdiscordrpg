﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class AdminModule : ModuleBase<SocketCommandContext>
    {
        private readonly ICharacterService _characterService;
        private readonly IPlayerService _playerService;
        private const string _adminName = "RPG World Builder";

        public AdminModule(
            ICharacterService characterService,
            IPlayerService playerService)
        {
            _characterService = characterService;
            _playerService = playerService;
        }

        [Command("tp")]
        public async Task Teleport(int locationId, int? subLocationId)
        {
            var user = Context.User as SocketGuildUser;
            var role = (user as IGuildUser).Guild.Roles.FirstOrDefault(x => x.Name == _adminName);
            if (!user.Roles.Contains(role))
            {
                await Context.Channel.SendMessageAsync("You do not have permissions to admin commands!");
                return;
            }

            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString(), user.Username);
            var character = player.CurrentCharacter;

            if(character == null)
            {
                return;
            }

            character.CurrentLocationId = locationId;
            character.CurrentSubLocationId = subLocationId;
            await _characterService.UpdateCharacter(character);
        }
    }
}
