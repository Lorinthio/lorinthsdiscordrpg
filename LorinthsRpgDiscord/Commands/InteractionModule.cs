﻿using Discord;
using Discord.Commands;
using LorinthsRpgDiscord.Bot.Messages;
using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class InteractionModule : ModuleBase<SocketCommandContext>
    {
        private readonly ICharacterService _characterService;
        private readonly ICharacterDiscordService _characterDiscordService;
        private readonly IPlayerService _playerService;

        public InteractionModule(
            ICharacterService characterService,
            ICharacterDiscordService characterDiscordService,
            IPlayerService playerService
            )
        {
            _characterService = characterService;
            _characterDiscordService = characterDiscordService;
            _playerService = playerService;
        }

        [Command("say")]
        [Summary("Speak as your RPG Character")]
        [Alias("speak", "s")]
        public async Task Say([Remainder] string message)
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            if (player.CurrentCharacter?.Created == true)
            {
                await Context.Channel.SendMessageAsync($@"{player.CurrentCharacter.CharacterName} said...
>>> {message}
");
            }

            await Context.Message.DeleteAsync();
        }

        [Command("info")]
        [Summary("View details about your or another character")]
        [Alias("i")]
        public async Task Info(string userIdParam = null)
        {
            bool isSelf = false;
            if (userIdParam == null)
            {
                userIdParam = Context.User.Id.ToString();
                isSelf = true;
            }
            else
            {
                userIdParam = userIdParam.Substring(3, userIdParam.Length - 4);
            }

            if (ulong.TryParse(userIdParam, out ulong userId))
            {
                var user = Context.User;
                var targetUser = await Context.Channel.GetUserAsync(userId);
                var player = await _playerService.GetOrCreatePlayer(userId.ToString());
                if (player.CurrentCharacter?.Created == null || player.CurrentCharacter?.Created == false)
                {
                    if (isSelf)
                    {
                        await user.SendMessageAsync($"You have not created an RPG Character yet! Use the command '!create' to create one");
                    }
                    else
                    {
                        await user.SendMessageAsync($"{targetUser.Username} has not created an RPG Character yet! Tell them to use !create in the rpg channel!");
                    }
                    return;
                }

                var embed = await _characterDiscordService.GetCharacterSheet(player.CurrentCharacterId.Value);
                if (isSelf)
                {
                    await user.SendMessageAsync($"__Your Character__");
                }
                else
                {
                    await user.SendMessageAsync($"__{targetUser.Username}'s Character__");
                }
                await user.SendMessageAsync("", false, embed);
            }
        }
    }
}
