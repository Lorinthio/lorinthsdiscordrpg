﻿using Discord;
using Discord.Commands;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class TimeModule : ModuleBase<SocketCommandContext>
    {
        private readonly ITimeService _timeService;
        public TimeModule(
            ITimeService timeService)
        {
            _timeService = timeService;
        }

        [Command("time")]
        public async Task Time()
        {
            var channel = Context.Channel;
            var gameClock = _timeService.GetTime();

            var embedBuilder = new EmbedBuilder
            {
                Description = $"It is currently ({gameClock.Time.Hours}:{gameClock.Time.Minutes})",
                Color = new Color(gameClock.Color.R, gameClock.Color.G, gameClock.Color.B)
            };

            await channel.SendMessageAsync("", false, embedBuilder.Build());
        }
    }
}
