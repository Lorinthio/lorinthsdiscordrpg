﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class GatherModule : ModuleBase<SocketCommandContext>
    { 
        private readonly ICharacterService _characterService;
        private readonly IGatheringService _gatheringService;
        private readonly IPlayerService _playerService;

        public GatherModule(
            ICharacterService characterService,
            IGatheringService gatheringService,
            IPlayerService playerService)
        {
            _characterService = characterService;
            _gatheringService = gatheringService;
            _playerService = playerService;
        }

        [Command("gather")]
        public async Task GatherResources()
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            var character = await _characterService.GetCharacter(player.CurrentCharacterId.Value);

            if(character.CurrentSubLocationId == null)
            {
                await Context.Channel.SendMessageAsync($"{user.Mention} - This is not a valid location to gather materials from");
                return;
            }

            var materials = await _gatheringService.GetPossibleItems(character.CurrentSubLocationId.Value);
            if(materials.Count() == 0)
            {
                await Context.Channel.SendMessageAsync($"{user.Mention} - There are no resources to gather here");
                return;
            }
            
            var startResult = await _gatheringService.StartGathering(character);
            var builder = new EmbedBuilder
            {
                Color = Color.Blue,
                Description = $"{character.CharacterName} has started gathering."
            };

            var availableResources = "";
            foreach (var item in materials.OrderBy(gm => gm.Name))
            {
                availableResources += $@"
{item.Name}";
            }

            var eta = TimeUtilities.GetEta(startResult.Duration);
            builder.AddField("Duration", eta);
            builder.AddField("Possible Resources", availableResources);
            await Context.Channel.SendMessageAsync("", false, builder.Build());
        }
    }
}
