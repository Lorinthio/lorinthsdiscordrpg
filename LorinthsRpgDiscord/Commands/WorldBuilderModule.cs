﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class WorldBuilderModule : ModuleBase<SocketCommandContext>
    {
        private const string _worldBuilderRole = "RPG World Builder";

        [Command("worldbuild")]
        [Alias("wb")]
        public async Task WorldBuild(string type, string json)
        {
            var user = Context.User as SocketGuildUser;
            var role = (user as IGuildUser).Guild.Roles.FirstOrDefault(x => x.Name == _worldBuilderRole);
            if (!user.Roles.Contains(role))
            {
                await Context.Channel.SendMessageAsync("You do not have permissions to World Build!");
                return;
            }

            switch (type.ToLower())
            {
                case "item":
                    await WorldBuildItem(json);
                    return;
            }
        }

        private async Task WorldBuildItem(string json)
        {
            ItemDto itemDto = JsonConvert.DeserializeObject<ItemDto>(json);

            await Context.Channel.SendMessageAsync("WorldBuilder: Attempting to create item named, " + itemDto.Name);
        }
    }
}
