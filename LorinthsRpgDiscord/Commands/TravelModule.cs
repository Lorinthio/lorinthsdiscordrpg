﻿using AutoMapper.Configuration;
using Discord;
using Discord.Commands;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class TravelModule : ModuleBase<SocketCommandContext>
    {
        private readonly ICharacterService _characterService;
        private readonly IGatheringService _gatheringService;
        private readonly IPlayerService _playerService;
        private readonly ILocationService _locationService;
        private readonly ITravelService _travelService;

        public TravelModule(
            ICharacterService characterService,
            IGatheringService gatheringService,
            IPlayerService playerService,
            ILocationService locationService,
            ITravelService travelService)
        {
            _characterService = characterService;
            _gatheringService = gatheringService;
            _playerService = playerService;
            _locationService = locationService;
            _travelService = travelService;
        }

        [Command("whereami")]
        [Alias("here")]
        public async Task WhereAmI()
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            var character = await _characterService.GetCharacter(player.CurrentCharacterId.Value);

            var isBusy = await ValidateIsBusy(character);
            if (isBusy)
            {
                return;
            }

            var location = await _locationService.GetLocation(character.CurrentLocationId.Value);
            var breadcrumb = await _locationService.GetLocationBreadcrumb(location, character.CurrentSubLocationId);

            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = $"Your current location"
            };
            builder.AddField(breadcrumb, location.Description?.Length == 0 ? "TODO" : location.Description);

            await GetGatheringMaterials(character, builder);
            await GetTravelLocations(location, character, builder);
            await ReplyAsync("", false, builder.Build());
        }

        [Command("go")]
        public async Task Go([Remainder] string locationName)
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            var character = await _characterService.GetCharacter(player.CurrentCharacterId.Value);

            var isBusy = await ValidateIsBusy(character);
            if (isBusy)
            {
                return;
            }

            var travelLocations = await _travelService.GetTravelLocations(character.CurrentLocationId.Value, character.CurrentSubLocationId);
            var targets = new List<TravelLocationDto>();
            foreach (var travelLoc in travelLocations)
            {
                if(travelLoc.SubLocation != null)
                {
                    if (travelLoc.SubLocation?.Name.ToLower().StartsWith(locationName.ToLower()) == true)
                    {
                        targets.Add(travelLoc);
                        continue;
                    }
                }
                else
                {
                    if (travelLoc.Location.Name.ToLower().StartsWith(locationName.ToLower()))
                    {
                        targets.Add(travelLoc);
                        continue;
                    }
                }
            }

            var isValid = await ValidateTargetLocations(targets);
            if (!isValid)
            {
                return;
            }

            var target = targets.First();

            if(target == null)
            {
                await ReplyAsync($"{Context.Message.Author.Mention} - That is not a valid destination");
                return;
            }

            var span = await _travelService.CharacterTravel(character, target.Location.Id, target.SubLocation?.Id, Dto.Enums.TravelTypeEnum.Foot);

            if(span == TimeSpan.Zero)
            {
                await ReplyAsync($"You have entered {target.Name}");
                return;
            }
            else
            {
                var eta = TimeUtilities.GetEta(span);
                await ReplyAsync($"Your character has started to walk to {target.Name}. (ETA: {eta})");
                return;
            }
        }

        private async Task<bool> ValidateIsBusy(Character character)
        {
            var characterBusy = await _characterService.IsCharacterBusy(character.Id);

            if (characterBusy.IsTravelling)
            {
                await ValidateCurrentlyTraveling(character);
            }
            else if (characterBusy.IsGathering)
            {
                await ValidateIsGathering(character);
            }

            return characterBusy.IsCrafting || characterBusy.IsGathering || characterBusy.IsTravelling;
        }

        private async Task<bool> ValidateCurrentlyTraveling(Character character)
        {
            var currentTravel = await _travelService.GetCharacterTravelTime(character.Id);
            if (currentTravel != null)
            {
                var remaining = currentTravel.ArrivalTime.Subtract(DateTime.UtcNow);
                var etaRemaining = TimeUtilities.GetEta(remaining);
                await ReplyAsync($"{Context.Message.Author.Mention} - You are traveling to {currentTravel.Location.Name} (ETA: {etaRemaining})");
                return false;
            }
            return true;
        }

        private async Task<bool> ValidateIsGathering(Character character)
        {
            var currentGathering = await _gatheringService.GetCharacterGatherTime(character.Id);
            if (currentGathering != null)
            {
                var remaining = currentGathering.FinishTime.Subtract(DateTime.UtcNow);
                var etaRemaining = TimeUtilities.GetEta(remaining);
                await ReplyAsync($"{Context.Message.Author.Mention} - You are currently gathering (ETA: {etaRemaining})");
                return false;
            }
            return true;
        }

        private async Task<bool> ValidateTargetLocations(List<TravelLocationDto> targets)
        {
            if (targets.Count > 1)
            {
                var embed = new EmbedBuilder
                {
                    Color = Color.Blue
                };
                foreach(var target in targets)
                {
                    embed.Description += $@"
{target.Name}";
                }
                await ReplyAsync("Your input matched more than one travel location. Please specify one of the below...", false, embed.Build());
                return false;
            }

            if (targets.Count == 0)
            {
                await ReplyAsync("Your input didn't match any of the available locations nearby. Use !here to get possible travel locations");
                return false;
            }

            return true;
        }
    
        private async Task GetGatheringMaterials(Character character, EmbedBuilder builder)
        {
            if (character.CurrentSubLocationId == null)
            {
                return;
            }

            var gatheringMaterials = await _gatheringService.GetPossibleItems(character.CurrentSubLocationId.Value);
            if (gatheringMaterials.Count() == 0)
            {
                return;
            }

            var availableResources = "";
            foreach(var item in gatheringMaterials.OrderBy(gm => gm.Name))
            {
                availableResources += $@"
{item.Name}";
            }
            builder.AddField("Available Resources", availableResources);
        }

        private async Task GetTravelLocations(Location location, Character character, EmbedBuilder builder)
        {
            var travelLocations = await _travelService.GetTravelLocations(location.Id, character.CurrentSubLocationId);
            var nearbyLocationString = "";
            var travelLocationString = "";
            foreach (var travelLoc in travelLocations)
            {
                if (travelLoc.DistanceInMiles > 0)
                {
                    travelLocationString += $"{travelLoc.Name} - {travelLoc.DistanceInMiles} miles\n";
                }
                else
                {
                    nearbyLocationString += $"{travelLoc.Name}\n";
                }
            }

            if (nearbyLocationString.Length > 0)
            {
                builder.AddField("Nearby Locations", nearbyLocationString);
            }
            if (travelLocationString.Length > 0)
            {
                builder.AddField("Travel Locations", travelLocationString);
            }
        }

    }
}
