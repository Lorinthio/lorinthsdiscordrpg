﻿using Discord;
using Discord.Commands;
using LorinthsRpgDiscord.Bot.Cache.Interfaces;
using LorinthsRpgDiscord.Data.Enums;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Bot.Messages;
using LorinthsRpgDiscord.Service.Interfaces;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class CreationModule : ModuleBase<SocketCommandContext>
    {
        private readonly ICharacterCreationCache _characterCreationCache;
        private readonly ICharacterService _characterService;
        private readonly IPlayerService _playerService;

        public CreationModule(
            ICharacterCreationCache characterCreationCache,
            ICharacterService characterService,
            IPlayerService playerService
            )
        {
            _characterCreationCache = characterCreationCache;
            _characterService = characterService;
            _playerService = playerService;
        }

        [Command("create")]
        [Summary("Create a new RPG Character.")]
        [Alias("new")]
        public async Task CreateAsync()
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            if(player.CurrentCharacter?.Created == true)
            {
                await Context.Channel.SendMessageAsync($"{user.Mention} - You have already created a character!");
                return;
            }

            var character = await _characterService.CreateCharacter(player.Id);
            await _characterCreationCache.SetCreationState(player, CreationStateEnum.Name);
            await Context.User.SendMessageAsync(CreationMessages.CreateName);
        }

        [Command("bio")]
        public async Task BioAsync([Remainder] string bio)
        {
            var player = await _playerService.GetOrCreatePlayer(Context.User.Id.ToString());
            player.CurrentCharacter.Bio = bio;
            await _characterService.UpdateCharacter(player.CurrentCharacter);
            await Context.User.SendMessageAsync("Updated your characters bio!");
        }
    }
}
