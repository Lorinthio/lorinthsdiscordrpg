﻿
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Bot.Commands.Interfaces;
using LorinthsRpgDiscord.Bot.Injection;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class CommandHandler : ICommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly ICharacterService _characterService;
        private readonly IPlayerService _playerService;
        private readonly IServiceProvider _serviceProvider;

        // Retrieve client and CommandService instance via ctor
        public CommandHandler(
            DiscordSocketClient client, 
            CommandService commands,
            ICharacterService characterService,
            IPlayerService playerService)
        {
            _commands = commands;
            _client = client;
            _characterService = characterService;
            _playerService = playerService;
            _serviceProvider = DependencyInjection.GetServiceProvider();
        }

        public async Task InstallCommandsAsync()
        {
            await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                            services: _serviceProvider);
            _client.MessageReceived += HandleCommandAsync;
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a system message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!(message.HasCharPrefix('!', ref argPos) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            // Create a WebSocket-based command context based on the message
            var context = new SocketCommandContext(_client, message);

            if (!message.Content.StartsWith("!create"))
            {
                var hasCharacter = await CurrentUserHasCharacter(context);
                if (!hasCharacter)
                {
                    return;
                }
            }

            // Execute the command with the command context we just
            // created, along with the service provider for precondition checks.
            await _commands.ExecuteAsync(
                context: context,
                argPos: argPos,
                services: _serviceProvider);
        }

        private async Task<bool> CurrentUserHasCharacter(SocketCommandContext context)
        {
            var user = context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            if(player.CurrentCharacterId == null)
            {
                await context.Channel.SendMessageAsync($"{context.User.Mention} - You have not created an RPG Character yet! Use the command '!create' to create one");
                return false;
            }

            var character = await _characterService.GetCharacter(player.CurrentCharacterId.Value);

            if (character?.Created == false)
            {
                await context.Channel.SendMessageAsync($"{context.User.Mention} - You have not finished creating a character. Check for a DM from me!");
                return false;
            }

            return true;
        }
    }
}