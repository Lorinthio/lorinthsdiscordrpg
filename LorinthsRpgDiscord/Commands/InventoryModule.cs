﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands
{
    public class InventoryModule : ModuleBase<SocketCommandContext>
    {
        private readonly IInventoryService _inventoryService;
        private readonly IPlayerService _playerService;

        public InventoryModule(
            IInventoryService inventoryService,
            IPlayerService playerService)
        {
            _inventoryService = inventoryService;
            _playerService = playerService;
        }

        [Command("inventory")]
        [Alias("inv")]
        public async Task SendInventory()
        {
            var user = Context.User;
            var player = await _playerService.GetOrCreatePlayer(user.Id.ToString());
            var inventory = await _inventoryService.GetCharacterInventory(player.CurrentCharacter.Id);

            await SendSummary(user, inventory);
            await SendWeapons(user, inventory);
            await SendArmor(user, inventory);
            await SendTools(user, inventory);
            await SendItems(user, inventory);
        }

        private async Task SendSummary(SocketUser user, InventoryDto inventory)
        {
            var embed = new EmbedBuilder
            {
                Color = Color.Blue,
                Description = "Inventory Summary"
            };

            embed.AddField("Weight", inventory.TotalWeight, true);
            embed.AddField("Value", inventory.TotalValue, true);
            await user.SendMessageAsync("", false, embed.Build());
        }

        private async Task SendWeapons(SocketUser user, InventoryDto inventory)
        {
            if (inventory.Weapons.Count == 0)
            {
                return;
            }

            var embed = new EmbedBuilder
            {
                Color = Color.Blue
            };

            var weaponInfo = "";
            foreach(var item in inventory.Weapons)
            {
                weaponInfo += $@"
{item.Name} x {item.Amount} [{item.TotalWeight} lbs]";
            }

            embed.AddField("Weapons", weaponInfo);
            await user.SendMessageAsync("", false, embed.Build());
        }

        private async Task SendArmor(SocketUser user, InventoryDto inventory)
        {
            if (inventory.Armor.Count == 0)
            {
                return;
            }

            var embed = new EmbedBuilder
            {
                Color = Color.Blue
            };

            var armorInfo = "";
            foreach (var item in inventory.Armor)
            {
                armorInfo += $@"
{item.Name} x {item.Amount} [{item.TotalWeight} lbs]";
            }

            embed.AddField("Armor", armorInfo);
            await user.SendMessageAsync("", false, embed.Build());
        }

        private async Task SendTools(SocketUser user, InventoryDto inventory)
        {
            if (inventory.CraftTools.Count == 0 && inventory.GatherTools.Count == 0)
            {
                return;
            }

            var embed = new EmbedBuilder
            {
                Color = Color.Blue
            };

            if(inventory.CraftTools.Count > 0)
            {
                var craftToolsInfo = "";
                foreach (var item in inventory.CraftTools)
                {
                    craftToolsInfo += $@"
{item.Name} x {item.Amount} [{item.TotalWeight} lbs]";
                }
                embed.AddField("Crafting Tools", craftToolsInfo);
            }
            if (inventory.GatherTools.Count > 0)
            {

                var gatherToolsInfo = "";
                foreach (var item in inventory.GatherTools)
                {
                    gatherToolsInfo += $@"
{item.Name} x {item.Amount} [{item.TotalWeight} lbs]";
                }
                embed.AddField("Gathering Tools", gatherToolsInfo);
            }

            await user.SendMessageAsync("", false, embed.Build());
        }

        private async Task SendItems(SocketUser user, InventoryDto inventory)
        {
            if (inventory.Items.Count == 0)
            {
                return;
            }

            var embed = new EmbedBuilder
            {
                Color = Color.Blue
            };

            var itemInfo = "";
            foreach (var item in inventory.Items)
            {
                itemInfo += $@"
{item.Name} x {item.Amount} [{item.TotalWeight} lbs]";
            }

            embed.AddField("Items", itemInfo);
            await user.SendMessageAsync("", false, embed.Build());
        }
    }
}
