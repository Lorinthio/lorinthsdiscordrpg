﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Commands.Interfaces
{
    public interface ICommandHandler
    {
        Task InstallCommandsAsync();
    }
}
