﻿using LorinthsRpgDiscord.Bot.Discord;
using LorinthsRpgDiscord.Bot.Injection;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DependencyInjection.Setup();
            Start().GetAwaiter().GetResult();
        }

        private static async Task Start()
        {
            var serviceProvider = DependencyInjection.GetServiceProvider();
            IDiscordBot bot = serviceProvider.GetService<IDiscordBot>();
            ITaskManager taskManager = serviceProvider.GetService<ITaskManager>();

            await bot.Login();
            await taskManager.Start();
        }
    }
}
