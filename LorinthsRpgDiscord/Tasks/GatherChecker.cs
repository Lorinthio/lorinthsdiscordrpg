﻿using Discord;
using LorinthsRpgDiscord.Bot.Discord;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Tasks
{
    public class GatherChecker : IGatherChecker
    {
        private readonly IGatheringService _gatheringService;
        private readonly ILocationService _locationService;
        private readonly IDiscordBot _discordBot;
        private const int _checkDelay = 5000;

        public GatherChecker(IGatheringService gatheringService,
            ILocationService locationService,
            IDiscordBot discordBot)
        {
            _gatheringService = gatheringService;
            _locationService = locationService;
            _discordBot = discordBot;
        }

        public async Task Run()
        {
            Thread.Sleep(_checkDelay);

            try
            {
                var completed = await _gatheringService.GetCompletedGatherTimes();
                if (completed == null)
                {
                    return;
                }

                foreach (var gatherTime in completed)
                {
                    var character = gatherTime.Character;
                    var player = gatherTime.Player;

                    var embedBuilder = new EmbedBuilder
                    {
                        Color = Color.Green
                    };

                    var breadcrumb = await _locationService.GetLocationBreadcrumb(character.CurrentLocationId.Value, character.CurrentSubLocationId);
                    embedBuilder.Description = $"{character.CharacterName} has finished gathering at {breadcrumb}";

                    var itemList = "";
                    foreach (var item in gatherTime.Items)
                    {
                        itemList += @$"
{item.GetInventoryLine()}";
                    }

                    embedBuilder.AddField("Materials Gathered", itemList);
                    if (gatherTime.CritAmount > 0)
                    {
                        embedBuilder.AddField("Critical", $"x{gatherTime.CritAmount}");
                    }

                    await _discordBot.SendMessage(UInt64.Parse(player.UserId), embedBuilder);

                    var user = _discordBot.GetUser(UInt64.Parse(player.UserId));
                    await _discordBot.SendMessageToChannel($"{user.Mention} - {character.CharacterName} has finished an activity, please check your messages for specifics");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
    }
}
