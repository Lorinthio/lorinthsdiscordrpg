﻿using Discord;
using LorinthsRpgDiscord.Bot.Discord;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Tasks
{
    public class DayNightChecker : IDayNightChecker
    {
        private readonly IDiscordBot _discordBot;
        private readonly ITimeService _timeService;
        private const int _checkDelay = 5000;

        private TimeOfDay? currentTimeOfDay;

        public DayNightChecker(
            IDiscordBot discordBot,
            ITimeService timeService)
        {
            _discordBot = discordBot;
            _timeService = timeService;
        }

        public async Task Run()
        {
            Thread.Sleep(_checkDelay);

            try {
                var gameClock = _timeService.GetTime();
                if (gameClock.TimeOfDay != currentTimeOfDay)
                {
                    currentTimeOfDay = gameClock.TimeOfDay;
                    var embedBuilder = new EmbedBuilder
                    {
                        Description = $"It is now {currentTimeOfDay}",
                        Color = new Color(gameClock.Color.R, gameClock.Color.G, gameClock.Color.B)
                    };
                    //_discordBot.SendMessageToChannel(embedBuilder);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

    }
}
