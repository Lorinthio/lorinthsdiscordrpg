﻿using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Tasks
{
    public class TaskManager : ITaskManager
    {
        private readonly IDayNightChecker _dayNightChecker;
        private readonly IGatherChecker _gatherChecker;
        private readonly ITravelChecker _travelChecker;

        public TaskManager(
            IDayNightChecker dayNightChecker,
            IGatherChecker gatherChecker,
            ITravelChecker travelChecker)
        {
            _dayNightChecker = dayNightChecker;
            _gatherChecker = gatherChecker;
            _travelChecker = travelChecker;
        }

        public async Task Start()
        {
            Task.WaitAll(Task.Run(async () =>
            {
                while (true)
                {
                    await _dayNightChecker.Run();
                }
            }),
            Task.Run(async () =>
            {
                while (true)
                {
                    await _gatherChecker.Run();
                }
            }),
            Task.Run(async () =>
            {
                while (true)
                {
                    await _travelChecker.Run();
                }
            }));
        }
    }
}
