﻿using Discord;
using LorinthsRpgDiscord.Bot.Discord;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Tasks
{
    public class TravelChecker : ITravelChecker
    {
        private readonly ITravelService _travelService;
        private readonly IDiscordBot _discordBot;
        private const int _checkDelay = 5000;

        public TravelChecker(ITravelService travelService,
            IDiscordBot discordBot)
        {
            _travelService = travelService;
            _discordBot = discordBot;
        }
        
        public async Task Run()
        {
            Thread.Sleep(_checkDelay);

            try
            {
                var completed = await _travelService.GetCompletedTravelTimes();
                if (completed == null)
                {
                    return;
                }

                foreach (var travelTime in completed)
                {
                    var character = travelTime.Character;
                    var player = character.Player;

                    var embedBuilder = new EmbedBuilder
                    {
                        Color = Color.Blue
                    };

                    embedBuilder.Description = $"{character.CharacterName} has arrived at {travelTime.Location.Name}";

                    await _discordBot.SendMessage(UInt64.Parse(player.UserId), embedBuilder);
                    var user = _discordBot.GetUser(UInt64.Parse(player.UserId));
                    await _discordBot.SendMessageToChannel($"{user.Mention} - {character.CharacterName} has arrived at their location, please check your messages for specifics");
                }
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
    }
}
