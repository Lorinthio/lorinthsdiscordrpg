﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Tasks.Interfaces
{
    public interface ITaskManager
    {
        Task Start();
    }
}
