﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Discord
{
    public interface IDiscordBot
    {
        Task Login();
        Task SendMessage(ulong userId, string message);
        Task SendMessage(ulong userId, EmbedBuilder embedBuilder);
        Task SendMessageToChannel(string message);
        Task SendMessageToChannel(EmbedBuilder embedBuilder);
        SocketUser GetUser(ulong userId);
    }
}
