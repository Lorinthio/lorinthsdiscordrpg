﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LorinthsRpgDiscord.Bot.Commands;
using LorinthsRpgDiscord.Bot.Logging;
using LorinthsRpgDiscord.Bot.MessageHandlers;
using LorinthsRpgDiscord.Bot.MessageHandlers.Interfaces;
using LorinthsRpgDiscord.Bot.Tasks.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Bot.Discord
{
    public class DiscordBot : IDiscordBot
    {
        private readonly ulong _channelId;
        private readonly DiscordSocketClient _client;
        private readonly IConfiguration _configuration;
        private readonly IGlobalMessageHandler _messageHandler;
        private readonly IServiceProvider _serviceProvider;

        public DiscordBot(
            IConfiguration configuration,
            IGlobalMessageHandler messageHandler,
            IServiceProvider serviceProvider)
        {
            _configuration = configuration;
            _messageHandler = messageHandler;
            _serviceProvider = serviceProvider;

            _channelId = _configuration.GetValue<ulong>("TargetChannelId");
            var _config = new DiscordSocketConfig { MessageCacheSize = 100 };
            _client = new DiscordSocketClient(_config);
        }

        public async Task Login()
        {
            _client.Log += Log;

            var token = _configuration.GetValue<string>("BotToken");
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            var commandService = new CommandService();
            var commandHandler = new CommandHandler(_client, commandService, _serviceProvider.GetService<ICharacterService>(), _serviceProvider.GetService<IPlayerService>());
            await commandHandler.InstallCommandsAsync();
            _client.MessageReceived += _messageHandler.MessageReceived;
            _client.Ready += Ready;

            var loggingService = new LoggingService(_client, commandService);
        }

        private async Task Ready()
        {
            var channel = _client.GetChannel(_channelId) as IMessageChannel;
            //await channel.SendMessageAsync("The dungeon has opened");
            Console.WriteLine("The dungeon has opened...");
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public async Task SendMessage(ulong userId, string message)
        {
            await _client.GetUser(userId).SendMessageAsync(message);
        }

        public async Task SendMessage(ulong userId, EmbedBuilder embedBuilder)
        {
            var user = _client.GetUser(userId);
            if(user == null)
            {
                Console.WriteLine($"Unable to message user, {userId}");
                return;
            }
            await _client.GetUser(userId).SendMessageAsync("", false, embedBuilder.Build());
        }

        public async Task SendMessageToChannel(string message)
        {
            var channel = _client.GetChannel(_channelId) as IMessageChannel;
            await channel.SendMessageAsync(message);
        }

        public async Task SendMessageToChannel(EmbedBuilder embedBuilder)
        {
            var channel = _client.GetChannel(_channelId) as IMessageChannel;
            await channel.SendMessageAsync("", false, embedBuilder.Build());
        }

        public SocketUser GetUser(ulong userId)
        {
            return _client.GetUser(userId);
        }
    }
}
