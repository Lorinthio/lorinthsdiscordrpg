﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class PostService : IPostService
    {
        private readonly DiscordRpgContext _context;

        public PostService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Post>> GetAllPosts()
        {
            return await _context.Post.OrderByDescending(p => p.UploadDate).ToListAsync();
        }

        public async Task<Post> GetPost(int id)
        {
            return await _context.Post.SingleOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Post> SavePost(Post post)
        {
            try
            {
                if (post.Id == default(int))
                {
                    _context.Post.Add(post);
                }
                else
                {
                    _context.Post.Update(post);
                }

                await _context.SaveChangesAsync();

                return post;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Post>> DeletePost(int id)
        {
            Post post = await GetPost(id);

            if (post == null)
            {
                throw new Exception("Post was not found.");
            }

            _context.Post.Remove(post);

            await _context.SaveChangesAsync();

            return await GetAllPosts();
        }
    }
}