﻿using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ITimeService
    {
        GameClock GetTime();
    }
}
