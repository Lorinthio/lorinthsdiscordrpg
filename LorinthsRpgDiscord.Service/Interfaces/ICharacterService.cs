﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ICharacterService
    {
        Task<Character> CreateCharacter(int playerId);
        Task<Character> GetCharacter(Guid id);
        Task UpdateCharacter(Character character);
        Task<CharacterBusyDto> IsCharacterBusy(Guid id);
    }
}
