﻿using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IInventoryService
    {
        Task<InventoryDto> GetCharacterInventory(Guid characterId);
        Task GiveItem(Guid characterId, int itemId, int amount);
        Task TakeItem(Guid characterId, int itemId, int amount);
    }
}
