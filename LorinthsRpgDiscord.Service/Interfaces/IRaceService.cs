﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IRaceService
    {
        Task<Race> Get(int id);
        Task<IEnumerable<Race>> GetAll();
        Task<IEnumerable<Class>> GetRaceAllowedClasses(int raceId);
        Task<IEnumerable<Location>> GetRaceStartingLocations(int raceId);
        Task<IEnumerable<LookupDto>> GetAllLookups();
    }
}
