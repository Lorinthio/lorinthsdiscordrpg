﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ILocationService
    {
        Task<Location> GetLocation(int id);
        Task<Location> SaveLocation(Location location);
        Task<Region> SaveRegion(Region region);
        Task<Continent> SaveContinent(Continent continent);
        Task<IEnumerable<LookupDto>> GetLocationTypes();
        Task<string> GetLocationBreadcrumb(int locationId, int? subLocationId);
        Task<string> GetLocationBreadcrumb(Location location, int? subLocationId);
    }
}
