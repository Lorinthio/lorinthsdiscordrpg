﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IClassService
    {
        Task<IEnumerable<Class>> GetAll();
        Task<Class> Get(int id);
        Task<IEnumerable<LookupDto>> GetAllLookups();
    }
}
