﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ISecurityService
    {
        Task<Guid> CreateSession(int playerId, string ipAddress);
        Task<User> GetCurrentUser(Guid sessionId, string ipAddress);
        Task<Player> GetCurrentPlayer(Guid sessionId, string ipAddress);
        Task<IEnumerable<string>> GetPlayerPermissions(int playerId);
    }
}
