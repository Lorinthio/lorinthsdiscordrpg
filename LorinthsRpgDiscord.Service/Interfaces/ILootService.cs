﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ILootService
    {
        Task<LootTableEntry> GetLootResult(int lootTableId);
    }
}
