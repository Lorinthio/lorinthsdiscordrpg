﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IPlayerService
    {
        Task<Player> GetPlayer(string id);
        Task<Player> GetOrCreatePlayer(string id);
        Task<Player> GetOrCreatePlayer(string id, string username);
        Task<Player> UpdatePlayer(DiscordUserDto userModel);
    }
}
