﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IGatheringService
    {
        Task<CharacterGatherTime> GetCharacterGatherTime(Guid characterId);
        Task<IEnumerable<GatherResultDto>> GetCompletedGatherTimes();
        Task<IEnumerable<Item>> GetPossibleItems(int subLocationId);
        Task<GatherStartDto> StartGathering(Character character);
    }
}
