﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ITravelService
    {
        Task<IEnumerable<TravelLocationDto>> GetTravelLocations(int locationId, int? subLocationId);
        Task<IEnumerable<TravelLocationDto>> GetWalkingLocations(int locationId, int? subLocationId);
        Task<IEnumerable<Location>> SearchLocations(string search);
        Task<CharacterTravelTime> GetCharacterTravelTime(Guid characterId);
        Task<IEnumerable<CharacterTravelTime>> GetCompletedTravelTimes();
        Task<TimeSpan> CharacterTravel(Character character, int locationId, int? subLocationId, TravelTypeEnum travelType);
        Task<TimeSpan> CharacterTravel(Guid characterId, int locationId, int? subLocationId, TravelTypeEnum travelType);
    }
}
