﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IGameSessionService
    {
        Task<GameSession> CreateGameSession(int playerId);
        Task<bool> GameSessionIncludesPlayer(Guid sessionId, int playerId);
    }
}
