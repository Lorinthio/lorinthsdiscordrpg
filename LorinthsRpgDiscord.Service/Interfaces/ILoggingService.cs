﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface ILoggingService
    {
        void LogInformation(string message);
        void LogWarning(string message);
        void LogDebug(string message);
        void LogError(string message);
        void LogError(Exception ex, string message);
    }
}
