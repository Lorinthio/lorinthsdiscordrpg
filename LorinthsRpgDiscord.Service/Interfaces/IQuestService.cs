﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IQuestService
    {

        Task<List<LookupDto>> GetQuestLookups();
        Task<List<LookupDto>> GetGoalTypes();
        Task<Quest> GetQuest(int id);
        Task<Quest> SaveQuest(Quest quest);
        Task<List<Quest>> SearchQuests(string searchText);
    }
}
