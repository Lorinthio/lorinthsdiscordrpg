﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IProfessionService
    {
        Task<CraftProfession> GetCraftProfession(int id);
        Task<GatherProfession> GetGatherProfession(int id);
        Task<IEnumerable<CraftProfession>> GetCraftProfessions();
        Task<IEnumerable<GatherProfession>> GetGatherProfessions();
    }
}
