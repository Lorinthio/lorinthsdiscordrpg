﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service.Interfaces
{
    public interface IPostService
    {
        Task<IEnumerable<Post>> GetAllPosts();

        Task<Post> GetPost(int id);

        Task<Post> SavePost(Post post);

        Task<IEnumerable<Post>> DeletePost(int id);
    }
}
