﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Extensions;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class GatheringService : IGatheringService
    {
        private readonly DiscordRpgContext _context;
        private readonly IInventoryService _inventoryService;
        private readonly ILootService _lootService;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;

        private const int GatherTriesPerCommand = 10;

        public GatheringService(
            DiscordRpgContext context,
            IInventoryService inventoryService,
            ILootService lootService,
            IServiceProvider serviceProvider,
            IConfiguration configuration)
        {
            _context = context;
            _inventoryService = inventoryService;
            _lootService = lootService;
            _serviceProvider = serviceProvider;
            _configuration = configuration;
        }

        public async Task<IEnumerable<Item>> GetPossibleItems(int subLocationId)
        {
            var subLocation = await _context.SubLocation
                .FirstOrDefaultAsync(sl => sl.Id == subLocationId);

            if(subLocation == null)
            {
                return new List<Item>();
            }

            var resourceNode = await _context.ResourceNode
                .FirstOrDefaultAsync(rn => rn.SubLocationId == subLocationId);

            if(resourceNode == null)
            {
                return new List<Item>();
            }

            var normalEntries = await _context.LootTableEntry
                .Include(lte => lte.TargetItem)
                .Where(lte => lte.LootTableId == resourceNode.LootTableId)
                .ToListAsync();
            var critEntries = await _context.LootTableEntry
                .Include(lte => lte.TargetItem)
                .Where(lte => lte.LootTableId == resourceNode.LootTableCritId)
                .ToListAsync();

            var normalItems = normalEntries.Select(entry => entry.TargetItem);
            var critItems = critEntries.Select(entry => entry.TargetItem);

            var allItems = normalItems.Union(critItems).DistinctBy(o => o.Id);
            return allItems;
        }

        public async Task<CharacterGatherTime> GetCharacterGatherTime(Guid characterId)
        {
            return await _context.CharacterGatherTime
                .FirstOrDefaultAsync(cgt => cgt.CharacterId == characterId);
        }

        public async Task<IEnumerable<GatherResultDto>> GetCompletedGatherTimes()
        {
            try
            {
                using (DiscordRpgContext context = (DiscordRpgContext)_serviceProvider.GetService(typeof(DiscordRpgContext)))
                {
                    var now = DateTime.UtcNow;
                    var completed = await context.CharacterGatherTime
                            .Include(cgt => cgt.Character)
                            .ThenInclude(c => c.Player)
                            .Where(cgt => cgt.FinishTime < now)
                            .ToListAsync();

                    if (completed.Count == 0)
                    {
                        return null;
                    }

                    var resultDtos = new List<GatherResultDto>();
                    foreach (var complete in completed)
                    {
                        var result = await GetGatherResult(complete);
                        await GiveCharacterItems(result);
                        resultDtos.Add(result);
                    }

                    context.CharacterGatherTime.RemoveRange(completed);
                    await context.SaveChangesAsync();

                    return resultDtos;
                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return null;
        }

        private async Task<GatherResultDto> GetGatherResult(CharacterGatherTime complete)
        {
            var resourceNode = await _context.ResourceNode.FirstOrDefaultAsync(rn => rn.Id == complete.ResourceNodeId);
            var result = new GatherResultDto
            {
                Character = complete.Character,
                Player = complete.Character.Player,
                CritAmount = 0,
                Items = new List<ItemStackDto>()
            };
            for (int i = 0; i < GatherTriesPerCommand; i++)
            {
                LootTableEntry lootEntry;
                if (RandomUtilities.GetNextChanceRoll() < 5.0)
                {
                    result.CritAmount++;
                    lootEntry = await _lootService.GetLootResult(resourceNode.LootTableCritId);
                }
                else
                {
                    lootEntry = await _lootService.GetLootResult(resourceNode.LootTableId);
                }

                if(lootEntry == null)
                {
                    continue;
                }

                var itemStack = result.Items.FirstOrDefault(i => i.Id == lootEntry.TargetItemId);
                var amount = lootEntry.TargetItemAmount ?? 1;
                if (itemStack == null)
                {
                    result.Items.Add(new ItemStackDto
                    {
                        Id = lootEntry.TargetItem.Id,
                        Name = lootEntry.TargetItem.Name,
                        Amount = amount,
                        TotalWeight = (int) Math.Ceiling(lootEntry.TargetItem.Weight * (amount))
                    }); ;
                }
                else
                {
                    itemStack.Amount += amount;
                    itemStack.TotalWeight = (int)Math.Ceiling(lootEntry.TargetItem.Weight * (itemStack.Amount));
                }
            }
            return result;
        }

        private async Task GiveCharacterItems(GatherResultDto resultDto)
        {
            foreach(var item in resultDto.Items)
            {
                await _inventoryService.GiveItem(resultDto.Character.Id, item.Id, item.Amount);
            }
        }

        public async Task<GatherStartDto> StartGathering(Character character)
        {
            var subLocation = _context.SubLocation
                .Include(sl => sl.ResourceNode)
                .FirstOrDefault(sl => sl.Id == character.CurrentSubLocationId);

            var gameTimeSpeedMultiplier = _configuration.GetValue<double>("GameTimeSpeedMultiplier");
            var gatherTimeSpan = new TimeSpan(2, 0, 0).Divide(gameTimeSpeedMultiplier);
            var finishTime = DateTime.UtcNow.Add(gatherTimeSpan);

            _context.CharacterGatherTime.Add(new CharacterGatherTime
            {
                CharacterId = character.Id,
                ResourceNodeId = subLocation.ResourceNode.Id,
                FinishTime = finishTime
            });
            await _context.SaveChangesAsync();

            return new GatherStartDto
            {
                LocationName = subLocation.Name,
                Duration = gatherTimeSpan
            };
        }
    }
}
