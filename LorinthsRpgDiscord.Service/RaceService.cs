﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class RaceService : IRaceService
    {
        private readonly DiscordRpgContext _context;
        public RaceService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<Race> Get(int id)
        {
            return await _context.Race
                .Include(r => r.StartingCoreAttributes)
                .Include(r => r.RaceStartingLocation)
                .ThenInclude(rsl => rsl.Location)
                .ThenInclude(l => l.SubLocation)
                .SingleOrDefaultAsync(r => r.Id == id);
        }

        public async Task<IEnumerable<Race>> GetAll()
        {
            return await _context.Race.Where(c => c.IsActive == true).ToListAsync();
        }

        public async Task<IEnumerable<Class>> GetRaceAllowedClasses(int raceId)
        {
            var race = await _context.Race
                .Include(r => r.RaceClasses)
                .ThenInclude(r => r.Class).FirstOrDefaultAsync(r => r.Id == raceId);

            return race.RaceClasses.Select(rc => rc.Class);
        }

        public async Task<IEnumerable<Location>> GetRaceStartingLocations(int raceId)
        {
            var race = await _context.Race
                .Include(r => r.RaceStartingLocation)
                .ThenInclude(r => r.Location)
                .ThenInclude(l => l.Region)
                .ThenInclude(r => r.Continent)
                .FirstOrDefaultAsync(r => r.Id == raceId);

            return race.RaceStartingLocation.Select(rsl => rsl.Location);
        }

        public async Task<IEnumerable<LookupDto>> GetAllLookups()
        {
            return await _context.Race.Select(r => new LookupDto
            {
                Id = r.Id,
                Name = r.Name
            }).ToListAsync();
        }
    }
}
