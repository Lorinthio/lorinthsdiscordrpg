﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class CharacterService : ICharacterService
    {
        private readonly DiscordRpgContext _context;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _config;

        public CharacterService(
            DiscordRpgContext context,
            IServiceProvider serviceProvider,
            IConfiguration config)
        {
            _context = context;
            _serviceProvider = serviceProvider;
            _config = config;
        }

        public async Task<Character> CreateCharacter(int playerId)
        {
            try
            {
                var character = _context.Character.FirstOrDefault(c => c.PlayerId == playerId && c.Created == false);
                if (character == null)
                {
                    character = new Character
                    {
                        Id = Guid.NewGuid(),
                        PlayerId = playerId,
                        ClassId = 1,
                        RaceId = 1,
                        CraftProfessionId = 1,
                        GatherProfessionId = 1,
                        Bio = "Use !bio to set this characters bio once you are finished with creation!"
                    };
                    _context.Add(character);
                }

                var player = _context.Player.Single(p => p.Id == playerId);
                player.CurrentCharacter = character;
                
                _context.Update(player);
                await _context.SaveChangesAsync();
                return character;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Task<Character> GetCharacter(Guid id)
        {
            return Task.FromResult(
                _context.Character
                .Include(c => c.CoreAttributes)
                .Include(c => c.Race)
                .Include(c => c.Class)
                .Include(c => c.CraftProfession)
                .Include(c => c.GatherProfession)
                .FirstOrDefault(c => c.Id == id));
        }

        public async Task UpdateCharacter(Character updated)
        {
            using(DiscordRpgContext context = (DiscordRpgContext) _serviceProvider.GetService(typeof(DiscordRpgContext)))
            {
                context.Update(updated);
                await context.SaveChangesAsync();
            }
        }

        public async Task<CharacterBusyDto> IsCharacterBusy(Guid id)
        {
            var characterBusy = new CharacterBusyDto
            {
                IsCrafting = false,
                IsGathering = await IsCharacterGathering(id),
                IsTravelling = await IsCharacterTravelling(id)
            };

            return characterBusy;
        }

        private async Task<bool> IsCharacterTravelling(Guid id)
        {
            var character = await _context.Character
                .Include(c => c.CharacterTravelTime)
                .FirstOrDefaultAsync(c => c.Id == id);

            return character.CharacterTravelTime != null;
        }

        private async Task<bool> IsCharacterGathering(Guid id)
        {
            var character = await _context.Character
                .Include(c => c.CharacterGatherTime)
                .FirstOrDefaultAsync(c => c.Id == id);

            return character.CharacterGatherTime != null;
        }
    }
}
