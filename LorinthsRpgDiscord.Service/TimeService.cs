﻿using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LorinthsRpgDiscord.Service
{
    public class TimeService : ITimeService
    {
        private readonly IConfiguration _config;
        public TimeService(
            IConfiguration config)
        {
            _config = config;
        }

        public GameClock GetTime()
        {
            var gameDaysPerRealDay = _config.GetValue<int>("GameTimeSpeedMultiplier");
            var minutesPerGameDay = (24 * 60) / gameDaysPerRealDay;

            var now = DateTime.UtcNow;
            var currentMinutes = (now.Hour * 60 + now.Minute) % minutesPerGameDay;
            var gameSeconds = now.Second * gameDaysPerRealDay;
            var gameMinutes = (currentMinutes * gameDaysPerRealDay) + (gameSeconds / 60);
            var gameHour = (gameMinutes / 60);

            //Remove extra seconds/minutes
            gameSeconds %= 60;
            gameMinutes %= 60;
            var time = new TimeSpan(gameHour, gameMinutes, gameSeconds);

            var color = Color.Blue;
            TimeOfDay timeOfDay = TimeOfDay.Midnight;
            if (time.Hours >= 21)
            {
                timeOfDay = TimeOfDay.Night;
                color = Color.DarkCyan;
            }
            else if (time.Hours >= 18)
            {
                timeOfDay = TimeOfDay.Evening;
                color = Color.Orange;
            }
            else if (time.Hours >= 12)
            {
                timeOfDay = TimeOfDay.Afternoon;
                color = Color.LightBlue;
            }
            else if (time.Hours >= 6)
            {
                timeOfDay = TimeOfDay.Morning;
                color = Color.Yellow;
            }
            else if (time.Hours >= 0)
            {
                timeOfDay = TimeOfDay.Midnight;
                color = Color.DarkSeaGreen;
            }

            return new GameClock
            {
                Time = time,
                Color = color,
                TimeOfDay = timeOfDay
            };
        }
    }
}
