﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LorinthsRpgDiscord.Service.Extensions
{
    public static class DistinctByExtension
    {
        public static IEnumerable<T> DistinctBy<T,K>(this IEnumerable<T> list, Func<T,K> selector)
        {
            var distinctKeys = list.Select(selector).Distinct();
            var distinctList = new List<T>();
            foreach(var key in distinctKeys)
            {
                distinctList.Add(list.FirstOrDefault(item => selector(item).Equals(key)));
            }

            return distinctList;
        }
        public static IEnumerable<T> DistinctBy<T, K>(this IEnumerable<T> list, Func<T, K> selector, Func<T,K> secondSelector)
        {
            var distinctKeys = list.GroupBy(o => new { A = selector(o), B = secondSelector(o) });
            var distinctList = new List<T>();
            foreach (var distinctKey in distinctKeys)
            {
                var item = list.FirstOrDefault(o =>
                    (selector(o).Equals(distinctKey.Key.A) || (selector(o) == null && distinctKey.Key.A == null)) &&
                    (secondSelector(o).Equals(distinctKey.Key.B) || (secondSelector(o) == null && distinctKey.Key.B == null)));
                if(item != null)
                {
                    distinctList.Add(item);
                }
            }

            return distinctList;
        }
    }
}
