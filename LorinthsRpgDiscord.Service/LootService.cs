﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class LootService : ILootService
    {
        private readonly DiscordRpgContext _context;
        private readonly Random random = new Random();
        public LootService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<LootTableEntry> GetLootResult(int lootTableId)
        {
            var lootTable = await _context.LootTable
                .Include(lt => lt.LootTableEntry)
                .ThenInclude(lt => lt.TargetItem)
                .FirstOrDefaultAsync(lt => lt.Id == lootTableId);

            return await GetLootResult(lootTable);
        }

        private async Task<LootTableEntry> GetLootResult(LootTable lootTable)
        {
            decimal roll = (decimal) random.NextDouble() * 100m;
            var current = 0m;
            foreach(var entry in lootTable.LootTableEntry)
            {
                current += entry.Chance;

                if(roll < current)
                {
                    return entry;
                }
            }

            return null;
        }
    }
}
