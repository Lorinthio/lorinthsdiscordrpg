﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class QuestService : IQuestService
    {
        private readonly DiscordRpgContext _context;
        public QuestService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<List<LookupDto>> GetGoalTypes()
        {
            var lookups = await _context.QuestStepObjectiveType.Select(qgt => new LookupDto
            {
                Id = qgt.Id,
                Name = qgt.Name
            }).ToListAsync();

            return lookups;
        }

        public async Task<Quest> GetQuest(int id)
        {
            var quest = await _context.Quest
                .Include(q => q.QuestStep)
                .ThenInclude(qs => qs.QuestStepObjective)
                .SingleOrDefaultAsync(q => q.Id == id);
            if(quest == null)
            {
                quest = new Quest();
            }
            
            return quest;
        }

        public async Task<List<LookupDto>> GetQuestLookups()
        {
            var lookups = await _context.Quest.Select(q => new LookupDto
            {
                Id = q.Id,
                Name = q.Name
            }).ToListAsync();

            return lookups;
        }

        public async Task<Quest> SaveQuest(Quest quest)
        {
            var index = 1;
            foreach(var questStep in quest.QuestStep)
            {
                questStep.Order = index;
                index++;
            }

            try
            {
                if (quest.Id == default(int))
                {
                    _context.Quest.Add(quest);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    _context.Quest.Update(quest);
                    await _context.SaveChangesAsync();
                    await CleanupBrokenQuestRelationships(quest);
                }

                return quest;
            } 
            catch(Exception ex)
            {
                throw;
            }
        }

        private async Task CleanupBrokenQuestRelationships(Quest quest)
        {
            var savedQuestSteps = await _context.QuestStep
                .Where(qs => qs.QuestId == quest.Id).AsNoTracking().ToListAsync();

            foreach (var savedQS in savedQuestSteps)
            {
                //If the saved step doesn't exist in the updated Quest 
                //  remove that step and related objectives
                var updatedStep = quest.QuestStep
                    .FirstOrDefault(qs => qs.Id == savedQS.Id);
                if (updatedStep == null)
                {
                    _context.QuestStep.Remove(savedQS);
                    _context.QuestStepObjective
                        .RemoveRange(savedQS.QuestStepObjective);

                    continue;
                }

                var savedQuestObjectives = await _context.QuestStepObjective
                    .Where(qso => qso.QuestStepId == savedQS.Id).AsNoTracking().ToListAsync();
                foreach (var savedQO in savedQuestObjectives)
                {
                    //If the saved objective doesn't exist in the updated Quest 
                    //  remove that objective
                    var updatedObjective = updatedStep.QuestStepObjective
                        .FirstOrDefault(qso => qso.Id == savedQO.Id);

                    if (updatedObjective == null)
                    {
                        _context.QuestStepObjective.Remove(savedQO);
                    }
                }
            }
            await _context.SaveChangesAsync();
        }

        public async Task<List<Quest>> SearchQuests(string searchText)
        {
            try
            {
                var quests = await _context.Quest.Where(q => q.Name.Contains(searchText)).ToListAsync();
                return quests;
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
