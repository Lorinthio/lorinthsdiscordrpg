﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class ProfessionService : IProfessionService
    {
        private readonly DiscordRpgContext _context;
        public ProfessionService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<CraftProfession> GetCraftProfession(int id)
        {
            return await _context.CraftProfession.FirstOrDefaultAsync(prof => prof.Id == id);
        }

        public async Task<IEnumerable<CraftProfession>> GetCraftProfessions()
        {
            return await _context.CraftProfession.ToListAsync();
        }

        public async Task<GatherProfession> GetGatherProfession(int id)
        {
            return await _context.GatherProfession
                .Include(gp => gp.GatheringProfessionStartingTool)
                .ThenInclude(st => st.StartingItem)
                .FirstOrDefaultAsync(prof => prof.Id == id);
        }

        public async Task<IEnumerable<GatherProfession>> GetGatherProfessions()
        {
            return await _context.GatherProfession.ToListAsync();
        }
    }
}
