﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class ClassService : IClassService
    {
        private readonly DiscordRpgContext _context;
        public ClassService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<Class> Get(int id)
        {
            return await _context.Class.SingleOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Class>> GetAll()
        {
            return await _context.Class.Where(c => c.IsActive).ToListAsync();
        }

        public async Task<IEnumerable<LookupDto>> GetAllLookups()
        {
            return await _context.Class.Select(c => new LookupDto
            {
                Id = c.Id,
                Name = c.Name
            }).ToListAsync();
        }
    }
}
