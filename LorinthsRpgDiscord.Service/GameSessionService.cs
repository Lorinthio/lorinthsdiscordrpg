﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class GameSessionService : IGameSessionService
    {
        private readonly IConfiguration _config;

        public GameSessionService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<GameSession> CreateGameSession(int playerId)
        {
            GameSession gameSession = null;
            try
            {
                using (var context = new DiscordRpgThreadedContext(_config))
                {
                    gameSession = new GameSession
                    {
                        Id = Guid.NewGuid(),
                        MapName = "Guard_Room",
                        EncounterName = "",
                    };
                    context.GameSession.Add(gameSession);

                    var gameSessionPlayer = new GameSessionPlayer
                    {
                        GameSession = gameSession,
                        PlayerId = playerId
                    };
                    context.GameSessionPlayer.Add(gameSessionPlayer);
                    await context.SaveChangesAsync();
                }

                return gameSession;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Task<bool> GameSessionIncludesPlayer(Guid sessionId, int playerId)
        {
            using(var context = new DiscordRpgThreadedContext(_config))
            {
                return context.GameSessionPlayer
                    .AnyAsync(gsp => gsp.GameSessionId == sessionId && gsp.PlayerId == playerId);
            }
        }
    }
}
