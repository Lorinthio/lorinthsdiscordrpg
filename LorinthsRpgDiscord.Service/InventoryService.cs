﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class InventoryService : IInventoryService
    {
        private readonly DiscordRpgContext _context;
        public InventoryService(
            DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<InventoryDto> GetCharacterInventory(Guid characterId)
        {
            var inventory = new InventoryDto();

            var allInventoryItems = await _context.CharacterInventory
                .Include(ci => ci.Item)
                .ThenInclude(i => i.Armor)
                .Include(ci => ci.Item)
                .ThenInclude(i => i.CraftTool)
                .Include(ci => ci.Item)
                .ThenInclude(i => i.GatherTool)
                .Include(ci => ci.Item)
                .ThenInclude(i => i.Weapon)
                .Where(ci => ci.CharacterId == characterId)
                .ToListAsync();

            foreach(var item in allInventoryItems)
            {
                var itemStack = new ItemStackDto
                {
                    Id = item.Item.Id,
                    Name = item.Item.Name,
                    TotalWeight = (int) (item.Item.Weight * item.Amount),
                    Amount = item.Amount
                };
                if (item.Item.Armor != null)
                {
                    inventory.Armor.Add(itemStack);
                }
                else if (item.Item.CraftTool != null)
                {
                    inventory.CraftTools.Add(itemStack);
                }
                else if (item.Item.GatherTool != null)
                {
                    inventory.GatherTools.Add(itemStack);
                }
                else if (item.Item.Weapon != null)
                {
                    inventory.Weapons.Add(itemStack);
                }
                else
                {
                    inventory.Items.Add(itemStack);
                }

                inventory.TotalValue += itemStack.TotalWeight;
                inventory.TotalWeight += itemStack.Amount;
            }

            return inventory;
        }

        public async Task GiveItem(Guid characterId, int itemId, int amount)
        {
            using(var context = new DiscordRpgContext())
            {
                var inventoryItem = await context.CharacterInventory
                    .SingleOrDefaultAsync(ci => ci.CharacterId == characterId && ci.ItemId == itemId);

                if (inventoryItem == null)
                {
                    context.Add(new CharacterInventory
                    {
                        CharacterId = characterId,
                        ItemId = itemId,
                        Amount = amount
                    });
                }
                else
                {
                    inventoryItem.Amount += amount;
                    context.Update(inventoryItem);
                }

                await context.SaveChangesAsync();
            }
        }

        public Task TakeItem(Guid characterId, int itemId, int amount)
        {
            throw new NotImplementedException();
        }
    }
}
