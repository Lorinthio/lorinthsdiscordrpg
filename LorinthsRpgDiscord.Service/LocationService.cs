﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Dto.Options;
using LorinthsRpgDiscord.Service.Extensions;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class LocationService : ILocationService
    {
        private readonly DiscordRpgContext _context;
        private readonly IConfiguration _configuration;
        public LocationService(DiscordRpgContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<Location> GetLocation(int id)
        {
            var location = await _context.Location
                .Include(l => l.SubLocation)
                .Include(l => l.Region)
                .ThenInclude(r => r.Continent)
                .Include(l => l.LocationType)
                .FirstOrDefaultAsync(l => l.Id == id);
            return location;
        }

        public async Task<Location> SaveLocation(Location model)
        {
            if(model.Id == default(int))
            {
                _context.Location.Add(model);
            }
            else
            {
                var location = _context.Location.FirstOrDefault(l => l.Id == model.Id);

                location.Name = model.Name;
                location.Description = model.Description;
                location.LocationTypeId = model.LocationTypeId;
                location.RegionId = model.RegionId;
                location.IsActive = model.IsActive;

                model = location;
            }
            await _context.SaveChangesAsync();

            return model;
        }

        public async Task<Region> SaveRegion(Region model)
        {
            if (model.Id == default(int))
            {
                _context.Region.Add(model);
            }
            else
            {
                var region = _context.Region.FirstOrDefault(r => r.Id == model.Id);

                region.Name = model.Name;
                region.Description = model.Description;
                region.ContinentId = model.ContinentId;
                region.IsActive = model.IsActive;

                model = region;
            }
            await _context.SaveChangesAsync();

            return model;
        }

        public async Task<Continent> SaveContinent(Continent model)
        {
            if (model.Id == default(int))
            {
                _context.Continent.Add(model);
            }
            else
            {
                var continent = _context.Continent.FirstOrDefault(c => c.Id == model.Id);

                continent.Name = model.Name;
                continent.Description = model.Description;
                continent.IsActive = model.IsActive;

                model = continent;
            }
            await _context.SaveChangesAsync();

            return model;
        }

        public async Task<IEnumerable<LookupDto>> GetLocationTypes()
        {
            return await _context.LocationType.Select(lt => new LookupDto
            {
                Id = lt.Id,
                Name = lt.Name
            }).ToListAsync();
        }

        public async Task<string> GetLocationBreadcrumb(int locationId, int? subLocationId)
        {
            var location = await _context.Location
                .Include(l => l.SubLocation)
                .Include(l => l.Region)
                .ThenInclude(r => r.Continent)
                .FirstOrDefaultAsync(l => l.Id == locationId);

            return await GetLocationBreadcrumb(location, subLocationId);
        }

        public async Task<string> GetLocationBreadcrumb(Location location, int? subLocationId)
        {
            if(location == null)
            {
                return await Task.FromResult("");
            }

            var breadCrumb = $"{location.Region.Continent.Name} > {location.Region.Name} > {location.Name}";
            if (subLocationId == null)
            {
                return breadCrumb;
            }

            var subLocation = location.SubLocation.FirstOrDefault(sl => sl.Id == subLocationId);
            if(subLocation == null)
            {
                return breadCrumb;
            }

            return $"{breadCrumb} - {subLocation?.Name}";
        }
    }
}
