﻿using LorinthsRpgDiscord.Service.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Service
{
    public class LoggingService : ILoggingService
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public LoggingService()
        {

        }

        public void LogDebug(string message)
        {
            logger.Debug(message);
        }
        public void LogError(string message)
        {
            logger.Error(message);
        }
        public void LogError(Exception ex, string message)
        {
            logger.Error(ex, message);
        }
        public void LogInformation(string message)
        {
            logger.Info(message);
        }
        public void LogWarning(string message)
        {
            logger.Warn(message);
        }
    }
}
