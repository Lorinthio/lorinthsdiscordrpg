﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Exceptions;
using LorinthsRpgDiscord.Dto.Security;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class SecurityService : ISecurityService
    {
        private readonly DiscordRpgContext _context;

        public SecurityService(DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<Guid> CreateSession(int playerId, string ipAddress)
        {
            var id = Guid.NewGuid();
            _context.WebSession.Add(new WebSession
            {
                Id = id,
                PlayerId = playerId,
                IpAddress = ipAddress,
                Expiration = DateTime.Now.AddDays(7)
            });

            await _context.SaveChangesAsync();
            return id;
        }

        public async Task<User> GetCurrentUser(Guid sessionId, string ipAddress)
        {
            var webSession = await GetCurrentWebSession(sessionId, ipAddress);
            if(webSession == null)
            {
                return null;
            }

            var roles = await GetPlayerPermissions(webSession.PlayerId);
            return new User
            {
                Id = webSession.Player.UserId,
                Username = webSession.Player.Username,
                Avatar = webSession.Player.Avatar,
                Roles = roles.ToArray()
            };
        }

        public async Task<Player> GetCurrentPlayer(Guid sessionId, string ipAddress)
        {
            var webSession = await GetCurrentWebSession(sessionId, ipAddress);
            if (webSession == null)
            {
                return null;
            }

            return webSession.Player;
        }

        private async Task<WebSession> GetCurrentWebSession(Guid sessionId, string ipAddress)
        {
            var webSession = await _context.WebSession
                .Include(s => s.Player)
                .FirstOrDefaultAsync(s => s.Id == sessionId); // && s.IpAddress == ipAddress);

            if (webSession == null)
            {
                return null;
            }

            if (webSession.Expiration < DateTime.Now)
            {
                await Logout(sessionId);
                throw new SessionExpiredException(webSession);
            }

            return webSession;
        }

        public async Task<IEnumerable<string>> GetPlayerPermissions(int playerId)
        {
            var roleIds = await _context.PlayerRole
                .Where(pr => pr.PlayerId == playerId)
                .Select(pr => pr.RoleId)
                .ToListAsync();
            var permissionNames = await _context.RoleWebsitePermission
                .Include(rwp => rwp.WebsitePermission)
                .Where(rwp => roleIds.Contains(rwp.RoleId))
                .Select(rwp => rwp.WebsitePermission.Name)
                .ToListAsync();

            return permissionNames;
        }

        public async Task<bool> Logout(Guid sessionId)
        {
            var webSession = await _context.WebSession
                .Include(s => s.Player)
                .FirstOrDefaultAsync(s => s.Id == sessionId);

            if(webSession == null)
            {
                return false;
            }

            _context.Remove(webSession);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
