﻿using LorinthsRpgDiscord.Data;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class PlayerService : IPlayerService
    {
        private DiscordRpgContext _context;

        public PlayerService(
            DiscordRpgContext context)
        {
            _context = context;
        }

        public async Task<Player> GetPlayer(string id)
        {
            return await _context.Player.SingleOrDefaultAsync(p => p.UserId == id.ToString());
        }

        public async Task<Player> GetOrCreatePlayer(string id)
        {
            return await EnsurePlayer(id, null);
        }

        public async Task<Player> GetOrCreatePlayer(string id, string username)
        {
            return await EnsurePlayer(id, username);
        }

        private async Task<Player> EnsurePlayer(string userId, string username)
        {
            try
            {
                var player = _context.Player
                    .Include(p => p.CurrentCharacter)
                    .FirstOrDefault(p => p.UserId == userId);

                if (player == null)
                {
                    player = new Player
                    {
                        UserId = userId.ToString(),
                        Username = username ?? ""
                    };
                    _context.Player.Add(player);
                    await _context.SaveChangesAsync();
                }
                return player;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    
        public  async Task<Player> UpdatePlayer(DiscordUserDto userModel)
        {
            var player = await GetOrCreatePlayer(userModel.Id, userModel.Username);

            if (player.Username != userModel.Username)
            {
                player.Username = userModel.Username;
            }
            if (player.Avatar != userModel.Avatar)
            {
                player.Avatar = userModel.Avatar;
            }

            if (userModel.Roles != null)
            {
                await UpdateRoles(player.Id, userModel);
            }
            _context.Update(player);
            await _context.SaveChangesAsync();
            return player;
        }

        public async Task UpdateRoles(int playerId, DiscordUserDto userModel)
        {
            var userRoles = await _context.PlayerRole
                .Where(pr => pr.PlayerId == playerId && pr.IsOverride == false)
                .ToListAsync();
            _context.RemoveRange(userRoles);

            var overrideRoles = await _context.PlayerRole
                .Where(pr => pr.PlayerId == playerId && pr.IsOverride == true)
                .ToListAsync();

            var usedRoles = await _context.Role.Select(r => r.Id).ToListAsync();
            foreach (var role in userModel.Roles)
            {
                if(!overrideRoles.Any(or => or.RoleId == role.Id) &&
                    usedRoles.Contains(role.Id.ToString()))
                {
                    _context.PlayerRole.Add(new PlayerRole
                    {
                        PlayerId = playerId,
                        RoleId = role.Id.ToString()
                    });
                }
            }
        }
    }
}
