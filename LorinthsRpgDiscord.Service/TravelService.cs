﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Dto.Options;
using LorinthsRpgDiscord.Service.Extensions;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Service
{
    public class TravelService : ITravelService
    {
        private readonly DiscordRpgContext _context;
        private readonly IConfiguration _configuration;
        public TravelService(DiscordRpgContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<IEnumerable<TravelLocationDto>> GetTravelLocations(int locationId, int? subLocationId)
        {
            return await GetLocationsInDistance(locationId, subLocationId, 50);
        }

        public async Task<IEnumerable<TravelLocationDto>> GetWalkingLocations(int locationId, int? subLocationId)
        {
            return await GetLocationsInDistance(locationId, subLocationId, 0);
        }

        public async Task<IEnumerable<Location>> SearchLocations(string search)
        {
            var locations = await _context.Location
                .Where(l => l.Name.Contains(search)).ToListAsync();
            return locations;
        }

        private async Task<IEnumerable<TravelLocationDto>> GetLocationsInDistance(int locationId, int? subLocationId, decimal distance)
        {
            List<TravelLocationDto> locations = new List<TravelLocationDto>();
            if (subLocationId != null)
            {
                var subLocation = await _context.SubLocation
                    .Include(sl => sl.Location).FirstOrDefaultAsync(sl => sl.Id == subLocationId);

                var subLocations = await _context.SubLocation
                    .Include(sl => sl.Location).Where(sl => sl.LocationId == locationId).ToListAsync();
                locations = subLocations.Select(subLocation => new TravelLocationDto
                {
                    Location = subLocation.Location,
                    SubLocation = subLocation,
                    DistanceInMiles = 0,
                    TravelTime = null
                }).ToList();

                if (!subLocation.CanTravel)
                {
                    return locations;
                }
            }

            var firstLocations = await _context.LocationDistance
                .Include(ld => ld.FirstLocation)
                .Where(ld => ld.SecondLocationId == locationId && (ld.DistanceInMiles == null || ld.DistanceInMiles <= distance))
                .Select(ld => new TravelLocationDto { Location = ld.FirstLocation, DistanceInMiles = ld.DistanceInMiles }).ToListAsync();
            var secondLocations = await _context.LocationDistance
                .Include(ld => ld.SecondLocation)
                .Where(ld => ld.FirstLocationId == locationId && (ld.DistanceInMiles == null || ld.DistanceInMiles <= distance))
                .Select(ld => new TravelLocationDto { Location = ld.SecondLocation, DistanceInMiles = ld.DistanceInMiles }).ToListAsync();

            locations.AddRange(firstLocations);
            locations.AddRange(secondLocations);

            locations = locations.DistinctBy(
                    l => l.Location?.Id, 
                    l => l.SubLocation?.Id)
                .OrderBy(l => l.DistanceInMiles)
                .ThenBy(l => l.Location.Name)
                .ThenBy(l => l.SubLocation?.Name).ToList();

            return locations.Where(l => l.Location.Id != locationId || l.SubLocation.Id != subLocationId);
        }

        public async Task<CharacterTravelTime> GetCharacterTravelTime(Guid characterId)
        {
            return await _context.CharacterTravelTime
                .Include(ctt => ctt.Location)
                .FirstOrDefaultAsync(ctt => ctt.CharacterId == characterId);
        }

        public async Task<TimeSpan> CharacterTravel(Character character, int locationId, int? subLocationId, TravelTypeEnum travelType)
        {
            if(locationId == character.CurrentLocationId)
            {
                character.CurrentLocationId = locationId;
                character.CurrentSubLocationId = subLocationId;
                _context.Update(character);
                await _context.SaveChangesAsync();
                return TimeSpan.Zero;
            }

            var distance = await _context.LocationDistance
                .FirstOrDefaultAsync(d =>
                    (d.FirstLocationId == character.CurrentLocationId && d.SecondLocationId == locationId) ||
                    (d.SecondLocationId == character.CurrentLocationId && d.FirstLocationId == locationId));

            if (distance.DistanceInMiles == null)
            {
                return TimeSpan.Zero;
            }

            var gameTimeSpeedMultiplier = _configuration.GetValue<decimal>("GameTimeSpeedMultiplier");
            var speed = GetTravelSpeed(travelType);
            var durationInHours = (distance.DistanceInMiles ?? 0) / speed;
            durationInHours /= gameTimeSpeedMultiplier;

            var days = (int)(durationInHours / 24);
            durationInHours %= 24;
            var hours = (int)(durationInHours / 1);
            durationInHours %= 1;

            var durationInMinutes = durationInHours * 60;
            var minutes = (int)(durationInMinutes / 1);

            durationInMinutes -= minutes;

            var durationInSeconds = durationInMinutes * 60;
            var seconds = (int)(durationInSeconds / 1);

            var timeSpan = new TimeSpan(days, hours, minutes, seconds);
            var arrived = DateTime.UtcNow.Add(timeSpan);

            _context.CharacterTravelTime.Add(new CharacterTravelTime
            {
                CharacterId = character.Id,
                LocationId = locationId,
                ArrivalTime = arrived
            });
            await _context.SaveChangesAsync();

            return timeSpan;
        }

        public async Task<TimeSpan> CharacterTravel(Guid characterId, int locationId, int? subLocationId, TravelTypeEnum travelType)
        {
            var character = await _context.Character.FirstOrDefaultAsync(c => c.Id == characterId);

            return await CharacterTravel(character, locationId, subLocationId, travelType);
        }

        private decimal GetTravelSpeed(TravelTypeEnum travelType)
        {
            var travelOptions = new TravelOptions();
            _configuration.GetSection(TravelOptions.Travel).Bind(travelOptions);

            switch (travelType)
            {
                case TravelTypeEnum.Foot:
                    return travelOptions.Foot;
                case TravelTypeEnum.Horse:
                    return travelOptions.Horse;
                case TravelTypeEnum.Boat:
                    return travelOptions.Boat;
            }

            return travelOptions.Foot;
        }

        public async Task<IEnumerable<CharacterTravelTime>> GetCompletedTravelTimes()
        {
            var completed = await _context.CharacterTravelTime
                .Include(ctt => ctt.Location)
                .ThenInclude(l => l.SubLocation)
                .Include(ctt => ctt.Character)
                .ThenInclude(c => c.Player)
                .Where(ctt => ctt.ArrivalTime <= DateTime.UtcNow).ToListAsync();

            if (completed.Count == 0)
            {
                return null;
            }

            foreach (var complete in completed)
            {
                complete.Character.CurrentLocationId = complete.LocationId;
                complete.Character.CurrentSubLocationId = complete.Location.SubLocation.FirstOrDefault(sl => sl.IsStartingSubLocation)?.Id;
                _context.Update(complete.Character);
            }

            _context.CharacterTravelTime.RemoveRange(completed);
            await _context.SaveChangesAsync();

            return completed;
        }
    }
}
