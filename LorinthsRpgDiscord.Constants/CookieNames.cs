﻿using System;

namespace LorinthsRpgDiscord.Constants
{
    public class CookieNames
    {
        private const string CookieBase = "DiscordRpg_";

        public const string GameSessionCookie = CookieBase + "GameSessionId";
        public const string WebSessionCookie = CookieBase + "SessionId";
    }
}
