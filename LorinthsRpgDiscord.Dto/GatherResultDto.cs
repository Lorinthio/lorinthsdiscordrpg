﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class GatherResultDto
    {
        public Character Character { get; set; }
        public Player Player { get; set; }
        public int CritAmount { get; set; }
        public List<ItemStackDto> Items { get; set; }
    }
}
