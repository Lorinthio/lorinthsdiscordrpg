﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Exceptions
{
    public class SessionExpiredException : Exception
    {
        public SessionExpiredException(WebSession webSession) : base($"Web session with Id {webSession.Id} has expired for player ${webSession.PlayerId}")
        {

        }
    }
}
