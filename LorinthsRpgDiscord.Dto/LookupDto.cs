﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class LookupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
