﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    [AirTableName(airTableName: "Item", identityInsert: true)]
    public class ItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public decimal Weight { get; set; }

        public void Update(Item item)
        {
            if(item.Id == default(int))
            {
                item.Id = Id;
            }
            item.Name = Name;
            item.Description = Description ?? "";
            item.Value = Value;
            item.Weight = Weight;
        }
    }
}
