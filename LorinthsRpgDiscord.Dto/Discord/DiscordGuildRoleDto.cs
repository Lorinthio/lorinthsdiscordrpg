﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Discord
{
    public class DiscordGuildRoleDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
