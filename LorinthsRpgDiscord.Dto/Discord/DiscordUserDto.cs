﻿using LorinthsRpgDiscord.Dto.Discord;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class DiscordUserDto
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int Discriminator { get; set; }
        public string Avatar { get; set; }
        public string Token { get; set; }
        public IEnumerable<DiscordGuildRoleDto> Roles { get; set; }
    }
}
