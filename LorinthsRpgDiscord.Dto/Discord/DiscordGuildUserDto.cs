﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Discord
{
    public class DiscordGuildUserDto
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
    }
}
