﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Enums
{
    public enum TimeOfDay
    {
        Midnight,
        Morning,
        Afternoon,
        Evening,
        Night
    }
}
