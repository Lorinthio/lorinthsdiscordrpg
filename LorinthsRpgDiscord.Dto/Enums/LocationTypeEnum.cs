﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Enums
{
    public enum LocationTypeEnum
    {
        POI = 1,
        City,
        Village,
        Dungeon,
        Field,
        Forest,
        Ocean,
        Underground,
        Outpost
    }
}
