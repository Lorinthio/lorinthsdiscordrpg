﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Enums
{
    public enum TravelTypeEnum
    {
        Foot, Horse, Boat
    }
}
