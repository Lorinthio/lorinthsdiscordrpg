﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class GatherStartDto
    {
        public string LocationName { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
