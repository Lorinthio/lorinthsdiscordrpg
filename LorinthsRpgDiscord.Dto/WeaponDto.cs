﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    [AirTableName("Weapon")]
    public class WeaponDto
    {
        public int ItemId { get; set; }
        public int Power { get; set; }
        public int Parry { get; set; }
        public int Reach { get; set; }
        public int ActionCost { get; set; }

        public void Update(Weapon weapon)
        {
            if (weapon.ItemId == default(int))
            {
                weapon.ItemId = ItemId;
            }
            weapon.Power = Power;
            weapon.Parry = Parry;
            weapon.Reach = Reach;
            weapon.ActionCost = ActionCost;
        }
    }
}
