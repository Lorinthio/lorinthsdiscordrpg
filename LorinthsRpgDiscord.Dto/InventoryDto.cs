﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class InventoryDto
    {
        public List<ItemStackDto> Armor;
        public List<ItemStackDto> CraftTools;
        public List<ItemStackDto> GatherTools;
        public List<ItemStackDto> Items;
        public List<ItemStackDto> Weapons;
        public decimal TotalValue = 0;
        public decimal TotalWeight = 0;

        public InventoryDto()
        {
            Armor = new List<ItemStackDto>();
            CraftTools = new List<ItemStackDto>();
            GatherTools = new List<ItemStackDto>();
            Items = new List<ItemStackDto>();
            Weapons = new List<ItemStackDto>();
        }
    }
}
