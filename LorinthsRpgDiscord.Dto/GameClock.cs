﻿using LorinthsRpgDiscord.Dto.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class GameClock
    {
        public TimeSpan Time { get; set; }
        public TimeOfDay TimeOfDay { get; set; }
        public Color Color { get; set; }
    }
}
