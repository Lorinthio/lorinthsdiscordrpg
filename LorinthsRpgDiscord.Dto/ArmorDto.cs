﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    [AirTableName("Armor")]
    public class ArmorDto
    {
        public int ItemId { get; set; }
        public int Defense { get; set; }
        public int MagicDefense { get; set; }
        public int Dodge { get; set; }
        public int ArmorSlotId { get; set; }

        public void Update(Armor armor)
        {
            if (armor.ItemId == default(int))
            {
                armor.ItemId = ItemId;
            }
            armor.Defense = Defense;
            armor.MagicDefense = MagicDefense;
            armor.Dodge = Dodge;
            armor.ArmorSlotId = ArmorSlotId;
        }
    }
}
