﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Options
{
    public class TravelOptions
    {
        public const string Travel = "Travel";

        public decimal Foot { get; set; }
        public decimal Horse { get; set; }
        public decimal Boat { get; set; }
    }
}
