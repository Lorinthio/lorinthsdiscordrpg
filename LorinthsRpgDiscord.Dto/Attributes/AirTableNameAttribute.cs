﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AirTableNameAttribute : Attribute
    {
        public string AirTableName { get; }
        public string DbTableName { get; }
        public bool IdentityInsert { get; }

        public AirTableNameAttribute(string airTableName, string dbTableName = null, bool identityInsert = false)
        {
            AirTableName = airTableName;
            DbTableName = dbTableName ?? airTableName;
            IdentityInsert = identityInsert;
        }
    }
}
