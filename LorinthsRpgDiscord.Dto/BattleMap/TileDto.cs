﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.BattleMap
{
    public class TileDto : Vector
    {
        public int Id { get; set; }
        public bool Blocked { get; set; }
    }
}
