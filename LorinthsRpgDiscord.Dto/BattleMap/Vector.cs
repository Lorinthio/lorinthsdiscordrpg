﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.BattleMap
{
    public class Vector
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
