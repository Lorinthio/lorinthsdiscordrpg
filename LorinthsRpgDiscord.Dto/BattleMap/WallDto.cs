﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.BattleMap
{
    public class WallDto
    {
        public int Id { get; set; }
        public Vector A { get; set; }
        public Vector B { get; set; }
    }
}
