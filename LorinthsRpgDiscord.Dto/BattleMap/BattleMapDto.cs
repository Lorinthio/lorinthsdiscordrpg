﻿using LorinthsRpgDiscord.Dto.BattleMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto.BattleMap
{
    public class BattleMapDto
    {
        public string Name { get; set; }
        public string BackgroundImageUrl { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Vector Offset { get; set; }
        public List<TileDto> Blocks { get; set; }
        public List<WallDto> Walls { get; set; }
    }
}
