﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class CharacterBusyDto
    {
        public bool IsCrafting { get; set; }
        public bool IsGathering { get; set; }
        public bool IsTravelling { get; set; }
    }
}
