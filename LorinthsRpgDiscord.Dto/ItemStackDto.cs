﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class ItemStackDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public int TotalWeight { get; set; }

        public string GetInventoryLine()
        {
            if(Amount == 1)
            {
                return $"{Name} [{TotalWeight} lbs]";
            }
            else
            {
                return $"{Name} x {Amount} [{TotalWeight} lbs]";
            }
        }
    }
}
