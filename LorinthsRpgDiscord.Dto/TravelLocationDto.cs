﻿using LorinthsRpgDiscord.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Dto
{
    public class TravelLocationDto
    {
        public Location Location { get; set; }
        public SubLocation SubLocation { get; set; }
        public decimal? DistanceInMiles { get; set; }
        public TimeSpan? TravelTime { get; set; }

        public string Name
        {
            get
            {
                return SubLocation != null ? $"{SubLocation.Name} ({Location.Name})" : Location.Name;
            }
        }
    }
}
