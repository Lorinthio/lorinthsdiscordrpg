﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/lookup")]
    [ApiController]
    public class LookupController : ControllerBase
    {
        private readonly IClassService _classService;
        private readonly ILocationService _locationService;
        private readonly IQuestService _questService;
        private readonly IRaceService _raceService;

        public LookupController(
            IClassService classService,
            ILocationService locationService,
            IQuestService questService,
            IRaceService raceService)
        {
            _classService = classService;
            _locationService = locationService;
            _questService = questService;
            _raceService = raceService;
        }

        [HttpGet]
        [Route("classes")]
        public async Task<IEnumerable<LookupDto>> GetClassLookups()
        {
            return await _classService.GetAllLookups();
        }

        [HttpGet]
        [Route("races")]
        public async Task<IEnumerable<LookupDto>> GetRaceLookups()
        {
            return await _raceService.GetAllLookups();
        }

        [HttpGet]
        [Route("objectiveTypes")]
        public async Task<IEnumerable<LookupDto>> GetGoalTypes()
        {
            return await _questService.GetGoalTypes();
        }

        [HttpGet]
        [Route("locationTypes")]
        public async Task<IEnumerable<LookupDto>> GetLocationTypes()
        {
            return await _locationService.GetLocationTypes();
        }
    }
}