﻿using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Extensions;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/oauth/discord")]
    public class DiscordOAuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IDiscordUserService _discordUserService;
        private readonly ILoggingService _loggingService;
        private readonly IPlayerService _playerService;
        private readonly ISecurityService _securityService;
        private const string _baseUrl = "https://discordapp.com/api/oauth2/authorize?";
        public DiscordOAuthController(IConfiguration config,
            IDiscordUserService discordUserService,
            ILoggingService loggingService,
            IPlayerService playerService,
            ISecurityService securityService)
        {
            _config = config;
            _discordUserService = discordUserService;
            _loggingService = loggingService;
            _playerService = playerService;
            _securityService = securityService;
        }

        [HttpGet]
        [Route("loginUrl")]
        public ActionResult GetLoginUrl()
        {
            _loggingService.LogInformation("DiscordOAuthController.LogIn");
            var oauthUrl = GetOAuthUrl();
            return Ok(oauthUrl);
        }

        [HttpGet]
        [Route("logout")]
        public ActionResult LogOut()
        {
            _loggingService.LogInformation("DiscordOAuthController.LogOut");
            var cookie = Request.Cookies.FirstOrDefault(c => c.Key == "DiscordRpg_SessionId");
            if (!Guid.TryParse(cookie.Value, out Guid sessionId))
            {
                return Redirect("/");
            }
            Response.Cookies.Delete("DiscordRpg_SessionId");

            return Redirect("/");
        }

        [HttpPost]
        [Route("handleRedirect")]
        public async Task HandleRedirect([FromBody] DiscordRedirectModel discordRedirectModel)
        {
            _loggingService.LogInformation("DiscordOAuthController.verify");

            var response = _discordUserService.GetUser(discordRedirectModel.Code);
            var player = await _playerService.UpdatePlayer(response);
            var sessionId = await _securityService.CreateSession(player.Id, HttpContext.GetIpAddress());

            var cookieOptions = new Microsoft.AspNetCore.Http.CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                IsEssential = true,
                Expires = DateTime.Now.AddDays(7),
            };

            Response.Cookies.Append("DiscordRpg_SessionId", sessionId.ToString(), cookieOptions);
        }

        private string GetOAuthUrl()
        {
            _loggingService.LogInformation("DiscordOAuthController.GetOAuthUrl");
            return $"{_baseUrl}" +
                $"client_id={_config["ClientId"]}" +
                $"&redirect_uri={_config["DiscordRedirectUrl"]}" +
                $"&response_type=code" +
                $"&scope={_config["DiscordScope"]}";
        }
    }
}
