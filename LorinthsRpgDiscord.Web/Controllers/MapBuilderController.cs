﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Dto.BattleMap;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Models;
using LorinthsRpgDiscord.Web.Models.Attributes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/mapbuilder")]
    [DiscordSecurity("World Builder")]
    public class MapBuilderController : SecureController
    {
        private readonly IWebHostEnvironment webHostEnvironment;

        public MapBuilderController(
            IWebHostEnvironment hostEnvironment,
            ISecurityService securityService) : base(securityService) {
            webHostEnvironment = hostEnvironment;
        }

        [Route("list")]
        [HttpGet]
        public IEnumerable<string> GetMapList()
        {
            string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "battlemaps");
            return Directory.GetFileSystemEntries(uploadsFolder).Select(f => Path.GetFileName(f).Replace("_", " ").Replace(".json", ""));
        }
        
        [Route("uploadImage")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(IList<IFormFile> files)
        {
            try
            {
                var file = files.First();

                if(file == null)
                {
                    return BadRequest("No file provided for image upload");
                }

                string uniqueFileName = null;
                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = file.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                return Ok($"/images/{uniqueFileName}");
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [Route("load")]
        [HttpGet]
        public ActionResult LoadMap([FromQuery] string mapName)
        {
            if(string.IsNullOrWhiteSpace(mapName)) {
                return BadRequest("Invalid Map Name");
            }

            string uploadsFolder = Path.Combine(this.webHostEnvironment.WebRootPath, "battlemaps");
            string filePath = Path.Combine(uploadsFolder, $"{mapName.Replace(" ", "_")}.json");
            if (!System.IO.File.Exists(filePath))
            {
                return BadRequest("Invalid Map Name");
            }

            var jsonText = System.IO.File.ReadAllText(filePath);
            var battleMap = JsonConvert.DeserializeObject<BattleMapDto>(jsonText);

            return Ok(battleMap);
        }

        [Route("save")]
        [HttpPost]
        public ActionResult SaveMap([FromBody] BattleMapDto battleMapDto)
        {
            try
            {
                var json = JsonConvert.SerializeObject(battleMapDto);

                string uploadsFolder = Path.Combine(this.webHostEnvironment.WebRootPath, "battlemaps");
                string filePath = Path.Combine(uploadsFolder, $"{battleMapDto.Name.Replace(" ", "_")}.json");
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                System.IO.File.WriteAllText(filePath, json);
            }
            catch(Exception ex)
            {

            }

            return Ok();
        }
    }
}