﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LorinthsRpgDiscord.Dto.Security;
using LorinthsRpgDiscord.Dto.Exceptions;
using LorinthsRpgDiscord.Web.Extensions;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/security")]
    [ApiController]
    public class SecurityController : SecureController
    {
        public SecurityController(ISecurityService securityService) : base(securityService) { }

        [HttpGet]
        [Route("getCurrentUser")]
        public async Task<User> GetCurrentUser()
        {
            return await GetCurrentSessionUser();
        }
    }
}
