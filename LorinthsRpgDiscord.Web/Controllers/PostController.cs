﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LorinthsRpgDiscord.Web.Models.Attributes;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/post")]
    [ApiController]
    public class PostController : SecureController
    {
        private readonly IPostService _postService;

        public PostController(
            ISecurityService securityService,
            IPostService postService) : base(securityService)
        {
            _postService = postService;
        }

        [HttpGet]
        [Route("getall")]
        public async Task<IEnumerable<Post>> GetAllPosts()
        {
            return await _postService.GetAllPosts();
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<Post> GetPost(int id)
        {
            return await _postService.GetPost(id);
        }

        [HttpPost]
        [DiscordSecurity("News Poster")]
        [Route("create")]
        public async Task<Post> CreatePost(PostViewModel postViewModel)
        {
            Post post = new Post()
            {
                Id = default(int),
                CreatorId = postViewModel.CreatorId,
                Title = postViewModel.Title,
                Content = postViewModel.Content,
                UploadDate = DateTime.UtcNow,
                EditingMode = false
            };

            return await _postService.SavePost(post);
        }

        [HttpPatch]
        [DiscordSecurity("News Poster")]
        [Route("edit/{id}")]
        public async Task<Post> EditPost(int id, PostViewModel postViewModel)
        {
            Post post = await _postService.GetPost(id);

            if (post.Title != postViewModel.Title)
            {
                post.Title = postViewModel.Title;
                post.LastUpdateDate = DateTime.UtcNow;
            }

            if (post.Content != postViewModel.Content)
            {
                post.Content = postViewModel.Content;
                post.LastUpdateDate = DateTime.UtcNow;
            }

            return await _postService.SavePost(post);
        }

        [HttpDelete]
        [DiscordSecurity("News Poster")]
        [Route("delete/{id}")]
        public async Task<IEnumerable<Post>> DeletePost(int id)
        {
            return await _postService.DeletePost(id);
        }
    }
}