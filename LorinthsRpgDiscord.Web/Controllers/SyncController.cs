﻿using LorinthsRpgDiscord.External.AirTable.Interfaces;
using LorinthsRpgDiscord.External.AirTable.Models;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Models;
using LorinthsRpgDiscord.Web.Models.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [DiscordSecurity("World Builder")]
    [Route("api/sync")]
    public class SyncController : SecureController
    {
        private readonly IAirTableSyncService _syncService;

        public SyncController(
            ISecurityService securityService,
            IAirTableSyncService syncService) : base(securityService)
        {
            _syncService = syncService;
        }

        [Route("tables")]
        [HttpPost]
        public async Task<ActionResult> SyncTables([FromBody] AirTableSyncRequest syncRequest)
        {
            var results = await _syncService.SyncTables(syncRequest);

            return Ok(results);
        }
    }
}
