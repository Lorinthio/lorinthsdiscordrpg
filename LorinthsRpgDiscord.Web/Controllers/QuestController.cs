﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LorinthsRpgDiscord.Web.Models.Attributes;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/quest")]
    [ApiController]
    public class QuestController : SecureController
    {
        private readonly IQuestService _questService;

        public QuestController(
            ISecurityService securityService,
            IQuestService questService) : base(securityService)
        {
            _questService = questService;
        }
        
        [HttpGet]
        [Route("lookups")]
        public async Task<IEnumerable<LookupDto>> GetQuestLookups()
        {
            return await _questService.GetQuestLookups();
        }

        [HttpGet]
        [DiscordSecurity("World Builder")]
        [Route("create")]
        public async Task<Quest> CreateQuest()
        {
            var quest = new Quest
            {
                Name = "New Quest",
                Description = "",
                IsActive = true
            };
            return await Task.FromResult(quest);
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<Quest> GetQuest(int id)
        {
            return await _questService.GetQuest(id);
        }

        [HttpPost]
        [Route("search")]
        public async Task<List<Quest>> SearchQuests(SearchViewModel searchViewModel)
        {
            return await _questService.SearchQuests(searchViewModel.SearchValue);
        }

        // POST: api/Quest
        [HttpPost]
        [Route("save")]
        [DiscordSecurity("World Builder")]
        public async Task<Quest> Save(Quest viewModel)
        {
            return await _questService.SaveQuest(viewModel);
        }

    }
}
