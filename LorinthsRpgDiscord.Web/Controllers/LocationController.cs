﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Enums;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Models;
using LorinthsRpgDiscord.Web.Models.Attributes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/location")]
    [DiscordSecurity("World Builder")]
    [ApiController]
    public class LocationController : SecureController
    {
        private readonly ILocationService _locationService;
        private readonly ITravelService _travelService;
        public LocationController(
            ISecurityService securityService,
            ILocationService locationService,
            ITravelService travelService): base(securityService)
        {
            _locationService = locationService;
            _travelService = travelService;
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<Location> GetLocation(int id)
        {
            return await _locationService.GetLocation(id);
        }

        [HttpPost]
        [Route("search")]
        public async Task<IEnumerable<Location>> SearchLocation(SearchViewModel searchViewModel)
        {
            return await _travelService.SearchLocations(searchViewModel.SearchValue);
        }

        [HttpGet]
        [Route("create")]
        public async Task<Location> CreateLocation()
        {
            var location = new Location
            {
                Name = "New Location",
                Description = "",
                LocationTypeId = (int) LocationTypeEnum.POI,
                IsActive = true
            };
            return await Task.FromResult(location);
        }

        [HttpPost]
        [Route("save")]
        public async Task<Location> SaveLocation(Location location)
        {
            return await _locationService.SaveLocation(location);

        }
    }
}