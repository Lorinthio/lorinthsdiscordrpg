﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LorinthsRpgDiscord.Web.Controllers
{
    [Route("api/player")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IPlayerService _playerService;

        public PlayerController(IPlayerService service)
        {
            _playerService = service;
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<Player> GetPost(string id)
        {
            return await _playerService.GetPlayer(id);
        }

        [HttpPost]
        [Route("create/{id}")]
        public async Task<Player> CreatePlayer(string id)
        {
            return await _playerService.GetOrCreatePlayer(id);
        }
    }
}
