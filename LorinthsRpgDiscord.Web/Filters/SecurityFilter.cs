﻿

using LorinthsRpgDiscord.Dto.Security;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Extensions;
using LorinthsRpgDiscord.Web.Models.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Filters
{
    public class SecurityFilter : ActionFilterAttribute
    {
        private readonly ISecurityService _securityService;
        public SecurityFilter(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!HasAccess(context).Result)
            {
                context.HttpContext.Response.StatusCode = 401;
                context.HttpContext.Response.Headers.Clear();

                context.Result = new EmptyResult();
                return;
            }

            base.OnActionExecuting(context);
        }

        private async Task<bool> HasAccess(ActionExecutingContext context)
        {
            var security = GetSecurityAttribute(context);
            if(security == null)
            {
                return true;
            }

            var cookie = context.HttpContext.Request.Cookies.FirstOrDefault(c => c.Key == "DiscordRpg_SessionId");
            if (!Guid.TryParse(cookie.Value, out Guid sessionId))
            {
                return false;
            }

            User user = null;
            try
            {
                user = await _securityService.GetCurrentUser(sessionId, context.HttpContext.GetIpAddress());
                return security.HasAccess(user);
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        private DiscordSecurity GetSecurityAttribute(ActionExecutingContext context)
        {
            DiscordSecurity attribute = null;
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerActionDescriptor != null)
            {
                attribute = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                    .FirstOrDefault(a => a.GetType().Equals(typeof(DiscordSecurity))) as DiscordSecurity;

                if(attribute == null)
                {
                    attribute = controllerActionDescriptor.ControllerTypeInfo.GetCustomAttributes(true)
                        .FirstOrDefault(a => a.GetType().Equals(typeof(DiscordSecurity))) as DiscordSecurity;
                }
            }

            return attribute;
        }
    }
}
