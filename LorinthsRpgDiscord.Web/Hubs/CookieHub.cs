﻿using LorinthsRpgDiscord.Constants;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Extensions;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Hubs
{
    public class CookieHub : Hub
    {
        private readonly static Dictionary<string, string> _connectionGroup = new Dictionary<string, string>();
        private readonly static Dictionary<string, Player> _connectionPlayers = new Dictionary<string, Player>();
        private readonly ISecurityService _securityService;

        public CookieHub(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        /// <summary>
        /// User connects to a session they have access to
        /// </summary>
        /// <param name="sessionId">The target session id</param>
        public void Connect(string sessionId = null)
        {
            sessionId ??= GetCookieValue(CookieNames.GameSessionCookie);

            // Check if user is a valid player of the session


            // Sets user to session
            Groups.AddToGroupAsync(Context.ConnectionId, sessionId);
            GetClient().SendAsync("Connected");
            _connectionGroup.Add(Context.ConnectionId, sessionId);
        }

        protected async Task<Player> GetPlayer()
        {
            var connectionId = Context.ConnectionId;

            if(_connectionPlayers.TryGetValue(connectionId, out Player player)) {
                return player;
            }
            else
            {
                var sessionId = Guid.Parse(GetCookieValue(CookieNames.WebSessionCookie));
                var ipAddress = Context.GetHttpContext().GetIpAddress();
                player = await _securityService.GetCurrentPlayer(sessionId, ipAddress);

                _connectionPlayers.Add(Context.ConnectionId, player);
                return player;
            }
        }

        protected IClientProxy GetClient()
        {
            return Clients.Client(Context.ConnectionId);
        }

        protected IClientProxy GetGroup()
        {
            var sessionId = _connectionGroup[Context.ConnectionId];
            return Clients.Group(sessionId);
        }

        protected string GetCookieValue(string cookieName)
        {
            return Context.GetHttpContext().Request.Cookies[cookieName];
        }
    }
}
