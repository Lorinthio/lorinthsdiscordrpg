﻿using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Hubs
{
    public class ChatHub : CookieHub
    {
        private readonly static Dictionary<string, string> _connectionUsername = new Dictionary<string, string>();

        public ChatHub(ISecurityService securityService) : base(securityService) { }

        public Task Join(string username)
        {
            Connect();

            // Sets user to session
            _connectionUsername.Add(Context.ConnectionId, username);
            return GetGroup().SendAsync("Message", "System", "System", $"{username} has joined the session!");
        }

        public Task Message(string chatMode, string message)
        {
            return GetGroup().SendAsync("Message", GetUsername(), chatMode, message);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            GetGroup().SendAsync("Message", "System", "System", $"{_connectionUsername[Context.ConnectionId]} has disconnected!");
            return base.OnDisconnectedAsync(exception);
        }

        private string GetUsername()
        {
            return _connectionUsername[Context.ConnectionId];
        }
    }
}
