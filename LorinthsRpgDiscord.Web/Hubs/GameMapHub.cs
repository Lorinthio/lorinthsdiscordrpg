﻿using LorinthsRpgDiscord.Dto.BattleMap;
using LorinthsRpgDiscord.Game;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System.IO;
using System.Linq;

namespace LorinthsRpgDiscord.Web.Hubs
{
    public class GameMapHub : CookieHub
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        public GameMapHub(
            IWebHostEnvironment hostEnvironment, 
            ISecurityService securityService) : base(securityService)
        {
            webHostEnvironment = hostEnvironment;
        }

        public void GetMap()
        {
            string uploadsFolder = Path.Combine(this.webHostEnvironment.WebRootPath, "battlemaps");
            string firstFile = Directory.GetFiles(uploadsFolder).FirstOrDefault();

            var jsonText = File.ReadAllText(firstFile);
            var battleMap = JsonConvert.DeserializeObject<BattleMapDto>(jsonText);

            GetClient().SendAsync("GameMap", battleMap);
        }

        public void Move()
        {
            
        }
    }
}
