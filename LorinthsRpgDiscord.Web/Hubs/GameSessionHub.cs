﻿using LorinthsRpgDiscord.Game;
using LorinthsRpgDiscord.Service.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Hubs
{
    public class GameSessionHub : CookieHub
    {
        private readonly IGameSessionService _gameSessionService;

        public GameSessionHub(IGameSessionService gameSessionService,
            ISecurityService securityService) : base(securityService)
        {
            _gameSessionService = gameSessionService;
        }

        /// <summary>
        /// User creates a session and gets the id back
        /// </summary>
        public async Task Create()
        {
            var player = await GetPlayer();
            var gameSession = await _gameSessionService.CreateGameSession(player.Id);
            await Clients.Client(Context.ConnectionId).SendAsync("Created", gameSession.Id);
        }
    }
}
