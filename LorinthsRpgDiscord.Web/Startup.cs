using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Service;
using LorinthsRpgDiscord.Data.Models;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Discord;
using LorinthsRpgDiscord.Web.Filters;
using LorinthsRpgDiscord.Web.Hubs;
using LorinthsRpgDiscord.External.AirTable;
using LorinthsRpgDiscord.External.AirTable.Interfaces;
using LorinthsRpgDiscord.Web.Extensions;
using Microsoft.Extensions.Logging;

namespace LorinthsRpgDiscord.Web
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddSignalR();

            services.AddControllersWithViews(config => 
                config.Filters.Add<SecurityFilter>())
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var context = Configuration["DiscordRpgContext"];

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddScoped<ICharacterService, CharacterService>()
                .AddScoped<IClassService, ClassService>()
                .AddScoped<IDiscordUserService, DiscordUserService>()
                .AddScoped<IGameSessionService, GameSessionService>()
                .AddScoped<IGatheringService, GatheringService>()
                .AddScoped<IInventoryService, InventoryService>()
                .AddScoped<ILocationService, LocationService>()
                .AddScoped<ILootService, LootService>()
                .AddScoped<IPlayerService, PlayerService>()
                .AddScoped<ISecurityService, SecurityService>()
                .AddScoped<IPostService, PostService>()
                .AddScoped<IProfessionService, ProfessionService>()
                .AddScoped<IQuestService, QuestService>()
                .AddScoped<IRaceService, RaceService>()
                .AddScoped<ITimeService, TimeService>()
                .AddScoped<ITravelService, TravelService>()
                .AddTransient<ILoggingService, LoggingService>()
                .AddScoped(typeof(IAirTableService<>), typeof(AirTableService<>))
                .AddScoped<IAirTableSyncService, AirTableSyncService>()
                .AddScoped<DiscordRpgContext, DiscordRpgThreadedContext>()
                .AddSingleton(Configuration)
                .AddDbContext<DiscordRpgThreadedContext>(ServiceLifetime.Transient);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = context =>
                {
                    context.Context.Response.Headers["Cache-Control"] = "no-cache, no-store, must-revalidate, max-age=0";
                    context.Context.Response.Headers["Expires"] = "-1";
                }
            });
            app.UseSpaStaticFiles();

            app.ConfigureCustomExceptionMiddleware();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapHub<ChatHub>("/game/chat");
                endpoints.MapHub<GameMapHub>("/game/map");
                endpoints.MapHub<GameSessionHub>("/game/session");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
