﻿using LorinthsRpgDiscord.Dto.Security;
using LorinthsRpgDiscord.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static Guid? GetCurrentUserSessionId(this SecureController controllerBase)
        {
            var cookie = controllerBase.Request.Cookies.FirstOrDefault(c => c.Key == "DiscordRpg_SessionId");
            if (!Guid.TryParse(cookie.Value, out Guid sessionId))
            {
                return null;
            }

            return sessionId;
        }
    }
}
