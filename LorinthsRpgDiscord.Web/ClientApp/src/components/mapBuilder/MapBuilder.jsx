import * as React from "react";
import { connect } from "../../common/state/connect";

import "./MapBuilder.css";
import Store from "../../common/state/store";
import StoreKeys from "../../common/state/storeKeys";
import MapEditor from "./mapEditor/MapEditor";
import { MapSelector } from "./mapSelector/MapSelector";
import BattleMap from "./mapEditor/viewModels/BattleMap";
import { ROLES, userHasRole } from "../../common/utils/roleChecker";

const propKeys = {
  map: StoreKeys.MAPBUILDER.MAP,
};

class MapBuilderC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listOpen: false,
    };

    this.onCreateMap = this.onCreateMap.bind(this);
    this.onOpenMap = this.onOpenMap.bind(this);
    this.onSelectorClose = this.onSelectorClose.bind(this);
  }

  componentDidMount() {
    const hasAccess = userHasRole(ROLES.WorldBuilder);
    if (!hasAccess) {
      window.location.href = "/";
    }
  }

  render() {
    return (
      <div>
        <h1>Map Builder</h1>
        {!this.props.map && (
          <div>
            <button onClick={this.onCreateMap}>Create</button>
            <button onClick={this.onOpenMap}>Open</button>
          </div>
        )}
        {this.props.map && <MapEditor map={this.props.map} />}
        {this.state.listOpen && <MapSelector onClose={this.onSelectorClose} />}
      </div>
    );
  }

  onCreateMap() {
    let map = new BattleMap();
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onOpenMap() {
    this.setState({
      listOpen: true,
    });
  }

  onSelectorClose() {
    this.setState({
      listOpen: false,
    });
  }
}

export const MapBuilder = (props) => {
  return connect(<MapBuilderC />, propKeys, props);
};
