import * as React from "react";
import { connect } from "../../../common/state/connect";
import { MdTimeline, MdClose, MdInfoOutline, MdOpenWith } from "react-icons/md";
import { Stage, Layer, Image, Circle, Rect, Line } from "react-konva";
import { loadImage } from "../../../common/utils/imageLoader";
import { uploadMapImage, saveMap } from "../api";
import StoreKeys from "../../../common/state/storeKeys";
import Store from "../../../common/state/store";
import { Wall } from "./viewModels/Wall";
import { Tile } from "./viewModels/Tile";
import { SetLoading } from "../../../common/components/Loading";
import { Action, ActionTypes } from "./viewModels/Action";

const propKeys = {};

export default class MapEditorC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      actions: [],
      undoneActions: [],
      gridImage: null,
      backgroundImage: null,
      pointA: null,
      shadowPoint: null,
      shadowBlock: null,
      wallMode: false,
      blockMode: false,
      dragMode: false,
    };

    this.fileInputRef = React.createRef();
    this.stage = null;

    this.onKeyDown = this.onKeyDown.bind(this);
    this.onMapNameChange = this.onMapNameChange.bind(this);
    this.onUploadClicked = this.onUploadClicked.bind(this);
    this.backgroundUpload = this.backgroundUpload.bind(this);
    this.onMapWidthChange = this.onMapWidthChange.bind(this);
    this.onMapHeightChange = this.onMapHeightChange.bind(this);
    this.onOffsetXChange = this.onOffsetXChange.bind(this);
    this.onOffsetYChange = this.onOffsetYChange.bind(this);
    this.resetOffset = this.resetOffset.bind(this);
    this.onGridClicked = this.onGridClicked.bind(this);
    this.shadowMouse = this.shadowMouse.bind(this);
    this.dragModeClicked = this.dragModeClicked.bind(this);
    this.wallModeClicked = this.wallModeClicked.bind(this);
    this.blockModeClicked = this.blockModeClicked.bind(this);
    this.onMapSave = this.onMapSave.bind(this);
  }

  componentDidMount() {
    this.checkSize();
    window.addEventListener("resize", this.checkSize);
    SetLoading(true, "Loading Images");
    const promises = [];
    promises.push(
      loadImage("/images/Grid.png").then((image) => {
        this.setState({ gridImage: image });
      })
    );
    if (
      this.props.map.backgroundImageUrl !== null &&
      this.props.map.backgroundImageUrl.length > 0
    ) {
      promises.push(
        loadImage(this.props.map.backgroundImageUrl).then((image) => {
          this.setState({ backgroundImage: image });
        })
      );
    }

    document.onkeydown = this.onKeyDown;

    Promise.all(promises).then(() => {
      SetLoading(false);
    });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.checkSize);
  }

  checkSize = () => {
    const width = this.container.offsetWidth;
    const height = this.container.offsetHeight;
    this.setState({
      stageWidth: width,
      stageHeight: height,
    });
  };

  render() {
    return (
      <div>
        <table>
          <tbody>
            <tr>
              <td>Map Name</td>
            </tr>
            <tr>
              <td>
                <input
                  onChange={this.onMapNameChange}
                  value={this.props.map.name}
                />
              </td>
            </tr>
            <tr className="space">
              <td>
                Width in Units
                <MdInfoOutline
                  color="beige"
                  style={{
                    color: "beige",
                    position: "relative",
                    left: 4,
                  }}
                  title="One Unit = 70px. Recommended size is 20-50 units"
                />
              </td>
              <td>
                Height in Units
                <MdInfoOutline
                  color="beige"
                  style={{
                    color: "beige",
                    position: "relative",
                    left: 4,
                  }}
                  title="One Unit = 70px. Recommended size is 20-50 units"
                />
              </td>
            </tr>
            <tr>
              <td>
                <input
                  style={{ display: "inline-block" }}
                  onChange={this.onMapWidthChange}
                  value={this.props.map.width}
                />
              </td>
              <td>
                <input
                  style={{ display: "inline-block" }}
                  onChange={this.onMapHeightChange}
                  value={this.props.map.height}
                />
              </td>
            </tr>
            <tr className="space">
              <td>Offset X</td>
              <td>Offset Y</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <input
                  type="range"
                  onChange={this.onOffsetXChange}
                  value={this.props.map.offset.x}
                  min={-70}
                  max={70}
                />
              </td>
              <td>
                <input
                  type="range"
                  onChange={this.onOffsetYChange}
                  value={this.props.map.offset.y}
                  min={-70}
                  max={70}
                />
              </td>
              <td>
                <button onClick={this.resetOffset}>Zero</button>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="toolbar">
          <span
            className={this.state.wallMode ? " active" : ""}
            title="Wall Mode"
          >
            <MdTimeline onClick={this.wallModeClicked} />
          </span>
          <span
            className={this.state.blockMode ? " active" : ""}
            title="Block Mode"
          >
            <MdClose onClick={this.blockModeClicked} />
          </span>
          <span
            className={this.state.dragMode ? " active" : ""}
            title="Drag Mode"
          >
            <MdOpenWith onClick={this.dragModeClicked} />
          </span>
        </div>
        <div
          className="map-builder-grid"
          ref={(node) => {
            this.container = node;
          }}
        >
          <Stage
            style={{ cursor: this.state.dragMode ? "move" : "default" }}
            width={this.state.stageWidth}
            height={this.state.stageHeight}
            onClick={this.onGridClicked}
            onMouseMove={this.shadowMouse}
            draggable={this.state.dragMode}
            dragBoundFunc={this.dragMap}
            ref={(node) => {
              this.stage = node;
            }}
          >
            <Layer>
              {this.state.backgroundImage && (
                <Image
                  x={this.props.map.offset.x}
                  y={this.props.map.offset.y}
                  image={this.state.backgroundImage}
                />
              )}

              <Image
                x={0}
                y={0}
                width={this.props.map.width * 70}
                height={this.props.map.height * 70}
                crop={{
                  x: 0,
                  y: 0,
                  width: this.props.map.width * 70,
                  height: this.props.map.height * 70,
                }}
                image={this.state.gridImage}
              />
              {this.props.map.walls.map((wall) => {
                return (
                  <Line
                    key={wall.id}
                    points={[
                      wall.a.x * 70,
                      wall.a.y * 70,
                      wall.b.x * 70,
                      wall.b.y * 70,
                    ]}
                    stroke="red"
                    strokeWidth={2}
                  />
                );
              })}
              {this.props.map.blocks.map((block) => {
                return (
                  <Rect
                    key={block.id}
                    x={block.x * 70}
                    y={block.y * 70}
                    width={70}
                    height={70}
                    fill={block.blocked ? "red" : "green"}
                    stroke="gray"
                  />
                );
              })}
              {this.state.pointA !== null && (
                <Circle
                  x={this.state.pointA.x * 70}
                  y={this.state.pointA.y * 70}
                  fill="red"
                  radius={5}
                />
              )}
              {this.state.shadowPoint !== null && (
                <Circle
                  x={this.state.shadowPoint.x * 70}
                  y={this.state.shadowPoint.y * 70}
                  fill="gray"
                  radius={5}
                />
              )}
              {this.state.shadowBlock !== null && (
                <Rect
                  x={this.state.shadowBlock.x * 70}
                  y={this.state.shadowBlock.y * 70}
                  width={70}
                  height={70}
                  fill="gray"
                  radius={5}
                />
              )}
            </Layer>
          </Stage>
        </div>
        <input
          id="background"
          name="background"
          size="1"
          ref={this.fileInputRef}
          type="file"
          onChange={this.backgroundUpload}
          accept="image/png"
          hidden
        />
        <button onClick={this.onUploadClicked}>Upload Background</button>
        <button onClick={this.onMapSave}>Save Map</button>
      </div>
    );
  }

  dragMap(pos) {
    var posX = Math.min(pos.x, 100);
    var posY = Math.min(pos.y, 100);
    return {
      x: posX,
      y: posY,
    };
  }

  onKeyDown(e) {
    if (document.activeElement.nodeName === "INPUT") {
      return;
    }

    if (e.keyCode === 90 && e.ctrlKey) {
      this.undo();
    } else if (e.keyCode === 89 && e.ctrlKey) {
      this.redo();
    } else if (e.keyCode === 83 && e.ctrlKey) {
      this.onMapSave();
      e.preventDefault();
    } else if (e.keyCode === 68) {
      this.dragModeClicked();
    } else if (e.keyCode === 87) {
      this.wallModeClicked();
    } else if (e.keyCode === 66) {
      this.blockModeClicked();
    }
  }

  undo() {
    if (this.state.actions.length === 0) {
      return;
    }

    let actions = this.state.actions;
    for (let i = actions.length - 1; i >= 0; i--) {
      let action = actions[i];
      if (action.actionType === ActionTypes.ADD) {
        if (action.param1 === "wall") {
          this.undoWallAdd(action);
        } else if (action.param1 === "block") {
          this.undoBlockAdd(action);
        }

        actions.splice(i, 1);
        this.setState({
          actions: [...actions],
        });
        break;
      }
    }
  }

  undoWallAdd(action) {
    let map = this.props.map;
    const walls = map.walls || [];
    const wallIndex = walls.findIndex((wall) => {
      return wall.id === action.item.id;
    });

    walls.splice(wallIndex, 1);
    Store.set(StoreKeys.MAPBUILDER.MAP, map);

    //this.addUndoneAction(action);
  }

  undoBlockAdd(action) {
    let map = this.props.map;
    const blocks = map.blocks || [];
    const blockIndex = blocks.findIndex((block) => {
      return block.id === action.item.id;
    });

    blocks.splice(blockIndex, 1);
    Store.set(StoreKeys.MAPBUILDER.MAP, map);

    //this.addUndoneAction(action);
  }

  addUndoneAction(action) {
    const undoneActions = this.state.undoneActions;
    undoneActions.push(action);
    this.setState({ undoneActions: [...undoneActions] });
  }

  redo() {
    // TODO: Not as important
  }

  onMapNameChange(e) {
    const map = this.props.map;
    map.name = e.target.value;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  wallModeClicked() {
    this.setState({
      wallMode: !this.state.wallMode,
      blockMode: false,
      dragMode: false,
      shadowPoint: null,
      shadowBlock: null,
    });
  }

  blockModeClicked() {
    this.setState({
      wallMode: false,
      blockMode: !this.state.blockMode,
      dragMode: false,
      shadowPoint: null,
      shadowBlock: null,
    });
  }

  dragModeClicked() {
    this.setState({
      wallMode: false,
      blockMode: false,
      dragMode: !this.state.dragMode,
      shadowPoint: null,
      shadowBlock: null,
    });
  }

  shadowMouse(e) {
    e = e.evt;
    let stageXOffset = this.stage.attrs.x || 0;
    let stageYOffset = this.stage.attrs.y || 0;
    let x = e.offsetX - stageXOffset;
    let y = e.offsetY - stageYOffset;

    if (this.state.wallMode) {
      this.shadowWallMode(x, y);
    } else if (this.state.blockMode) {
      this.shadowBlockMode(x, y);
    }
  }

  shadowWallMode(x, y) {
    let closestX = Math.floor((x + 35) / 70);
    let closestY = Math.floor((y + 35) / 70);

    if (
      this.state.shadowPoint === null ||
      closestX !== this.state.shadowPoint.x ||
      closestY !== this.state.shadowPoint.y
    ) {
      this.setState({
        shadowPoint: {
          x: closestX,
          y: closestY,
        },
      });
    }
  }

  shadowBlockMode(x, y) {
    let gridX = Math.floor(x / 70);
    let gridY = Math.floor(y / 70);

    if (
      this.state.shadowBlock === null ||
      gridX !== this.state.shadowBlock.x ||
      gridY !== this.state.shadowBlock.y
    ) {
      this.setState({
        shadowBlock: {
          x: gridX,
          y: gridY,
        },
      });
    }
  }

  onGridClicked(e) {
    e = e.evt;
    let stageXOffset = this.stage.attrs.x || 0;
    let stageYOffset = this.stage.attrs.y || 0;
    let x = e.offsetX - stageXOffset;
    let y = e.offsetY - stageYOffset;

    if (this.state.wallMode) {
      this.onWallClick(x, y);
    }
    if (this.state.blockMode) {
      this.onBlockClick(x, y);
    }
  }

  onWallClick(x, y) {
    let closestX = Math.floor((x + 35) / 70);
    let closestY = Math.floor((y + 35) / 70);

    if (!this.state.pointA) {
      this.setState({
        pointA: {
          x: closestX,
          y: closestY,
        },
      });
    } else {
      let pointB = {
        x: closestX,
        y: closestY,
      };

      let map = this.props.map;
      let wall = new Wall(map.walls.length, this.state.pointA, pointB);
      map.walls.push(wall);

      let actions = this.state.actions;
      let action = new Action(ActionTypes.ADD, wall, "wall");
      actions.push(action);

      this.setState({
        pointA: null,
        actions: [...actions],
      });

      Store.set(StoreKeys.MAPBUILDER.MAP, map);
    }
  }

  onBlockClick(x, y) {
    let gridX = Math.floor(x / 70);
    let gridY = Math.floor(y / 70);

    let map = this.props.map;
    let block = new Tile(map.blocks.length, gridX, gridY);
    map.blocks.push(block);

    let actions = this.state.actions;
    let action = new Action(ActionTypes.ADD, block, "block");
    actions.push(action);

    this.setState({
      actions: [...actions],
    });

    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onMapWidthChange(e) {
    let map = this.props.map;
    map.width = e.target.value;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onMapHeightChange(e) {
    let map = this.props.map;
    map.height = e.target.value;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onOffsetXChange(e) {
    let map = this.props.map;
    map.offset.x = e.target.value;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onOffsetYChange(e) {
    let map = this.props.map;
    map.offset.y = e.target.value;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  resetOffset() {
    let map = this.props.map;
    map.offset = {
      x: 0,
      y: 0,
    };
    Store.set(StoreKeys.MAPBUILDER.MAP, map);
  }

  onUploadClicked() {
    let element = this.fileInputRef.current;
    element.click();
  }

  backgroundUpload = async (e) => {
    let file = e.target.files[0];
    let form = new FormData();
    form.append("files", file);

    let map = this.props.map;
    let url = await uploadMapImage(form);
    map.backgroundImageUrl = url;
    Store.set(StoreKeys.MAPBUILDER.MAP, map);

    loadImage(url).then((image) => {
      this.setState({ backgroundImage: image });
    });
  };

  onMapSave() {
    saveMap(Store.get(StoreKeys.MAPBUILDER.MAP));
  }
}

export const MapEditor = (props) => connect(<MapEditorC />, propKeys, props);
