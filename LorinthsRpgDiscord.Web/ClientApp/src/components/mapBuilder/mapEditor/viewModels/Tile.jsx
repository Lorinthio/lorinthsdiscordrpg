import * as React from "react";
import { Rect } from "react-konva";

export class Tile {
  constructor(id, x, y, blocked = true) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.blocked = blocked;
  }

  render() {
    return (
      <Rect
        key={this.id}
        x={this.x * 70}
        y={this.y * 70}
        width={70}
        height={70}
        fill={this.blocked ? "red" : "green"}
        stroke="gray"
      />
    );
  }
}
