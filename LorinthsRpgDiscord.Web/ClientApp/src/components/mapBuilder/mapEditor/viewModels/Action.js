export const ActionTypes = {
  ADD: "ADD",
  REMOVE: "REMOVE",
  UNDO: "UNDO",
  REDO: "REDO",
};

/**
 * Action class for storing an action a user did
 */
export class Action {
  /**
   * @param {ActionTypes} actionType - value from ActionTypes.Key
   * @param {any} item - The item created from the action
   * @param {any} param1 - Additional data 1
   * @param {any} param2 - Additional data 2
   * @param {any} param3 - Additional data 3
   */
  constructor(actionType, item, param1 = null, param2 = null, param3 = null) {
    this.actionType = actionType;
    this.item = item;
    this.param1 = param1;
    this.param2 = param2;
    this.param3 = param3;
  }
}
