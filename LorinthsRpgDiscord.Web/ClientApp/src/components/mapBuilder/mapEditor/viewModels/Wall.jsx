import * as React from "react";
import { Line } from "react-konva";

export class Wall {
  constructor(id, pointA, pointB) {
    this.id = id;
    this.a = pointA;
    this.b = pointB;
  }

  render() {
    return (
      <Line
        key={this.id}
        points={[this.a.x * 70, this.a.y * 70, this.b.x * 70, this.b.y * 70]}
        stroke="red"
        strokeWidth={2}
      />
    );
  }
}
