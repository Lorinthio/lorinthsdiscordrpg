export default class BattleMap {
  constructor() {
    this.name = "New Battle Map";
    this.backgroundImageUrl = null;
    this.width = 10;
    this.height = 10;
    this.offset = {
      x: 0,
      y: 0,
    };
    this.walls = [];
    this.blocks = [];
  }
}
