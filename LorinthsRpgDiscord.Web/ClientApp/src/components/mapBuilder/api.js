import { ToastsStore } from "react-toasts";
var axios = require("axios");

const mapRoute = "/api/mapbuilder";

export const uploadMapImage = async (file) => {
  try {
    return await axios
      .post(`${mapRoute}/uploadImage`, file)
      .then((response) => {
        return response.data;
      });
  } catch (error) {
    ToastsStore.error(
      "Error uploading Map Image: " + error,
      3000,
      "custom-toast"
    );
  }
};

export const getMapList = async () => {
  try {
    return await axios.get(`${mapRoute}/list`).then((response) => {
      return response.data;
    });
  } catch (error) {
    ToastsStore.error("Error getting map list: " + error, 3000, "custom-toast");
  }
};

export const loadMap = async (mapName) => {
  try {
    return await axios
      .get(`${mapRoute}/load?mapName=${mapName}`)
      .then((response) => {
        return response.data;
      });
  } catch (error) {
    ToastsStore.error("Error loading map: " + error, 3000, "custom-toast");
  }
};

export const saveMap = async (battleMap) => {
  try {
    return await axios.post(`${mapRoute}/save`, battleMap).then((response) => {
      ToastsStore.success("Map Saved!", 3000);
      return response.data;
    });
  } catch (error) {
    ToastsStore.error(
      "Error saving battle map: " + error,
      3000,
      "custom-toast"
    );
  }
};
