import * as React from "react";
import { connect } from "../../../common/state/connect";
import { getMapList, loadMap } from "../api";
import { SetLoading } from "../../../common/components/Loading";

import "./MapSelector.css";
import Store from "../../../common/state/store";
import StoreKeys from "../../../common/state/storeKeys";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const propKeys = {};

export default class MapSelectorC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      maps: [],
      selectedMap: null,
    };

    this.onMapSelect = this.onMapSelect.bind(this);
    this.onMapLoad = this.onMapLoad.bind(this);
  }

  componentDidMount() {
    this.loadMapList();
  }

  render() {
    return (
      <>
        <Modal className="map-selector" show={true} onHide={this.props.onClose}>
          <Modal.Header closeButton>
            <Modal.Title>Please Select a Map</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            {this.state.maps.map((mapName, index) => {
              return (
                <li
                  className={
                    this.state.selectedMap === mapName ? "selected" : ""
                  }
                  key={index}
                  onClick={() => {
                    this.onMapSelect(mapName);
                  }}
                >
                  {mapName}
                </li>
              );
            })}
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="dark"
              bg="dark"
              disabled={this.state.selectedMap === null}
              onClick={this.onMapLoad}
            >
              Load
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }

  loadMapList = async () => {
    SetLoading(true, "Getting Map List");
    const maps = await getMapList();
    this.setState({
      maps,
    });
    SetLoading(false);
  };

  onMapSelect = (mapName) => {
    this.setState({
      selectedMap: mapName,
    });
  };

  onMapLoad = () => {
    const mapName = this.state.selectedMap;
    this.props.onClose();
    SetLoading(true, `Loading Map, ${mapName}`);
    loadMap(mapName).then((map) => {
      Store.set(StoreKeys.MAPBUILDER.MAP, map);
    });
  };
}

export const MapSelector = (props) =>
  connect(<MapSelectorC />, propKeys, props);
