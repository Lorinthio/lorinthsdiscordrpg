import React, { Component } from "react";
import { getPlayer } from "../../common/api/playerApi";
import { Editor } from "@tinymce/tinymce-react";
import { userHasRole, ROLES } from "../../common/utils/roleChecker.js";
import { Container } from "react-bootstrap";

class Post extends Component {
  constructor(props) {
    super(props);

    this.state = {
      creator: undefined,
    };
  }

  componentDidMount() {
    this.getCreator(this.props.post.creatorId);
  }

  render() {
    const dateOptions = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    const hasEditAccess = userHasRole(ROLES.NewsPoster);
    return (
      <>
        <Container className="post" fluid>
          {this.props.post.editingMode && (
            <div className="edit-state">
              {this.props.post.editingMode && (
                <>
                  <button
                    style={{ float: "right" }}
                    onClick={() =>
                      this.props.onEditionAbortion(this.props.post.id)
                    }
                    className="btn btn-danger"
                  >
                    Abort
                  </button>
                  <button
                    style={{ float: "right" }}
                    onClick={() => this.props.onEdit(this.props.post.id)}
                    className="btn btn-warning"
                  >
                    Save
                  </button>
                </>
              )}
              <input
                onChange={this.props.onPostTitleChange}
                name="EditedPost"
                className="edited-post-element"
                defaultValue={this.props.post.title}
              />
              {
                <Editor
                  initialValue={this.props.post.content}
                  init={{
                    width: "80%",
                    height: 250,
                    menubar: false,
                    skin: "oxide-dark",
                    content_css: "dark",
                    plugins: [
                      "advlist autolink lists link image charmap print preview anchor",
                      "searchreplace visualblocks code fullscreen",
                      "insertdatetime media table paste code help wordcount",
                    ],
                    toolbar:
                      "undo redo | formatselect | bold italic backcolor | " +
                      "alignleft aligncenter alignright alignjustify | " +
                      "bullist numlist outdent indent | removeformat | help",
                  }}
                  onEditorChange={this.props.onPostContentChange}
                />
              }
            </div>
          )}
          {!this.props.post.editingMode && (
            <div className="content">
              <div className="title">
                <label>
                  {
                    //On first render() this.state.creator is udefined so this.state.creator.username will throw a nasty exception
                    this.state.creator && (
                      <img
                        alt="creator avatar"
                        className="creator"
                        title={`${
                          this.state.creator.username
                        } on ${this.props.post.uploadDate.toLocaleDateString(
                          undefined,
                          dateOptions
                        )} `}
                        src={this.getCreatorAvatar()}
                      />
                    )
                  }
                  {this.props.post.title}
                </label>

                {hasEditAccess && (
                  <>
                    <button
                      style={{ float: "right" }}
                      onClick={() =>
                        this.props.onPostDelete(this.props.post.id)
                      }
                      className="btn btn-danger"
                    >
                      Delete
                    </button>
                    <button
                      style={{ float: "right" }}
                      onClick={() => this.props.onEdit(this.props.post.id)}
                      className="btn btn-warning"
                    >
                      Edit
                    </button>
                  </>
                )}
              </div>

              <hr />
              <div dangerouslySetInnerHTML={this.getHtmlContent()} />
            </div>
          )}
        </Container>
      </>
    );
  }

  getHtmlContent() {
    return { __html: this.props.post.content };
  }

  getCreator = async (creatorId) => {
    const [creator] = await Promise.all([getPlayer(creatorId)]);

    this.setState({ creator: creator.data });
  };

  getCreatorAvatar() {
    return this.state.creator.avatar
      ? `https://cdn.discordapp.com/avatars/${this.state.creator.userId}/${this.state.creator.avatar}.png`
      : "https://cdn.discordapp.com/embed/avatars/1.png";
  }
}

export default Post;
