import { ToastsStore } from "react-toasts";
var axios = require("axios");

const postRoute = "/api/post";

export const getAllPosts = async () => {
    try {
        return await axios.get(`${postRoute}/getall`);
    } catch (error) {
        ToastsStore.error("Error getting post: " + error, 3000, "custom-toast");
    }
};

export const getPost = async (postId) => {
    try {
        return await axios.get(`${postRoute}/get/${postId}`);
    } catch (error) {
        ToastsStore.error("Error getting post: " + error, 3000, "custom-toast");
    }
};

export const createPost = async (creatorId, title, content) => {
    try {
        return await axios.post(`${postRoute}/create`, {creatorId, title, content});
    } catch (error) {
        ToastsStore.error(
            "Error creating post: " + error,
            3000,
            "custom-toast"
        );
    } 
};

export const editPost = async (postId, title, content) => {
    try {
        let creatorId = "0"; /* placeholder */
        return await axios.patch(`${postRoute}/edit/${postId}`, {creatorId, title, content});
    } catch (error) {
        ToastsStore.error(
            "Error editing post: " + error,
            3000,
            "custom-toast"
        );
    }
};

export const deletePost = async (postId) => {
    try {
        return await axios.delete(`${postRoute}/delete/${postId}`);
    } catch (error) {
        ToastsStore.error("Error deleting post: " + error, 3000, "custom-toast");
    }
};