import React, { Component } from "react";
import Post from "./Post.jsx";
import "./Posts.css";
import { getAllPosts, createPost, editPost, deletePost } from "./api.js";
import Store from "../../common/state/store";
import StoreKeys from "../../common/state/storeKeys";
import { connect } from "../../common/state/connect.jsx";
import { userHasRole, ROLES } from "../../common/utils/roleChecker.js";
import { Editor } from "@tinymce/tinymce-react";
import { SetLoading } from "../../common/components/Loading.jsx";
import { Container, Row, Col } from "react-bootstrap";

/*
    TODO:
    - Post edit save DONE
    - Utilize all API methods DONE
    - Make some css magic
    - Check for creator DONE
    - Fetch creator name from ID DONE
    - Add user avatar (use className="avatar") DONE
    - Utilize permission (Add post form only visible for admins) 735623449191645215 DONE
    - Add button to discard changes in edit (cancel/abort) DONE
    - When already in edit mode Edit button should change name to Save DONE
    - Fix the bug with post duplication when changing multiple posts at the same time DONE (Forbid user to edit 2nd and next post at the same time)

    - It works. https://youtu.be/otCpCn0l4Wo?t=14
*/

const propKeys = {
  userRoles: StoreKeys.USER.ROLES,
};

class PostsC extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: undefined,
      content: undefined,
      isAdding: false,
      posts: [],
    };

    this.handleCancelCreation = this.handleCancelCreation.bind(this);
    this.handleCreatePost = this.handleCreatePost.bind(this);
    this.handleEditModeChange = this.handleEditModeChange.bind(this);
    this.handleAbortPostEdition = this.handleAbortPostEdition.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);

    this.handlePostCreation = this.handlePostCreation.bind(this);
    this.handleDeletePost = this.handleDeletePost.bind(this);
  }

  componentDidMount() {
    SetLoading(true, "Loading News!");
    this.loadData();
  }

  render() {
    const hasEditAccess = userHasRole(ROLES.NewsPoster);
    return (
      <Container fluid>
        <Row>
          <Col xl={1} l={1} />
          <Col xl={10} l={10} m={12}>
            {this.state.posts.map((post) => (
              <Post
                key={post.id}
                onEdit={this.handleEditModeChange}
                onEditionAbortion={this.handleAbortPostEdition}
                onPostDelete={this.handleDeletePost}
                onPostTitleChange={this.handleTitleChange}
                onPostContentChange={this.handleContentChange}
                post={post}
              />
            ))}
            {hasEditAccess && (
              <>
                {this.state.isAdding && (
                  <div className="new-post">
                    <input
                      id="NewPostTitle"
                      onChange={this.handleTitleChange}
                      name="NewPost"
                      className="post-area new-post-element"
                    />
                    <div className="post-area new-post-element">
                      <Editor
                        init={{
                          height: 500,
                          menubar: false,
                          skin: "oxide-dark",
                          content_css: "dark",
                          plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table paste code help wordcount",
                          ],
                          toolbar:
                            "undo redo | formatselect | bold italic backcolor | " +
                            "alignleft aligncenter alignright alignjustify | " +
                            "bullist numlist outdent indent | removeformat | help",
                        }}
                        onEditorChange={this.handleContentChange}
                      />
                    </div>

                    <div style={{ width: 176 }} className="position-center">
                      <button
                        onClick={this.handlePostCreation}
                        className="btn btn-primary"
                      >
                        Post
                      </button>
                      <button
                        onClick={this.handleCancelCreation}
                        className="btn btn-primary"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                )}
                {!this.state.isAdding && (
                  <div className="text-center">
                    <button
                      onClick={this.handleCreatePost}
                      className="btn btn-primary"
                    >
                      Create New Post
                    </button>
                  </div>
                )}
              </>
            )}
          </Col>
          <Col xl={1} l={1} />
        </Row>
      </Container>
    );
  }

  loadData = async () => {
    let postsResponse = await getAllPosts();
    let posts = postsResponse.data;
    for (let post of posts) {
      post.uploadDate = new Date(post.uploadDate);
    }

    //SetLoading(false);
    this.setState({
      posts,
    });
    SetLoading(false);
  };

  handleEditModeChange = async (postId) => {
    let post = this.state.posts.find((p) => p.id === postId);
    let currentlyEditedPosts = this.state.posts.filter((p) => p.editingMode);

    if (
      currentlyEditedPosts.length >= 1 &&
      !currentlyEditedPosts.includes(post)
    ) {
      return;
    }

    //Set Initial values
    if (!post.editingMode) {
      this.setState({ title: post.title, content: post.content });
    }

    if (
      post.editingMode &&
      this.state.title !== undefined &&
      this.state.content !== undefined
    ) {
      await editPost(postId, this.state.title, this.state.content);
      await this.loadData();
    }

    post.editingMode = !post.editingMode;

    this.setState({ post });
  };

  handleAbortPostEdition = (postId) => {
    let post = this.state.posts.find((p) => p.id === postId);

    post.editingMode = false;

    this.setState({ post });
  };

  handleTitleChange = (event) => {
    const title = event.currentTarget.value;
    this.setState({ title });
  };

  handleContentChange = (content) => {
    this.setState({ content });
  };

  handleCancelCreation = () => {
    this.setState({
      isAdding: false,
    });
  };

  handleCreatePost = () => {
    this.setState({
      isAdding: true,
    });
  };

  handlePostCreation = async () => {
    let title = this.state.title;
    let content = this.state.content;
    await createPost(Store.get(StoreKeys.USER.CURRENT).id, title, content);

    this.loadData();
  };

  handleDeletePost = async (postId) => {
    await deletePost(postId);

    this.loadData();
  };
}

export const Posts = (props) => {
  return connect(<PostsC />, propKeys, props);
};
