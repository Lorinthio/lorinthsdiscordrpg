import * as React from "react";
import StoreKeys from "../../common/state/storeKeys";
import { SetLoading } from "../../common/components/Loading";
import SearchInput from "../../common/components/SearchInput";
import { IconButton } from "../../common/components/IconButton";
import { MdAddCircleOutline } from "react-icons/md";
import { getLocationTypes } from "../../common/lookupApi";
import {
  searchLocations,
  getLocation,
  createLocation,
  saveLocation,
} from "./location/api";
import Location from "./location/Location";
import Store from "../../common/state/store";
import { connect } from "../../common/state/connect";

const propKeys = {
  location: StoreKeys.LOCATIONBUILDER.LOCATION,
};

class LocationBuilderC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: [],
      locationTypes: [],
    };

    this.onLocationSearch = this.onLocationSearch.bind(this);
    this.onLocationSelect = this.onLocationSelect.bind(this);
    this.onCreateLocation = this.onCreateLocation.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentDidMount() {
    SetLoading(true, "Loading Location Lookups");
    this.loadLookups();
  }

  render() {
    return (
      <div className="location-builder">
        <h1>Location Builder</h1>
        <SearchInput
          ownerKey="LocationBuilder"
          onSearch={this.onLocationSearch}
          onSelect={this.onLocationSelect}
          options={this.state.locations}
          placeholder="Search For a Location"
        />
        <IconButton
          Component={<MdAddCircleOutline />}
          text="Create Location"
          onClick={this.onCreateLocation}
        />

        {this.props.location !== undefined ? (
          <Location
            location={this.props.location}
            locationTypes={this.state.locationTypes}
            saveCallback={this.onSave}
          />
        ) : (
          <></>
        )}
      </div>
    );
  }

  loadLookups = async () => {
    let [locationTypesResponse] = await Promise.all([getLocationTypes()]);

    SetLoading(false);
    this.setState({
      locationTypes: locationTypesResponse.data,
    });
  };

  onLocationSearch = async (text) => {
    let locationResponse = await searchLocations(text);
    SetLoading(false);
    return locationResponse.data;
  };

  onLocationSelect = async (item) => {
    let locationResponse = await getLocation(item.id);
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, locationResponse.data);
  };

  onCreateLocation = async () => {
    let locationResponse = await createLocation();
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, locationResponse.data);
  };

  onSave = async () => {
    SetLoading(true, "Saving Location");
    let location = Store.get(StoreKeys.LOCATIONBUILDER.LOCATION);
    let locationResponse = await saveLocation(location);
    SetLoading(false);
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, locationResponse.data);
  };
}

export function LocationBuilder() {
  return connect(<LocationBuilderC />, propKeys);
}
