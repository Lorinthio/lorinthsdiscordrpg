﻿import * as React from "react";
import Quest from "./quest/Quest.jsx";
import { getQuest, createQuest, saveQuest, searchQuests } from "./quest/api.js";
import {
  getClasses,
  getRaces,
  getObjectiveTypes,
} from "../../common/lookupApi.js";
import { IconButton } from "../../common/components/IconButton.jsx";

import { MdAddCircleOutline } from "react-icons/md";
import Store from "../../common/state/store.js";
import StoreKeys from "../../common/state/storeKeys.js";
import { SetLoading } from "../../common/components/Loading.jsx";
import SearchInput from "../../common/components/SearchInput.jsx";
import { connect } from "../../common/state/connect.jsx";

const propKeys = {
  quest: StoreKeys.QUESTBUILDER.QUEST,
};

class QuestBuilderC extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedQuestId: undefined,
      quests: [],
      classes: [],
      races: [],
      objectiveTypes: [],
    };

    this.loadLookups = this.loadLookups.bind(this);
    this.onCreateQuest = this.onCreateQuest.bind(this);
    this.onQuestSelect = this.onQuestSelect.bind(this);
    this.onQuestSearch = this.onQuestSearch.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentDidMount() {
    SetLoading(true, "Loading Quest Lookups");
    this.loadLookups();
    var onSave = this.onSave;
    document.addEventListener(
      "keydown",
      function (e) {
        if (
          (window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) &&
          e.keyCode === 83
        ) {
          e.preventDefault();
          onSave();
        }
      },
      false
    );
  }

  render() {
    return (
      <div className="quest-builder">
        <h1>Quest Builder</h1>
        <SearchInput
          ownerKey="QuestBuilder"
          onSearch={this.onQuestSearch}
          onSelect={this.onQuestSelect}
          options={this.state.quests}
          placeholder="Search For a Quest"
        />
        <IconButton
          Component={<MdAddCircleOutline />}
          text="Create Quest"
          onClick={this.onCreateQuest}
        />

        {this.props.quest !== undefined ? (
          <Quest
            quest={this.props.quest}
            classes={this.state.classes}
            races={this.state.races}
            objectiveTypes={this.state.objectiveTypes}
            saveCallback={this.onSave}
          />
        ) : (
          <></>
        )}
      </div>
    );
  }

  loadLookups = async () => {
    let [
      classesResponse,
      racesResponse,
      objectiveTypesResponse,
    ] = await Promise.all([getClasses(), getRaces(), getObjectiveTypes()]);

    SetLoading(false);
    this.setState({
      classes: classesResponse.data,
      races: racesResponse.data,
      objectiveTypes: objectiveTypesResponse.data,
    });
  };

  onQuestSearch = async (text) => {
    let quests = await searchQuests(text);
    SetLoading(false);
    return quests.data;
  };

  onQuestSelect = async (item) => {
    let questResponse = await getQuest(item.id);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, questResponse.data);
  };

  onCreateQuest = async () => {
    let questResponse = await createQuest();
    Store.set(StoreKeys.QUESTBUILDER.QUEST, questResponse.data);
  };

  onSave = async () => {
    SetLoading(true, "Saving Quest");
    let quest = Store.get(StoreKeys.QUESTBUILDER.QUEST);
    let questResponse = await saveQuest(quest);
    SetLoading(false);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, questResponse.data);
  };
}

export function QuestBuilder() {
  return connect(<QuestBuilderC />, propKeys);
}
