import * as React from "react";
import { syncTables } from "./api/syncApi";
import { SetLoading } from "../../common/components/Loading";

export default class SyncPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      all: false,
      item: false,
      armor: false,
      weapon: false,
      syncResponse: null,
    };

    this.onChange = this.onChange.bind(this);
    this.onChangeAll = this.onChangeAll.bind(this);
    this.startSync = this.startSync.bind(this);
  }

  render() {
    return (
      <>
        <>
          <table>
            <tbody>
              <tr>
                <td>All</td>
                <td>
                  <input
                    type="checkbox"
                    checked={this.state.all}
                    onChange={this.onChangeAll}
                  />
                </td>
              </tr>
              <tr>
                <td>Item</td>
                <td>
                  <input
                    type="checkbox"
                    checked={this.state.item}
                    onChange={(e) => this.onChange(e, "item")}
                  />
                </td>
              </tr>
              <tr>
                <td>Armor</td>
                <td>
                  <input
                    type="checkbox"
                    checked={this.state.armor}
                    onChange={(e) => this.onChange(e, "armor")}
                  />
                </td>
              </tr>
              <tr>
                <td>Weapon</td>
                <td>
                  <input
                    type="checkbox"
                    checked={this.state.weapon}
                    onChange={(e) => this.onChange(e, "weapon")}
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <button onClick={this.startSync}>Sync Selected</button>
        </>
        <>
          {this.state.syncResponse && (
            <div>
              {this.state.syncResponse.map((tableRecord) => {
                console.log(tableRecord);
                return (
                  <div key={tableRecord.tableName}>
                    <label>
                      {tableRecord.tableName} {" -> "}{" "}
                      {tableRecord.success ? "Success!" : "Failed"}
                      {" { "} {tableRecord.errorMessage} {" } "}
                      {" [ "} {tableRecord.innerErrorMessage} {" ] "}
                    </label>
                  </div>
                );
              })}
            </div>
          )}
        </>
      </>
    );
  }

  onChangeAll(e) {
    let checked = e.currentTarget.checked;
    let state = {};
    for (let key in this.state) {
      state[key] = checked;
    }

    state.syncResponse = this.state.syncResponse;

    this.setState(state);
  }

  onChange(e, name) {
    let checked = e.currentTarget.checked;
    let update = {};
    update[name] = checked;
    this.setState(update);
  }

  startSync = async () => {
    const request = this.state;
    console.log(request);
    SetLoading(true, "Sync Tables");
    const syncResponse = await syncTables(request);
    SetLoading(false);

    this.setState({
      syncResponse,
    });
  };
}
