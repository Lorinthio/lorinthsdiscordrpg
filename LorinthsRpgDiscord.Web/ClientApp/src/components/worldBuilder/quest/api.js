import { ToastsStore } from "react-toasts";
var axios = require("axios");

const questRoute = "/api/quest";

export const getQuest = async (questId) => {
  try {
    return await axios.get(`${questRoute}/get/` + questId);
  } catch (error) {
    ToastsStore.error("Error getting quest: " + error, 3000, "custom-toast");
  }
};

export const createQuest = async () => {
  try {
    return await axios.get(`${questRoute}/create`);
  } catch (error) {
    ToastsStore.error("Error creating quest: " + error, 3000, "custom-toast");
  }
};

export const saveQuest = async (quest) => {
  try {
    let saveResponse = await axios.post(`${questRoute}/save`, quest);
    ToastsStore.success("Quest Saved!", 3000, "custom-toast");
    return saveResponse;
  } catch (error) {
    ToastsStore.error("Error saving quest: " + error, 3000, "custom-toast");
    return null;
  }
};

export const getLookups = async () => {
  try {
    return await axios.get(`${questRoute}/lookups`);
  } catch (error) {
    ToastsStore.error("Error fetching lookups: " + error, 3000, "custom-toast");
  }
};

export const searchQuests = async (searchValue) => {
  try {
    searchValue = searchValue ?? "";
    return await axios.post(`${questRoute}/search`, { searchValue });
  } catch (error) {
    ToastsStore.error("Error searching quests: " + error, 3000, "custom-toast");
  }
};
