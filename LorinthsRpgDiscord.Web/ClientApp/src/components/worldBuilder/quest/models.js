export class ViewState {
  constructor() {
    this.detailsOpen = true;
    this.requirementsOpen = true;
    this.stepsOpen = [];
    this.objectivesOpen = [];
  }
}
