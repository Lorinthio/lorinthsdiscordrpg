import * as React from "react";
import Store from "../../../../../common/state/store";
import StoreKeys from "../../../../../common/state/storeKeys";
import {
  FaArrowDown,
  FaArrowUp,
  FaRegTrashAlt,
  FaRegPlusSquare,
  FaRegMinusSquare,
} from "react-icons/fa";
import { ObjectiveTypes } from "../../../../../common/enums/QuestStepObjectiveTypes";
import SearchInput from "../../../../../common/components/SearchInput";
import { searchLocations } from "../../../location/api";

export default class Objective extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: [],
      targetOptions: [],
    };

    this.onDescriptionChange = this.onDescriptionChange.bind(this);
    this.onTypeChange = this.onTypeChange.bind(this);
    this.onMoveUp = this.onMoveUp.bind(this);
    this.onMoveDown = this.onMoveDown.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onOpen = this.onOpen.bind(this);
    this.renderTargetOptions = this.renderTargetOptions.bind(this);
    this.onTargetClear = this.onTargetClear.bind(this);
    this.onTargetSearch = this.onTargetSearch.bind(this);
    this.onTargetSelect = this.onTargetSelect.bind(this);
  }

  render() {
    const isOpen = Store.get(
      StoreKeys.QUESTBUILDER.VIEW.OBJECTIVE_OPEN.replace(
        "{step}",
        this.props.stepIndex
      ).replace("{objective", this.props.index),
      true
    );
    return (
      <div className="objective">
        <div className="header">
          <div className="name">
            {isOpen ? (
              <FaRegMinusSquare onClick={this.onClose} />
            ) : (
              <FaRegPlusSquare onClick={this.onOpen} />
            )}
            <h3>{this.props.objective.description}</h3>
          </div>

          <div className="actions">
            {!this.props.isFirst && (
              <FaArrowUp
                color="beige"
                title="Move Up"
                onClick={this.onMoveUp}
              />
            )}
            {!this.props.isLast && (
              <FaArrowDown
                color="beige"
                title="Move Down"
                onClick={this.onMoveDown}
              />
            )}
            <FaRegTrashAlt
              color="beige"
              title="Delete"
              onClick={this.onDelete}
            />
          </div>
        </div>
        <div className="form" hidden={!isOpen}>
          <table>
            <tbody>
              <tr>
                <td>
                  <label>Description</label>
                </td>
                <td>
                  <textarea
                    value={this.props.objective.description}
                    onChange={this.onDescriptionChange}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <label>Type</label>
                </td>
                <td>
                  <select
                    required={true}
                    onChange={this.onTypeChange}
                    value={this.props.objective.questStepObjectiveTypeId}
                  >
                    {this.props.objectiveTypes}
                  </select>
                </td>
              </tr>
              <tr>
                <td>Target</td>
                <td>{this.renderTargetOptions()}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  onClose() {
    Store.set(
      StoreKeys.QUESTBUILDER.VIEW.OBJECTIVE_OPEN.replace(
        "{step}",
        this.props.stepIndex
      ).replace("{objective", this.props.index),
      false
    );
  }

  onOpen() {
    Store.set(
      StoreKeys.QUESTBUILDER.VIEW.OBJECTIVE_OPEN.replace(
        "{step}",
        this.props.stepIndex
      ).replace("{objective", this.props.index),
      true
    );
  }

  onDescriptionChange(event) {
    this.props.objective.description = event.currentTarget.value;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onTypeChange(event) {
    this.props.objective.questStepObjectiveTypeId = parseInt(
      event.target.value
    );
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onMoveDown() {
    let step = this.props.step;
    let currentObjective = this.props.objective;
    let otherObjective = step.questStepObjective[this.props.index + 1];

    step.questStepObjective[this.props.index] = otherObjective;
    step.questStepObjective[this.props.index + 1] = currentObjective;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onMoveUp() {
    let step = this.props.step;
    let currentObjective = this.props.objective;
    let otherObjective = step.questStepObjective[this.props.index - 1];

    step.questStepObjective[this.props.index] = otherObjective;
    step.questStepObjective[this.props.index - 1] = currentObjective;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onDelete() {
    let step = this.props.step;
    step.questStepObjective.splice(this.props.index, 1);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  renderTargetOptions() {
    return (
      <SearchInput
        ownerKey={`${this.props.quest.id}_${this.props.objective.id}_${this.props.index}`}
        onClear={this.onTargetClear}
        onSearch={this.onTargetSearch}
        onSelect={this.onTargetSelect}
        options={this.state.targetOptions}
        placeholder="Target"
      />
    );
  }

  onTargetSearch = async (text) => {
    let targetOptions;
    let response;

    switch (this.props.objective.questStepObjectiveTypeId) {
      case ObjectiveTypes.Explore:
        response = await searchLocations(text);
        targetOptions = response.data;
        break;
      default:
        targetOptions = [];
        break;
    }

    return targetOptions;
  };

  onTargetSelect = async (item) => {
    this.props.objective.targetId = item.id;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  };

  onTargetClear = async () => {
    this.props.objective.targetId = null;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  };
}
