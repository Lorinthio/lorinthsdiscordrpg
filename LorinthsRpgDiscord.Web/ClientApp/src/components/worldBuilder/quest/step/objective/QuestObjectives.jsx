import * as React from "react";
import CollapseableSection from "../../../../../common/components/CollapseableSection.jsx";
import StoreKeys from "../../../../../common/state/storeKeys.js";
import Objective from "./Objective.jsx";
import Store from "../../../../../common/state/store.js";

export default class QuestObjectives extends React.Component {
  constructor(props) {
    super(props);

    this.onObjectiveAdd = this.onObjectiveAdd.bind(this);
  }

  render() {
    const objectives = this.props.objectives.map((objective, index) => {
      return (
        <Objective
          key={index}
          index={index}
          quest={this.props.quest}
          step={this.props.step}
          stepIndex={this.props.stepIndex}
          objective={objective}
          objectiveTypes={this.props.objectiveTypes}
          isFirst={index === 0}
          isLast={index === this.props.objectives.length - 1}
        />
      );
    });
    return (
      <>
        <CollapseableSection
          viewKey={StoreKeys.QUESTBUILDER.VIEW.OBJECTIVES.replace(
            "{step}",
            this.props.stepIndex
          )}
          title="Objectives"
          className="objectives-list"
        >
          {objectives}
          <button onClick={this.onObjectiveAdd}>Add Objective</button>
        </CollapseableSection>
      </>
    );
  }

  onObjectiveAdd() {
    this.props.step.questStepObjective.push({
      id: 0,
      questStepId: this.props.step.id,
      questStepObjectiveTypeId: 1,
      description:
        "New Objective " + (this.props.step.questStepObjective.length + 1),
    });
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }
}
