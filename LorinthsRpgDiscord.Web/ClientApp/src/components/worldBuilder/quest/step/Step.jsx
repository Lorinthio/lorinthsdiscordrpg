﻿import * as React from "react";
import {
  FaArrowDown,
  FaArrowUp,
  FaRegTrashAlt,
  FaRegPlusSquare,
  FaRegMinusSquare,
} from "react-icons/fa";
import Store from "../../../../common/state/store";
import StoreKeys from "../../../../common/state/storeKeys";
import QuestObjectives from "./objective/QuestObjectives.jsx";

export default class Step extends React.Component {
  constructor(props) {
    super(props);

    this.onNameChange = this.onNameChange.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);

    this.onMoveDown = this.onMoveDown.bind(this);
    this.onMoveUp = this.onMoveUp.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onOpen = this.onOpen.bind(this);
  }

  render() {
    const isOpen = Store.get(
      StoreKeys.QUESTBUILDER.VIEW.STEP_OPEN.replace("{step}", this.props.index),
      true
    );
    return (
      <div className="step">
        <div className="header">
          <div className="name">
            {isOpen ? (
              <FaRegMinusSquare onClick={this.onClose} />
            ) : (
              <FaRegPlusSquare onClick={this.onOpen} />
            )}
            <h3>{this.props.step.name}</h3>
          </div>

          <div className="actions">
            {!this.props.isFirst && (
              <FaArrowUp
                color="beige"
                title="Move Up"
                onClick={this.onMoveUp}
              />
            )}
            {!this.props.isLast && (
              <FaArrowDown
                color="beige"
                title="Move Down"
                onClick={this.onMoveDown}
              />
            )}
            <FaRegTrashAlt
              color="beige"
              title="Delete"
              onClick={this.onDelete}
            />
          </div>
        </div>
        <div className="form" hidden={!isOpen}>
          <table>
            <tbody>
              <tr>
                <td>
                  <label>Name</label>
                </td>
                <td>
                  <input
                    value={this.props.step.name}
                    onChange={this.onNameChange}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <label>Description</label>
                </td>
                <td>
                  <textarea
                    value={this.props.step.description}
                    onChange={this.onDescriptionChange}
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <QuestObjectives
            quest={this.props.quest}
            step={this.props.step}
            stepIndex={this.props.index}
            objectives={this.props.step.questStepObjective}
            objectiveTypes={this.props.objectiveTypes}
          />
        </div>
      </div>
    );
  }

  onClose() {
    Store.set(
      StoreKeys.QUESTBUILDER.VIEW.STEP_OPEN.replace("{step}", this.props.index),
      false
    );
  }

  onOpen() {
    Store.set(
      StoreKeys.QUESTBUILDER.VIEW.STEP_OPEN.replace("{step}", this.props.index),
      true
    );
  }

  onNameChange(event) {
    this.props.step.name = event.currentTarget.value;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onDescriptionChange(event) {
    this.props.step.description = event.currentTarget.value;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onMoveDown() {
    let quest = this.props.quest;
    let currentStep = this.props.step;
    let otherStep = this.props.quest.questStep[this.props.index + 1];

    quest.questStep[this.props.index] = otherStep;
    quest.questStep[this.props.index + 1] = currentStep;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onMoveUp() {
    let quest = this.props.quest;
    let currentStep = this.props.step;
    let otherStep = this.props.quest.questStep[this.props.index - 1];

    quest.questStep[this.props.index] = otherStep;
    quest.questStep[this.props.index - 1] = currentStep;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onDelete() {
    let quest = this.props.quest;
    quest.questStep.splice(this.props.index, 1);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }
}
