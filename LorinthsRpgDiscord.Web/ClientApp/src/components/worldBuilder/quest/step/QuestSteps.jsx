import * as React from "react";
import { MdAddCircleOutline } from "react-icons/md";

import { IconButton } from "../../../../common/components/IconButton.jsx";
import Step from "./Step.jsx";
import Store from "../../../../common/state/store.js";
import StoreKeys from "../../../../common/state/storeKeys.js";
import CollapseableSection from "../../../../common/components/CollapseableSection.jsx";

export default class QuestSteps extends React.Component {
  constructor(props) {
    super(props);

    this._container = React.createRef();
    this.addStep = this.addStep.bind(this);
  }

  render() {
    const steps = this.props.steps.map((step, index) => {
      return (
        <Step
          key={index}
          index={index}
          quest={this.props.quest}
          step={step}
          objectiveTypes={this.props.objectiveTypes}
          isFirst={index === 0}
          isLast={index === this.props.steps.length - 1}
        />
      );
    });
    return (
      <div className="quest-steps">
        <CollapseableSection
          viewKey={StoreKeys.QUESTBUILDER.VIEW.STEPS}
          title="Steps"
        >
          <div className="quest-steps-container" ref={this._container}>
            {steps}
          </div>
          <IconButton
            Component={<MdAddCircleOutline />}
            text="Add Step"
            onClick={this.addStep}
          />
        </CollapseableSection>
      </div>
    );
  }

  addStep() {
    const newStep = {
      id: 0,
      name: "New Step " + (this.props.steps.length + 1),
      description: "",
      questId: this.props.quest.id,
      questStepObjective: [],
    };

    let steps = [...this.props.steps, newStep];
    this.props.quest.questStep = steps;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }
}
