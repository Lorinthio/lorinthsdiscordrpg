﻿import * as React from "react";
import QuestSteps from "./step/QuestSteps.jsx";
import Store from "../../../common/state/store";
import StoreKeys from "../../../common/state/storeKeys";

import CollapseableSection from "../../../common/components/CollapseableSection.jsx";

import "./Quest.css";

export default class Quest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      detailsOpen: true,
      requirementsOpen: true,
    };

    this.onNameChange = this.onNameChange.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);
    this.onActiveChange = this.onActiveChange.bind(this);
    this.onLevelChange = this.onLevelChange.bind(this);
    this.onClassChange = this.onClassChange.bind(this);
    this.onRaceChange = this.onRaceChange.bind(this);
  }

  render() {
    const noRequirement = (
      <option key="" value="">
        No Requirement
      </option>
    );
    const classOptions = this.props.classes.map((c) => {
      return (
        <option key={c.id} value={c.id}>
          {c.name}
        </option>
      );
    });
    const raceOptions = this.props.races.map((r) => {
      return (
        <option key={r.id} value={r.id}>
          {r.name}
        </option>
      );
    });
    const objectiveTypes = this.props.objectiveTypes.map((objectiveType) => {
      return (
        <option key={objectiveType.id} value={objectiveType.id}>
          {objectiveType.name}
        </option>
      );
    });

    classOptions.unshift(noRequirement);
    raceOptions.unshift(noRequirement);

    return (
      <>
        <div className="quest">
          <CollapseableSection
            title="Quest Details"
            viewKey={StoreKeys.QUESTBUILDER.VIEW.DETAILS}
          >
            <table>
              <tbody>
                {this.props.quest.id !== 0 ? (
                  <tr>
                    <td>
                      <label>Id</label>
                    </td>
                    <td>
                      <input
                        value={this.props.quest.id ?? undefined}
                        disabled={true}
                      />
                    </td>
                  </tr>
                ) : (
                  <></>
                )}
                <tr>
                  <td>
                    <label>Name</label>
                  </td>
                  <td>
                    <input
                      value={this.props.quest.name ?? ""}
                      onChange={this.onNameChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Description</label>
                  </td>
                  <td>
                    <textarea
                      value={this.props.quest.description ?? ""}
                      onChange={this.onDescriptionChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Enabled?</label>
                  </td>
                  <td>
                    <input
                      type="checkbox"
                      checked={this.props.quest.isActive ?? false}
                      onChange={this.onActiveChange}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </CollapseableSection>

          <CollapseableSection
            title="Quest Requirements"
            viewKey={StoreKeys.QUESTBUILDER.VIEW.REQUIREMENTS}
          >
            <table>
              <tbody>
                <tr>
                  <td>
                    <label>Level</label>
                  </td>
                  <td>
                    <input
                      value={this.props.quest.levelRequired || ""}
                      onChange={this.onLevelChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Class</label>
                  </td>
                  <td>
                    <select
                      value={this.props.quest.classIdRequired || ""}
                      onChange={this.onClassChange}
                    >
                      {classOptions}
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Race</label>
                  </td>
                  <td>
                    <select
                      value={this.props.quest.raceIdRequired || ""}
                      onChange={this.onRaceChange}
                    >
                      {raceOptions}
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
          </CollapseableSection>

          <QuestSteps
            quest={this.props.quest}
            steps={this.props.quest.questStep}
            objectiveTypes={objectiveTypes}
          />
        </div>
        <hr />
        <button onClick={this.props.saveCallback}>Save</button>
      </>
    );
  }

  onNameChange(event) {
    this.props.quest.name = event.currentTarget.value;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onDescriptionChange(event) {
    this.props.quest.description = event.currentTarget.value;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onLevelChange(event) {
    let levelRequired = parseInt(event.target.value.replace(/\D/, ""));
    this.props.quest.levelRequired = levelRequired;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onActiveChange(event) {
    this.props.quest.isActive = event.currentTarget.checked;
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onClassChange(event) {
    this.props.quest.classIdRequired = parseInt(event.target.value);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }

  onRaceChange(event) {
    this.props.quest.raceIdRequired = parseInt(event.target.value);
    Store.set(StoreKeys.QUESTBUILDER.QUEST, this.props.quest);
  }
}
