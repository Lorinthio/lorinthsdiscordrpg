import { ToastsStore } from "react-toasts";
var axios = require("axios");

const locationRoute = "/api/location";

export const getLocation = async (locationId) => {
  try {
    return await axios.get(`${locationRoute}/get/` + locationId);
  } catch (error) {
    ToastsStore.error("Error getting location: " + error, 3000, "custom-toast");
  }
};

export const searchLocations = async (searchValue) => {
  try {
    searchValue = searchValue ?? "";
    return await axios.post(`${locationRoute}/search`, { searchValue });
  } catch (error) {
    ToastsStore.error(
      "Error searching locations: " + error,
      3000,
      "custom-toast"
    );
  }
};

export const createLocation = async () => {
  try {
    return await axios.get(`${locationRoute}/create`);
  } catch (error) {
    ToastsStore.error(
      "Error creating Location: " + error,
      3000,
      "custom-toast"
    );
  }
};

export const saveLocation = async (location) => {
  try {
    return await axios.post(`${locationRoute}/save`, location);
  } catch (error) {
    ToastsStore.error(
      "Error creating Location: " + error,
      3000,
      "custom-toast"
    );
  }
};
