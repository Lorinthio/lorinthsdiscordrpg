import * as React from "react";
import CollapseableSection from "../../../common/components/CollapseableSection";
import StoreKeys from "../../../common/state/storeKeys";
import Store from "../../../common/state/store";
import SearchInput from "../../../common/components/SearchInput";
import { SetLoading } from "../../../common/components/Loading";
import { searchLocations } from "./api";

export default class Location extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: [],
    };

    this.onNameChange = this.onNameChange.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);
    this.onParentLocationSearch = this.onParentLocationSearch.bind(this);
    this.onParentLocationSelect = this.onParentLocationSelect.bind(this);
    this.onParentLocationClear = this.onParentLocationClear.bind(this);
    this.onLocationTypeChange = this.onLocationTypeChange.bind(this);
  }

  render() {
    const locationTypeOptions = this.props.locationTypes.map((c) => {
      return (
        <option key={c.id} value={c.id}>
          {c.name}
        </option>
      );
    });
    return (
      <>
        <div className="location">
          <CollapseableSection
            title="Location Details"
            viewKey={StoreKeys.LOCATIONBUILDER.VIEW.DETAILS}
          >
            <table>
              <tbody>
                {this.props.location.id !== 0 ? (
                  <tr>
                    <td>
                      <label>Id</label>
                    </td>
                    <td>
                      <input
                        value={this.props.location.id ?? undefined}
                        disabled={true}
                      />
                    </td>
                  </tr>
                ) : (
                  <></>
                )}
                <tr>
                  <td>
                    <label>Name</label>
                  </td>
                  <td>
                    <input
                      value={this.props.location.name ?? ""}
                      onChange={this.onNameChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Description</label>
                  </td>
                  <td>
                    <textarea
                      value={this.props.location.description ?? ""}
                      onChange={this.onDescriptionChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Type</label>
                  </td>
                  <td>
                    <select
                      value={this.props.location.locationTypeId || ""}
                      onChange={this.onLocationTypeChange}
                    >
                      {locationTypeOptions}
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Parent Location</label>
                  </td>
                  <td>
                    <SearchInput
                      onSearch={this.onParentLocationSearch}
                      onSelect={this.onParentLocationSelect}
                      onClear={this.onParentLocationClear}
                      options={this.state.locations}
                      placeholder="Search For a Location"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </CollapseableSection>
        </div>
        <hr />
        <button onClick={this.props.saveCallback}>Save</button>
      </>
    );
  }

  onNameChange(event) {
    this.props.location.name = event.currentTarget.value;
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, this.props.location);
  }

  onDescriptionChange(event) {
    this.props.location.description = event.currentTarget.value;
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, this.props.location);
  }

  onLocationTypeChange(event) {
    this.props.location.locationTypeId = parseInt(event.target.value);
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, this.props.location);
  }

  onParentLocationSearch = async (text) => {
    let locationResponse = await searchLocations(text);
    SetLoading(false);
    this.setState({
      locations: locationResponse.data,
    });
  };

  onParentLocationSelect = async (item) => {
    this.props.location.parentLocationId = item.id;
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, this.props.location);
  };

  onParentLocationClear = async () => {
    this.props.location.parentLocationId = null;
    Store.set(StoreKeys.LOCATIONBUILDER.LOCATION, this.props.location);
  };
}
