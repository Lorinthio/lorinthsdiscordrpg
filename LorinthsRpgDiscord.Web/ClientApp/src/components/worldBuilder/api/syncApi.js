import { ToastsStore } from "react-toasts";
var axios = require("axios");

const syncRoute = "/api/sync";

export const syncTables = async (request) => {
  try {
    return await axios.post(`${syncRoute}/tables`, request).then((response) => {
      return response.data;
    });
  } catch (error) {
    ToastsStore.error("Error getting location: " + error, 3000, "custom-toast");
  }
};
