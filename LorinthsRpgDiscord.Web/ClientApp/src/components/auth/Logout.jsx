import * as React from "react";

import { handleLogout } from "../../common/api/authApi";
import { SetLoading } from "../../common/components/Loading";

export default class Logout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      discordUrl: null,
    };
  }

  render() {
    return <></>;
  }

  componentDidMount() {
    SetLoading(true, "Logging out");
    handleLogout().then(() => {
      SetLoading(false);
      window.location.href = "/";
    });
  }
}
