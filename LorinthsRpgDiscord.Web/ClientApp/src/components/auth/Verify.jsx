import * as queryString from "query-string";
import * as React from "react";
import { ToastsStore } from "react-toasts";

import { handleDiscordRedirect } from "../../common/api/authApi";
import { checkCookies } from "./UserSession";
import { SetLoading } from "../../common/components/Loading";

export default class Verify extends React.Component {
  render() {
    return <></>;
  }

  componentDidMount() {
    SetLoading(true, "Verifying Login");
    const params = queryString.parse(this.props.location.search);
    if (params.error != null) {
      window.location.href = "/";
    }

    const code = params.code;
    handleDiscordRedirect(code).then(async () => {
      ToastsStore.info("You are logged in!", 3000);

      await checkCookies();
      SetLoading(false);
      window.location.href = "/";
    });
  }
}
