import { AuthConstants } from "../../models/models";

import Cookies from "universal-cookie";
import React from "react";
import Store from "../../common/state/store";
import StoreKeys from "../../common/state/storeKeys";
import { getCurrentUser } from "../../common/api/securityApi";
import { ToastsStore } from "react-toasts";

const cacheKeys = {
  sessionId: "CACHE_SESSIONID",
  lastCheck: "CACHE_LASTCHECK",
  user: "CACHE_USER",
};
const cookies = new Cookies();

export const UserSession = () => {
  checkCookies();
  return <></>;
};

export const checkCookies = async () => {
  const sessionId = cookies.get(AuthConstants.cookies.session);

  if (sessionId == null) {
    localStorage.removeItem(cacheKeys.sessionId);
    localStorage.removeItem(cacheKeys.lastCheck);
    localStorage.removeItem(cacheKeys.user);

    return;
  }

  const gameSessionId = cookies.get(AuthConstants.cookies.gameSession);
  if (gameSessionId != null) {
    Store.set(StoreKeys.GAME.SESSIONID, gameSessionId);
  }

  if (loadFromLocalStorage(sessionId)) {
    return;
  }
  await loadFromApi(sessionId);
};

//this method is made to save on the back and forth on page changes.
const loadFromLocalStorage = (sessionId) => {
  let lastSessionId = localStorage.getItem(cacheKeys.sessionId);
  if (lastSessionId !== sessionId) {
    return false;
  }

  let lastCheck = localStorage.getItem(cacheKeys.lastCheck);
  if (lastCheck == null) {
    return false;
  }

  let lastChecked = new Date(lastCheck);
  let milliDiffs = new Date() - lastChecked;

  //5 minute cache of user and roles
  if (milliDiffs > 5 * 60 * 1000) {
    return false;
  }
  let cacheUser = localStorage.getItem(cacheKeys.user);
  if (cacheUser === undefined) {
    return false;
  }

  let user = JSON.parse(cacheUser);
  Store.set(StoreKeys.USER.CURRENT, user);

  return true;
};

const loadFromApi = async (sessionId) => {
  localStorage.setItem(cacheKeys.sessionId, sessionId);
  localStorage.setItem(cacheKeys.lastCheck, new Date().toString());

  getCurrentUser()
    .then((user) => {
      Store.set(StoreKeys.USER.CURRENT, user);
      localStorage.setItem(cacheKeys.user, JSON.stringify(user));
    })
    .catch((error) => {
      ToastsStore.error(
        "Issue with User Token, you have been signed out",
        3000
      );
    });
};
