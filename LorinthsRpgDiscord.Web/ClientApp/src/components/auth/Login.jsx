import * as React from "react";

import "./Login.css";
import { getDiscordLoginUrl } from "../../common/api/authApi";

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      discordUrl: null,
    };
  }

  render() {
    return (
      <div className="login-container">
        {this.state.discordUrl && (
          <a href={this.state.discordUrl}>Login through discord</a>
        )}
      </div>
    );
  }

  componentDidMount() {
    getDiscordLoginUrl().then((loginUrl) => {
      this.setState({
        discordUrl: loginUrl,
      });
    });
  }
}
