import * as React from "react";
import * as signalR from "@microsoft/signalr";
import { Stage, Layer, Image } from "react-konva";

import "./GameMap.css";
import StoreKeys from "../../../common/state/storeKeys";
import { connect } from "../../../common/state/connect";
import Store from "../../../common/state/store";
import { loadImage } from "../../../common/utils/imageLoader";

const propKeys = {
  map: StoreKeys.GAME.MAP,
};

export class GameMapC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stageWidth: 200,
      stageHeight: 200,
      container: null,
      backgroundImage: null,
      gridImage: null,
    };

    this.mapConnection = null;

    this.loadGameMap = this.loadGameMap.bind(this);
    this.loadGrid = this.loadGrid.bind(this);
  }

  componentDidMount() {
    this.checkSize();
    window.addEventListener("resize", this.checkSize);
    this.loadGrid();

    this.mapConnection = new signalR.HubConnectionBuilder()
      .withUrl("/game/map")
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this.mapConnection.on("GameMap", this.loadGameMap);
    this.mapConnection.start().then(() => {
      this.mapConnection.send("Connect").then(() => {
        this.mapConnection.send("GetMap");
      });
    });
  }

  loadGrid() {
    loadImage("/images/Grid.png").then((image) => {
      this.setState({
        gridImage: image,
      });
    });
  }

  loadGameMap(battleMap) {
    Store.set(StoreKeys.GAME.MAP, battleMap);
    loadImage(battleMap.backgroundImageUrl).then((image) => {
      this.setState({ backgroundImage: image });
    });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.checkSize);
  }

  checkSize = () => {
    const width = this.container.offsetWidth;
    const height = this.container.offsetWidth / 2;
    this.setState({
      stageWidth: width,
      stageHeight: height,
    });
  };

  render() {
    return (
      <div
        className="game-map"
        ref={(node) => {
          this.container = node;
        }}
      >
        {this.state.backgroundImage && this.props.map && (
          <Stage
            width={this.state.stageWidth}
            height={this.state.stageHeight}
            draggable={true}
          >
            <Layer>
              <Image
                x={this.props.map.offset.x}
                y={this.props.map.offset.y}
                image={this.state.backgroundImage}
              />
              <Image
                x={0}
                y={0}
                width={this.state.backgroundImage.width}
                height={this.state.backgroundImage.height}
                crop={{
                  x: 0,
                  y: 0,
                  width: this.state.backgroundImage.width,
                  height: this.state.backgroundImage.height,
                }}
                image={this.state.gridImage}
              />
            </Layer>
          </Stage>
        )}
      </div>
    );
  }
}

export const GameMap = (props) => {
  return connect(<GameMapC />, propKeys, props);
};
