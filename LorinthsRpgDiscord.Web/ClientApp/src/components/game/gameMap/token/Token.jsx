import * as React from "react";
import { Image } from "react-konva";

const gridSize = 70;
export default class Token extends React.Component {
  render() {
    return (
      <>
        <Image
          x={this.props.x * gridSize}
          y={this.props.y * gridSize}
          image={this.props.image}
        />
      </>
    );
  }
}
