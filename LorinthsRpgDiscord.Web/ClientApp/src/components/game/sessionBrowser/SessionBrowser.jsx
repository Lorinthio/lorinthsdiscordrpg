import * as React from "react";

export default class SessionBrowser extends React.Component {
  constructor(props) {
    super(props);

    this.createSession = this.createSession.bind(this);
    this.createComplete = this.createComplete.bind(this);
  }

  componentDidUpdate() {
    if (this.props.connection) {
      this.props.connection.on("Created", this.createComplete);
    }
  }

  componentWillUnmount() {
    this.props.connection.off("Created");
  }

  render() {
    return (
      <>
        <button onClick={this.createSession}>Create Session</button>
      </>
    );
  }

  createSession() {
    this.props.connection.send("Create");
  }

  createComplete(sessionId) {
    this.props.setSession(sessionId);
  }
}
