import * as React from "react";
import { connect } from "../../../common/state/connect";
import StoreKeys from "../../../common/state/storeKeys";
import * as signalR from "@microsoft/signalr";
import {
  ListGroup,
  InputGroup,
  FormControl,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";

import "./ChatWindow.css";

const propKeys = {
  user: StoreKeys.USER.CURRENT,
};

const chatModeToVariant = {
  Group: "info",
  Session: "Dark",
  System: "Warning",
  Npc: "Success",
  Enemy: "Danger",
};

export default class ChatWindowC extends React.Component {
  constructor(props) {
    super(props);

    this.chatConnection = null;
    this.messagesEnd = null;
    this.currentScroll = 0;

    this.state = {
      messages: [],
      chatMessage: "",
      chatMode: "Group",
      stickyChat: true,
    };

    this.handleEnterPressed = this.handleEnterPressed.bind(this);
    this.onChatMessageChange = this.onChatMessageChange.bind(this);
    this.onMessage = this.onMessage.bind(this);
    this.onSelectChatMode = this.onSelectChatMode.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }

  componentDidMount() {
    this.chatConnection = new signalR.HubConnectionBuilder()
      .withUrl("/game/chat")
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this.chatConnection.on("Message", this.onMessage);
    this.chatConnection.start().then(() => {
      this.chatConnection.send("Join", this.props.user.username);
    });
  }

  render() {
    return (
      <div className="game-chat">
        <div className="messages" onScroll={this.onScroll}>
          <ListGroup variant="dark" bg="dark">
            {this.state.messages.map((message, index) => {
              return (
                <ListGroup.Item
                  key={index}
                  variant={chatModeToVariant[message.chatMode]}
                  bg="dark"
                >
                  {message.username} : {message.text}
                </ListGroup.Item>
              );
            })}
            <div
              ref={(el) => {
                this.messagesEnd = el;
              }}
            ></div>
          </ListGroup>
        </div>
        <InputGroup className="mb-3">
          <DropdownButton
            as={InputGroup.Prepend}
            variant="dark"
            bg="dark"
            title={this.state.chatMode}
            id="input-group-dropdown-1"
            className="no-margin"
          >
            <Dropdown.Item
              variant="dark"
              bg="dark"
              onClick={() => this.onSelectChatMode("Group")}
            >
              Group
            </Dropdown.Item>
            <Dropdown.Item
              variant="dark"
              bg="dark"
              onClick={() => this.onSelectChatMode("Session")}
            >
              Session
            </Dropdown.Item>
          </DropdownButton>
          <FormControl
            variant="dark"
            bg="dark"
            placeholder="Type a message..."
            aria-label="Message"
            aria-describedby="basic-addon1"
            value={this.state.chatMessage}
            onKeyDown={this.handleEnterPressed}
            onChange={this.onChatMessageChange}
          />
        </InputGroup>
      </div>
    );
  }

  onScroll(e) {
    const target = e.target;

    // Calculate Direction
    const directionUp = target.scrollTop - this.currentScroll < 0;
    this.currentScroll = target.scrollTop;

    if (directionUp) {
      this.setState({
        stickyChat: false,
      });
    } else if (target.scrollTop >= target.scrollHeight - target.offsetHeight) {
      this.setState({
        stickyChat: true,
      });
    }
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };

  onChatMessageChange(e) {
    this.setState({
      chatMessage: e.target.value,
    });
  }

  handleEnterPressed(e) {
    if (e.key === "Enter") {
      this.sendMessage();
    }
  }

  sendMessage() {
    let message = this.state.chatMessage.trim();
    if (message.length === 0) {
      return;
    }
    this.setState({
      chatMessage: "",
    });

    let chatMode = this.state.chatMode;
    this.chatConnection.invoke("Message", chatMode, message);
  }

  onMessage(username, chatMode, text) {
    let newMessage = { username, chatMode, text };
    this.state.messages.push(newMessage);

    this.setState({
      messages: this.state.messages.slice(),
    });

    if (this.state.stickyChat) {
      this.scrollToBottom();
    }
  }

  onSelectChatMode(chatMode) {
    this.setState({
      chatMode,
    });
  }
}

export const ChatWindow = (props) => {
  return connect(<ChatWindowC />, propKeys, props);
};
