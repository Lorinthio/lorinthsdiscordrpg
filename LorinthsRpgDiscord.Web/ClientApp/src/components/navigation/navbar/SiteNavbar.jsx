import * as React from "react";
import { connect } from "../../../common/state/connect";
import StoreKeys from "../../../common/state/storeKeys";

import "./SiteNavbar.css";
import { ROLES, userHasRole } from "../../../common/utils/roleChecker";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";

const propKeys = {
  user: StoreKeys.USER.CURRENT,
  roles: StoreKeys.USER.ROLES,
};

class SiteNavbarC extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      worldBuilderOpen: false,
    };

    this.onWorldBuilderHover = this.onWorldBuilderHover.bind(this);
    this.onWorldBuilderLeave = this.onWorldBuilderLeave.bind(this);
  }

  render() {
    const hasWorldBuilderAccess = userHasRole(ROLES.WorldBuilder);
    return (
      <>
        <Navbar variant="dark" expand="lg">
          <Navbar.Brand href="/">
            <img src="/images/LLRpg.png" alt="logo" />
            LorinthsLair
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-end"
          >
            <Nav className="mr-auto">
              <Nav.Link href="/">Home</Nav.Link>
              {hasWorldBuilderAccess && (
                <>
                  <NavDropdown title="World Builder" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/worldbuilder/location">
                      Location
                    </NavDropdown.Item>
                    <NavDropdown.Item href="/worldbuilder/quest">
                      Quest
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/worldbuilder/sync">
                      Sync
                    </NavDropdown.Item>
                  </NavDropdown>
                  <Nav.Link href="/mapbuilder">Map Builder</Nav.Link>
                </>
              )}
              <Nav.Link href="/contact">Contact</Nav.Link>
            </Nav>
            <NavDropdown.Divider />
            <Nav>
              {this.props.user != null && (
                <Nav.Link href="/logout">Sign Out</Nav.Link>
              )}
              {this.props.user == null && (
                <Nav.Link href="/login">Sign In</Nav.Link>
              )}
            </Nav>
            {this.props.user != null && (
              <Nav.Item>
                <img
                  className="avatar"
                  alt="Your Avatar"
                  src={this.getUserAvatar()}
                />
              </Nav.Item>
            )}
          </Navbar.Collapse>
        </Navbar>
      </>
    );
  }

  /*
  render() {
    const hasWorldBuilderAccess = userHasRole(ROLES.WorldBuilder);
    return (
      <div className="nav-bar">
        <ul>
          <li className="main" onClick={() => this.onLinkClick("/")}>
            <label>LorinthsLair</label>
          </li>
          <li onClick={() => this.onLinkClick("/")}>
            <label>Home</label>
          </li>
          {hasWorldBuilderAccess && (
            <>
              <li
                onMouseEnter={this.onWorldBuilderHover}
                onMouseLeave={this.onWorldBuilderLeave}
                onClick={() => this.onLinkClick("/worldbuilder")}
              >
                {this.state.worldBuilderOpen && (
                  <div className="dropdown-options-div">
                    <ul>
                      <li
                        onClick={(e) =>
                          this.onLinkClick("/worldbuilder/location", e, true)
                        }
                      >
                        Location
                      </li>
                      <li
                        onClick={(e) =>
                          this.onLinkClick("/worldbuilder/quest", e, true)
                        }
                      >
                        Quest
                      </li>
                      <li
                        onClick={(e) =>
                          this.onLinkClick("/worldbuilder/sync", e, true)
                        }
                      >
                        Sync
                      </li>
                    </ul>
                  </div>
                )}
                <label>
                  World Builder <FaChevronDown color="beige" />
                </label>
              </li>
              <li onClick={() => this.onLinkClick("/mapbuilder")}>
                <label>Map Builder</label>
              </li>
            </>
          )}
          <li onClick={() => this.onLinkClick("/contact")}>
            <label>Contact Us</label>
          </li>
        </ul>
        <ul className="right">
          {this.props.user != null && (
            <>
              <li style={{ display: "contents" }}>
                <img
                  className="avatar"
                  alt="Your Avatar"
                  src={this.getUserAvatar()}
                />
              </li>
              <li
                style={{ float: "inherit" }}
                onClick={() => this.onLinkClick("/logout")}
              >
                Log out
              </li>
            </>
          )}
          {this.props.user == null && (
            <li onClick={() => this.onLinkClick("/login")}>Log in</li>
          )}
        </ul>
      </div>
    );
  }
  */

  getUserAvatar() {
    if (this.props.user) {
      return `https://cdn.discordapp.com/avatars/${this.props.user.id}/${this.props.user.avatar}.png`;
    } else {
      return "https://cdn.discordapp.com/embed/avatars/1.png";
    }
  }

  onWorldBuilderHover() {
    this.setState({
      worldBuilderOpen: true,
    });
  }
  onWorldBuilderLeave() {
    this.setState({
      worldBuilderOpen: false,
    });
  }

  onLinkClick(url, e, stopPropagation) {
    window.location.href = url;
    if (e && stopPropagation) {
      e.stopPropagation();
    }
  }
}

export function SiteNavbar() {
  return connect(<SiteNavbarC />, propKeys);
}
