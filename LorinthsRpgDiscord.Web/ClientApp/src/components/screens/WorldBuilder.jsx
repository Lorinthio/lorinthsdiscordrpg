import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { LocationBuilder } from "../worldBuilder/LocationBuilder";
import { QuestBuilder } from "../worldBuilder/QuestBuilder";
import SyncPage from "../worldBuilder/SyncPage";
import { userHasRole, ROLES } from "../../common/utils/roleChecker";

export default class WorldBuilder extends React.Component {
  componentDidMount() {
    const hasAccess = userHasRole(ROLES.WorldBuilder);
    if (!hasAccess) {
      window.location.href = "/";
    }
  }
  render() {
    return (
      <div className="world-builder">
        <Switch>
          <Route path="/worldbuilder/location">
            {this.renderLocationBuilder()}
          </Route>
          <Route path="/worldbuilder/quest">{this.renderQuestBuilder()}</Route>
          <Route path="/worldbuilder/sync">{this.renderSyncPage()}</Route>
          <Redirect to="/worldbuilder/location" />
        </Switch>
      </div>
    );
  }

  renderLocationBuilder() {
    return <LocationBuilder />;
  }

  renderQuestBuilder() {
    return <QuestBuilder />;
  }

  renderSyncPage() {
    return <SyncPage />;
  }
}
