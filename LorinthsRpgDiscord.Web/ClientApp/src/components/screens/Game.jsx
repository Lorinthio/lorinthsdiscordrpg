import * as React from "react";
import * as queryString from "query-string";
import { connect } from "../../common/state/connect";
import StoreKeys from "../../common/state/storeKeys";
import { Container, Row, Col } from "react-bootstrap";
import { ChatWindow } from "../game/chatWindow/ChatWindow";
import { GameMap } from "../game/gameMap/GameMap";
import Store from "../../common/state/store";
import SessionBrowser from "../game/sessionBrowser/SessionBrowser";
import * as signalR from "@microsoft/signalr";
import Cookies from "universal-cookie";
import { AuthConstants } from "../../models/models";

const propKeys = {
  sessionId: StoreKeys.GAME.SESSIONID,
  user: StoreKeys.USER.CURRENT,
};

export default class GameC extends React.Component {
  constructor(props) {
    super(props);

    this.setSession = this.setSession.bind(this);
  }

  componentDidMount() {
    Store.wait(StoreKeys.USER.CURRENT).then((value) => {
      if (value == null) {
        window.location.href = "/login";
      }
    });

    const params = queryString.parse(this.props.location.search);
    const sessionId = params.sessionId;

    this.gameSessionConnection = new signalR.HubConnectionBuilder()
      .withUrl("/game/session")
      .configureLogging(signalR.LogLevel.Information)
      .build();
    this.gameSessionConnection.start().then(() => {
      if (sessionId) {
        this.setSession(sessionId);
      }
    });

    this.forceUpdate();
  }

  setSession(sessionId) {
    // Store cookie for access later
    const cookies = new Cookies();
    cookies.set(AuthConstants.cookies.gameSession, sessionId);

    // Update our store cache for component access
    Store.set(StoreKeys.GAME.SESSIONID, sessionId);
  }

  render() {
    const inGame = this.props.sessionId !== undefined;
    return (
      <>
        {!inGame && (
          <Container className="game-session-browser">
            <SessionBrowser
              connection={this.gameSessionConnection}
              setSession={this.setSession}
            />
          </Container>
        )}
        {inGame && (
          <Container className="game-view" fluid>
            <label>Session Id: {this.props.sessionId}</label>
            <Row className="h-60">
              <Col lg={2} />
              <Col lg={8}>
                <GameMap />
              </Col>
              <Col lg={2} />
            </Row>
            <Row className="h-25">
              <Col lg={2} />
              <Col lg={8}>
                <ChatWindow />
              </Col>
              <Col lg={2} />
            </Row>
          </Container>
        )}
      </>
    );

    /*
        <div className="d-lg-block d-none">{this.renderLarge()}</div>
        <div className="d-lg-none d-md-block">{this.renderMedium()}</div>
    */
  }

  renderLarge() {
    return (
      <Container className="h-100">
        <Row className="h-75">
          <Col lg={2} />
          <Col lg={8}>
            <GameMap />
          </Col>
          <Col lg={2} />
        </Row>
        <Row className="h-25">
          <Col lg={2} />
          <Col lg={8}>
            <ChatWindow />
          </Col>
          <Col lg={2} />
        </Row>
      </Container>
    );
  }

  renderMedium() {
    return (
      <Container>
        <Row className="h-75">
          <Col md={1} />
          <Col md={10}>
            <GameMap />
          </Col>
          <Col md={1} />
        </Row>
        <Row className="h-25">
          <Col md={1} />
          <Col md={10}>
            <ChatWindow />
          </Col>
          <Col md={1} />
        </Row>
      </Container>
    );
  }
}

export const Game = (props) => {
  return connect(<GameC />, propKeys, props);
};
