import * as React from "react";

export default class Contact extends React.Component {
  render() {
    return (
      <>
        <h1>Contact Info</h1>
        <div>
          <p>Contact info will be provided soon.</p>
          <p>
            In the mean time join us on our{" "}
            <a href="https://discord.gg/s5f6gdC">discord server</a>!
          </p>
        </div>
      </>
    );
  }
}
