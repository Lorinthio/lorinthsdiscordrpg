import * as React from "react";
import { Posts } from "../devDiary/Posts.jsx";

export default class Home extends React.Component {
  render() {
    return (
      <div className="Home">
        <Posts />
      </div>
    );
  }
}
