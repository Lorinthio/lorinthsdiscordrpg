import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { App } from "./app/App";
import { UserSession } from "./components/auth/UserSession";
import { unregister } from "./registerServiceWorker";

const rootElement = document.getElementById("root");

ReactDOM.render(
  <BrowserRouter>
    <UserSession />
    <Switch>
      <Route path="/" component={App} />
    </Switch>
  </BrowserRouter>,
  rootElement
);

unregister();
