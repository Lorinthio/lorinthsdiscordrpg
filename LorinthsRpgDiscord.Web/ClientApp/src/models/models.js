export const AuthConstants = {
  cookies: {
    session: "DiscordRpg_SessionId",
    gameSession: "DiscordRpg_GameSessionId",
  },
};

export class AuthToken {
  value;
  expiration;
}
