import * as React from "react";
import { MdClose } from "react-icons/md";

export default class SearchInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      focused: false,
      options: [],
    };

    this.debounceTimeout = null;

    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onClear = this.onClear.bind(this);
    this.onSearchResolve = this.onSearchResolve.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ownerKey !== this.props.ownerKey) {
      this.setState({
        value: "",
        options: [],
      });
    }
  }

  render() {
    const options = this.state.options.map((item) => {
      return (
        <li
          key={item.id}
          className="item"
          onClick={() => {
            this.onSelect(item);
          }}
        >
          {item.name}
        </li>
      );
    });
    return (
      <div onBlur={this.onBlur}>
        <input
          className="search-input"
          value={this.state.value}
          onChange={this.onChange}
          onFocus={this.onFocus}
          placeholder={this.props.placeholder}
        />
        {this.props.onClear && (
          <MdClose
            className="search-clear-icon"
            color="beige"
            onClick={this.onClear}
          />
        )}
        {this.props.options && this.state.focused && (
          <div className="search-options-div">
            <ul className="list">{options}</ul>
          </div>
        )}
      </div>
    );
  }

  onBlur() {
    //delay so client can pickup clicks on the select options
    setTimeout(() => {
      this.setState({ focused: false });
    }, 200);
  }

  onFocus() {
    this.setState({ focused: true });
  }

  onChange(event) {
    let value = event.currentTarget.value;

    window.clearTimeout(this.debounceTimeout);
    this.debounceTimeout = window.setTimeout(async () => {
      let options = await this.props.onSearch(value);
      this.onSearchResolve(options);
    }, this.props.debounce ?? 500);

    this.setState({
      value: value,
    });
  }

  onSelect(item) {
    this.setState({
      value: item.name,
    });

    this.props.onSelect(item);
  }

  onClear() {
    this.setState({
      value: "",
    });

    this.props.onClear();
  }

  onSearchResolve(options) {
    console.log("onSearchResolve");
    console.log(options);
    this.setState({
      options,
    });
  }
}
