import * as React from "react";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";

export default class Pagination extends React.Component {
  constructor(props) {
    super(props);

    this.state = { currentPage: 1 };

    this.handleLeftClick = this.handleLeftClick.bind(this);
    this.handleRightClick = this.handleRightClick.bind(this);
  }

  render() {
    return (
      <div className="pagination">
        {this.state.currentPage > 1 && (
          <span onClick={this.handleLeftClick}>
            <MdChevronLeft />
          </span>
        )}
        {this.state.currentPage == 1 && (
          <span
            style={{ width: 30, height: 19, display: "inline-block" }}
          ></span>
        )}
        <span>{this.state.currentPage}</span>
        <span onClick={this.handleRightClick}>
          <MdChevronRight />
        </span>
      </div>
    );
  }

  handleLeftClick() {
    let currentPage = this.state.currentPage - 1;
    this.setState({
      currentPage,
    });

    this.props.onChange(currentPage);
  }

  handleRightClick() {
    let currentPage = this.state.currentPage + 1;
    this.setState({
      currentPage,
    });

    this.props.onChange(currentPage);
  }
}
