import * as React from "react";

export class IconButton extends React.Component {
  render() {
    return (
      <button className="icon-button" onClick={this.props.onClick}>
        {this.props.Component}

        {this.props.text}
      </button>
    );
  }
}
