import * as React from "react";
import Store from "../state/store";
import { FaRegPlusSquare, FaRegMinusSquare } from "react-icons/fa";

export default class CollapseableSection extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  render() {
    const isOpen = Store.get(this.props.viewKey, true);
    const icon = isOpen ? (
      <FaRegMinusSquare onClick={this.onClick} />
    ) : (
      <FaRegPlusSquare onClick={this.onClick} />
    );
    return (
      <>
        <h2 className="collapseable-title">
          {icon}
          {this.props.title}
        </h2>
        <div className={this.props.className} hidden={!isOpen}>
          {this.props.children}
        </div>
      </>
    );
  }

  onClick() {
    let value = Store.get(this.props.viewKey, true);
    console.log(this.props.viewKey + " Before: " + value);
    value = !value;
    console.log(this.props.viewKey + " After: " + value);
    Store.set(this.props.viewKey, value);
  }
}
