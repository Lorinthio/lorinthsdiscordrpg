import * as React from "react";
import Loader from "react-loader-spinner";
import Store from "../state/store";
import StoreKeys from "../state/storeKeys";

export function LoadingComponent(props) {
  return (
    <>
      {props.loading && (
        <div className="loader-div">
          <div className="center">
            <Loader
              type="BallTriangle"
              color="#00BFFF"
              height={80}
              width={80}
            />
            <h2>{props.text}</h2>
          </div>
        </div>
      )}
    </>
  );
}

export function SetLoading(isLoading, text = "") {
  Store.set(StoreKeys.APP.LOADING, isLoading);
  Store.set(StoreKeys.APP.LOADING_TEXT, text);
}
