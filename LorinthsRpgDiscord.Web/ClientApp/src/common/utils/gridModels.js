export const entityGridColumns = {
  item: [
    { key: "id", name: "id", editable: true, primaryKey: true },
    { key: "name", name: "name", editable: true },
    { key: "description", name: "description", editable: true },
    { key: "value", name: "value", editable: true },
    { key: "weight", name: "weight", editable: true },
  ],
  weapon: [
    { key: "itemId", name: "ItemId", editable: true, primaryKey: true },
    { key: "power", name: "Power", editable: true },
    { key: "parry", name: "Parry", editable: true },
    { key: "reach", name: "Reach", editable: true },
    { key: "actionCost", name: "ActionCost", editable: true },
  ],
};
