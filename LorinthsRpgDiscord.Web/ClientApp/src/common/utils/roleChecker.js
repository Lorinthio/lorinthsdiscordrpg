import Store from "../state/store";
import StoreKeys from "../state/storeKeys";

const userRoles = {};

export const userHasRole = (role) => {
  if (role in userRoles) {
    return userRoles[role];
  }

  let user = Store.get(StoreKeys.USER.CURRENT);
  if (!user) {
    return false;
  }

  let roles = user.roles;
  if (!roles) {
    return false;
  }
  for (let r of roles) {
    if (r === role) {
      userRoles[role] = true;
      return true;
    }
  }

  userRoles[role] = false;
  return false;
};

export const ROLES = {
  WorldBuilder: "World Builder",
  NewsPoster: "News Poster",
};
