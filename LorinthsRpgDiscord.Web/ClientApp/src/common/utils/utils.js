const generateSessionId = () => {
  return Math.floor(Math.random() * Math.floor(10000000000000000));
};

export const Utils = {
  generateSessionId: generateSessionId(),
};
