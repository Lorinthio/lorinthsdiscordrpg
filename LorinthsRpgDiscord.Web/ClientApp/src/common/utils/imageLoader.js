export const loadImage = (url) => {
  var image = new window.Image();
  image.src = url;

  let promise = new Promise((resolve, reject) => {
    image.addEventListener("load", () => {
      resolve(image);
    });
  });

  return promise;
};
