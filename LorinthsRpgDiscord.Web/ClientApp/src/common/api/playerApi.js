import { ToastsStore } from "react-toasts";
var axios = require("axios");

const playerRoute = "/api/player";

export const getPlayer = async (playerId) => {
  try {
    return await axios.get(`${playerRoute}/get/${playerId}`);
  } catch (error) {
    ToastsStore.error("Error getting player: " + error, 3000, "custom-toast");
  }
};
