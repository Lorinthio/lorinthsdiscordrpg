import { ToastsStore } from "react-toasts";
var axios = require("axios");

const securityRoute = "/api/security";

export const getCurrentUser = async () => {
  try {
    return await axios
      .get(`${securityRoute}/getCurrentUser`)
      .then((response) => {
        return response.data;
      });
  } catch (error) {
    ToastsStore.error("Error getting player: " + error, 3000, "custom-toast");
  }
};
