import { ToastsStore } from "react-toasts";
var axios = require("axios");

const entityRoute = "/api/entity";

export const searchEntity = async (entity, page = 1, pageSize = 20) => {
  try {
    return await axios.get(
      `${entityRoute}/search?entity=${entity}&page=${page}&pageSize=${pageSize}`
    );
  } catch (error) {
    ToastsStore.error("Error searching entity: " + error, 3000, "custom-toast");
  }
};
