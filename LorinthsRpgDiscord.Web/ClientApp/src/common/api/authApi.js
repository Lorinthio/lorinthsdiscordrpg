import { ToastsStore } from "react-toasts";
var axios = require("axios");

const oauthRoute = "/api/oauth/discord";

export const getDiscordLoginUrl = async () => {
  try {
    return await axios.get(`${oauthRoute}/loginUrl`).then((response) => {
      return response.data;
    });
  } catch (error) {
    ToastsStore.error(
      "Error getting discord login url: " + error,
      3000,
      "custom-toast"
    );
  }
};

export const handleDiscordRedirect = async (code) => {
  try {
    return await axios
      .post(`${oauthRoute}/handleRedirect`, { Code: code })
      .then((response) => {
        return response.data;
      });
  } catch (error) {
    ToastsStore.error(
      "Error handling discord redirect: " + error,
      3000,
      "custom-toast"
    );
  }
};

export const handleLogout = async () => {
  try {
    return await axios.get(`${oauthRoute}/logout`).then((response) => {
      return response.data;
    });
  } catch (error) {
    ToastsStore.error("Error handling logout: " + error, 3000, "custom-toast");
  }
};
