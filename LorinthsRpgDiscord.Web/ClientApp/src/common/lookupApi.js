var axios = require("axios");

const lookupRoute = "/api/lookup";

export const getClasses = async () => {
  try {
    return await axios.get(`${lookupRoute}/classes`);
  } catch (error) {
    console.log(error);
  }
};

export const getRaces = async () => {
  try {
    return await axios.get(`${lookupRoute}/races`);
  } catch (error) {
    console.log(error);
  }
};

export const getObjectiveTypes = async () => {
  try {
    return await axios.get(`${lookupRoute}/objectiveTypes`);
  } catch (error) {
    console.log(error);
  }
};

export const getLocationTypes = async () => {
  try {
    return await axios.get(`${lookupRoute}/locationTypes`);
  } catch (error) {
    console.log(error);
  }
};
