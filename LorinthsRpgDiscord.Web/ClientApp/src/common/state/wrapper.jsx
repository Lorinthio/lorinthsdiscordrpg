import * as React from "react";
import Store from "./store";

export default class StateWrapper extends React.Component {
  constructor(props) {
    super(props);

    this.onStateChange = this.onStateChange.bind(this);
    Store.subscribe(this.onStateChange, this.props.keys);
  }

  render() {
    console.log("StateWrapper.render");
    return React.Children.map(this.props.children, (child) =>
      React.cloneElement(child)
    );
  }

  onStateChange = () => {
    console.log("StateWrapper.onStateChange");
    this.setState({});
  };
}
