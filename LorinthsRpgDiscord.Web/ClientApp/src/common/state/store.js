export class ApplicationStore {
  constructor() {
    this.data = {};
    this.resolvers = {};
    this.promises = {};
    this.onChangeSubscriptions = {};
  }

  get = (key, defaultValue) => {
    if (this.data[key] == null) {
      this.data[key] = defaultValue;
      return defaultValue;
    }
    return this.data[key];
  };

  wait = (key) => {
    let resolveCallback = null;
    const promise = new Promise((resolve) => (resolveCallback = resolve));
    if (!this.data[key]) {
      this.resolvers[key] = resolveCallback;
      this.promises[key] = promise;
      return promise;
    } else {
      resolveCallback(this.data[key]);
      return promise;
    }
  };

  set = (key, value) => {
    if (isPrimitive(value)) {
      this.data[key] = value;
    } else {
      if (Array.isArray(value)) {
        this.data[key] = [...value];
      } else {
        this.data[key] = { ...value };
      }
    }
    this.resolve(key, value);
    this.notify(key);
  };

  subscribe = (callback, keys) => {
    for (var k of keys) {
      if (!this.onChangeSubscriptions[k]) {
        this.onChangeSubscriptions[k] = [];
      }

      this.onChangeSubscriptions[k].push(callback);
    }
  };

  resolve = (key, data) => {
    if (this.resolvers[key]) {
      console.log(this.resolvers[key]);
      this.resolvers[key](data);
    }
  };

  notify = (key) => {
    if (!this.onChangeSubscriptions[key]) {
      return;
    }
    for (let callback of this.onChangeSubscriptions[key]) {
      callback();
    }
  };

  print = () => {
    console.log(this.data);
  };
}

function isPrimitive(test) {
  return test !== Object(test);
}

const Store = new ApplicationStore();

export default Store;
