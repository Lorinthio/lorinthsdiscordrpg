export const ObjectiveTypes = {
  Kill: 1,
  Gather: 2,
  Talk: 3,
  Explore: 4,
  Craft: 5,
};
