import * as React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { ToastsContainer, ToastsStore } from "react-toasts";

import "./App.css";
import { LoadingComponent } from "../common/components/Loading.jsx";
import StoreKeys from "../common/state/storeKeys.js";
import { SiteNavbar } from "../components/navigation/navbar/SiteNavbar";
import Home from "../components/screens/Home";
import WorldBuilder from "../components/screens/WorldBuilder";
import { connect } from "../common/state/connect";
import { MapBuilder } from "../components/mapBuilder/MapBuilder";
import { Game } from "../components/screens/Game";
import Login from "../components/auth/Login";
import Verify from "../components/auth/Verify";
import Logout from "../components/auth/Logout";
import Contact from "../components/screens/Contact";

const propKeys = {
  loading: StoreKeys.APP.LOADING,
  loadingText: StoreKeys.APP.LOADING_TEXT,
  user: StoreKeys.USER.CURRENT,
};

class AppC extends React.Component {
  render() {
    const authRoutes = this.renderAuth();
    return (
      <>
        <LoadingComponent
          loading={this.props.loading}
          text={this.props.loadingText}
        />

        <SiteNavbar />
        <div style={{ margin: 8 }}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/game" component={Game} />
            <Route path="/mapbuilder" component={MapBuilder} />
            <Route path="/worldbuilder" component={WorldBuilder} />
            <Route path="/contact" component={Contact} />
            {authRoutes}
            <Redirect to="/" />
          </Switch>
        </div>

        <ToastsContainer store={ToastsStore} />
      </>
    );
  }

  renderAuth() {
    return (
      <>
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout} />
        <Route path="/verify" component={Verify} />
      </>
    );
  }
}

export function App() {
  return connect(<AppC />, propKeys);
}
