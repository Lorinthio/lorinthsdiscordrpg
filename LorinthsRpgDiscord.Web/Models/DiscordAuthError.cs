﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Models
{
    public class DiscordAuthError
    {
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
    }
}
