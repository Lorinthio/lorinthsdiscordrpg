﻿using LorinthsRpgDiscord.Constants;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto.Exceptions;
using LorinthsRpgDiscord.Dto.Security;
using LorinthsRpgDiscord.Service.Interfaces;
using LorinthsRpgDiscord.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Models
{
    public class SecureController : ControllerBase
    {
        private readonly ISecurityService _securityService;
        public SecureController(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        public async Task<User> GetCurrentSessionUser()
        {
            var sessionId = this.GetCurrentUserSessionId();
            if (sessionId == null)
            {
                return null;
            }

            User user = null;
            try
            {
                user = await _securityService.GetCurrentUser(sessionId.Value, HttpContext.GetIpAddress());
                if(user == null && sessionId != null)
                {
                    Response.Cookies.Delete(CookieNames.WebSessionCookie);
                    throw new Exception();
                }
            }
            catch (SessionExpiredException)
            {
                Response.Cookies.Delete(CookieNames.WebSessionCookie);
            }
            return user;
        }

        public async Task<Player> GetCurrentSessionPlayer()
        {
            var sessionId = this.GetCurrentUserSessionId();
            if (sessionId == null)
            {
                return null;
            }

            Player player = null;
            try
            {
                player = await _securityService.GetCurrentPlayer(sessionId.Value, HttpContext.GetIpAddress());
            }
            catch (SessionExpiredException)
            {
                Response.Cookies.Delete(CookieNames.WebSessionCookie);
            }
            return player;
        }
    }
}
