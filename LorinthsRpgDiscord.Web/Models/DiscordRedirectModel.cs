﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Models
{
    public class DiscordRedirectModel
    {
        public string Code { get; set; }
    }
}
