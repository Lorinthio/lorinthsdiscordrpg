﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Models
{
    public class PostViewModel
    {
        public string CreatorId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
