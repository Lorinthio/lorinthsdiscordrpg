﻿using LorinthsRpgDiscord.Dto.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Web.Models.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class DiscordSecurity : Attribute
    {
        private string[] roles { get; set; }
        /// <summary>
        /// Give specific roles to this API Call
        /// </summary>
        /// <param name="roles"></param>
        public DiscordSecurity(params string[] roles)
        {
            this.roles = roles;
        }

        public bool HasAccess(User user)
        {
            return user.Roles.Any(r => roles.Contains(r));
        }

    }
}
