﻿using Discord;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Discord.Interfaces
{
    public interface ICharacterDiscordService
    {
        Task<Embed> GetCharacterSheet(Guid characterId);
    }
}
