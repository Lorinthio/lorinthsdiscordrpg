﻿using LorinthsRpgDiscord.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Discord.Interfaces
{
    public interface IDiscordUserService
    {
        DiscordUserDto GetUser(string access_token);
    }
}
