﻿using Discord;
using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Service.Interfaces;
using System;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Discord
{
    public class CharacterDiscordService : ICharacterDiscordService
    {
        private readonly ICharacterService _characterService;

        public CharacterDiscordService(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        public async Task<Embed> GetCharacterSheet(Guid characterId)
        {
            var embedBuilder = new EmbedBuilder
            {
                Color = new Color(114, 137, 218),
                Description = "Character Information"
            };

            var character = await _characterService.GetCharacter(characterId);
            embedBuilder.AddField("Name", character.CharacterName);
            embedBuilder.AddField("Race", character.Race.Name);
            embedBuilder.AddField("Starting Class", character.Class.Name, true);
            embedBuilder.AddField("Craft Profession", character.CraftProfession.Name, true);
            embedBuilder.AddField("Gather Profession", character.GatherProfession.Name, true);

            embedBuilder.AddField("STR", character.CoreAttributes.Strength, true);
            embedBuilder.AddField("END", character.CoreAttributes.Endurance, true);
            embedBuilder.AddField("DEX", character.CoreAttributes.Dexterity, true);
            embedBuilder.AddField("INT", character.CoreAttributes.Intelligence, true);
            embedBuilder.AddField("CHA", character.CoreAttributes.Charisma, true);
            embedBuilder.AddField("-", "-", true);
            embedBuilder.AddField("Bio", character.Bio);

            return embedBuilder.Build();
        }

    }
}
