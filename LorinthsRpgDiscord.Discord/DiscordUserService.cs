﻿using LorinthsRpgDiscord.Discord.Interfaces;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Discord;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;

namespace LorinthsRpgDiscord.Discord
{
    public class DiscordUserService : IDiscordUserService
    {
        private readonly IConfiguration _config;
        public DiscordUserService(IConfiguration config)
        {
            _config = config;
        }

        public DiscordUserDto GetUser(string code)
        {
            string access_token = GetAccessToken(code);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://discordapp.com/api/users/@me");
            request.Method = "Get";
            request.Headers.Add("Authorization", "Bearer " + access_token);
            request.ContentType = "application/x-www-form-urlencoded";

            string apiResponse = "";
            using (HttpWebResponse response1 = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader1 = new StreamReader(response1.GetResponseStream());
                apiResponse = reader1.ReadToEnd();
            }

            var user = JsonConvert.DeserializeObject<DiscordUserDto>(apiResponse);
            user.Token = code;
            user.Roles = GetRoles(user.Id).ToList();
            
            return user;
        }

        private string GetAccessToken(string code)
        {
            string client_id = _config["ClientId"];
            string client_sceret = _config["ClientSecret"];
            string redirect_url = _config["DiscordRedirectUrl"];

            /*Get Access Token from authorization code by making http post request*/

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://discordapp.com/api/oauth2/token");
            webRequest.Method = "POST";
            string parameters = "client_id=" + client_id + "&client_secret=" + client_sceret + "&grant_type=authorization_code&code=" + code + "&redirect_uri=" + redirect_url + "";
            byte[] byteArray = Encoding.UTF8.GetBytes(parameters);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = byteArray.Length;
            Stream postStream = webRequest.GetRequestStream();

            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            WebResponse response = webRequest.GetResponse();
            postStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(postStream);
            string responseFromServer = reader.ReadToEnd();

            string tokenInfo = responseFromServer.Split(',')[0].Split(':')[1];
            return tokenInfo.Trim().Substring(1, tokenInfo.Length - 3);
        }

        private IEnumerable<DiscordGuildRoleDto> GetRoles(string userId)
        {
            var guildId = _config["DiscordGuildId"];
            var guildUser = GetData<DiscordGuildUserDto>($"https://discordapp.com/api/guilds/" +
                $"{guildId}/" +
                $"members/" +
                $"{userId}");

            var guildRoles = GetData<List<DiscordGuildRoleDto>>($"https://discordapp.com/api/guilds/" +
                $"{guildId}/" +
                $"roles");
            
            return guildRoles.Where(role => guildUser.Roles.Contains(role.Id));
        }

        private T GetData<T>(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "Get";
            request.Headers.Add("Authorization", "Bot NzI1NzUxNzE3OTA3NTI5NzI4.XxZOUA.bkiF8jDhNyiyYxvX1x6MkMWFJSM");
            request.ContentType = "application/json";

            string apiResponse = "";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<T>(apiResponse);
        }
    }
}
