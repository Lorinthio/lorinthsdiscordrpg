﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.External.AirTable.Interfaces
{
    public interface IAirTableService<T>
    {
        Task<IEnumerable<T>> GetRecords(string tableName);
    }
}
