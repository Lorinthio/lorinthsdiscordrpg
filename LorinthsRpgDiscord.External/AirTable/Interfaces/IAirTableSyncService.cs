﻿using LorinthsRpgDiscord.External.AirTable.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.External.AirTable.Interfaces
{
    public interface IAirTableSyncService
    {
        Task<IEnumerable<AirTableSyncResult>> SyncTables(AirTableSyncRequest syncRequest);
    }
}
