﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.External.AirTable.Models
{
    public class AirTableSyncResult
    {
        public string TableName { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string InnerErrorMessage { get; set; }
    }
}
