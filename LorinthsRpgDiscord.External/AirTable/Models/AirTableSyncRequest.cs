﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.External.AirTable.Models
{
    public class AirTableSyncRequest
    {
        public bool All { get; set; }
        public bool Item { get; set; }
        public bool Armor { get; set; }
        public bool Weapon { get; set; }
    }
}
