﻿using AirtableApiClient;
using LorinthsRpgDiscord.External.AirTable.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.External.AirTable
{
    public class AirTableService<T> : IAirTableService<T>
    {
        private readonly IConfiguration _config;
        public AirTableService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<IEnumerable<T>> GetRecords(string tableName)
        {
            string apiKey = _config["AirTable:ApiKey"];
            string baseId = _config["AirTable:Base"];

            string offset = null;
            string errorMessage = null;
            var records = new List<AirtableRecord>();

            using (AirtableBase airtableBase = new AirtableBase(apiKey, baseId))
            {
                //
                // Use 'offset' and 'pageSize' to specify the records that you want
                // to retrieve.
                // Only use a 'do while' loop if you want to get multiple pages
                // of records.
                //

                do
                {
                    var response = await airtableBase.ListRecords(
                           tableName,
                           offset: offset,
                           pageSize: 100);

                    if (response.Success)
                    {
                        records.AddRange(response.Records.ToList());
                        offset = response.Offset;
                    }
                    else if (response.AirtableApiError is AirtableApiException)
                    {
                        errorMessage = response.AirtableApiError.ErrorMessage;
                        break;
                    }
                    else
                    {
                        errorMessage = "Unknown error";
                        break;
                    }
                } while (offset != null);
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                // Error reporting
                throw new Exception(errorMessage);
            }
            else
            {
                List<T> itemList = new List<T>();
                // Do something with the retrieved 'records' and the 'offset'
                // for the next page of the record list.
                foreach(var record in records)
                {
                    Type type = typeof(T);
                    T item = (T) Activator.CreateInstance(typeof(T));
                    if(record.Fields.Count <= 1)
                    {
                        continue;
                    }
                    foreach(var key in record.Fields.Keys)
                    {
                        var property = type.GetProperty(key);
                        if (property != null)
                        {
                            property.SetValue(item, ConvertValue(property, record.Fields[key]));
                        }
                    }

                    itemList.Add(item);
                }

                return itemList;
            }
        }
        private object ConvertValue(System.Reflection.PropertyInfo prop, object value)
        {
            try
            {
                if (prop.PropertyType.Name == "Int32")
                {
                    return int.Parse(RemoveGarbage(value.ToString()));
                }
                else if (prop.PropertyType.Name == "Decimal")
                {
                    return decimal.Parse(value.ToString());
                }
                else if (prop.PropertyType.Name == "String")
                {
                    return value ?? "";
                }
                return value;
            } 
            catch(Exception ex)
            {
                throw new Exception($"Invalid format of {prop.Name} where value is {value}", ex);
            }
        }

        private string RemoveGarbage(string value)
        {
            return value.Replace('{', ' ').Replace('[', ' ').Replace(']', ' ').Replace('}', ' ').Trim();
        }
    }
}
