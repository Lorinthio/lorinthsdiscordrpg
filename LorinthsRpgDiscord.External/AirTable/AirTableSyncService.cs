﻿using LorinthsRpgDiscord.Data.Models;
using LorinthsRpgDiscord.Dto;
using LorinthsRpgDiscord.Dto.Attributes;
using LorinthsRpgDiscord.External.AirTable.Interfaces;
using LorinthsRpgDiscord.External.AirTable.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.External.AirTable
{
    public class AirTableSyncService : IAirTableSyncService
    {
        private readonly IConfiguration _config;
        private readonly IAirTableService<ItemDto> _itemAirTable;
        private readonly IAirTableService<ArmorDto> _armorAirTable;
        private readonly IAirTableService<WeaponDto> _weaponAirTable;

        public AirTableSyncService(
            IConfiguration config,
            IAirTableService<ItemDto> itemAirTable,
            IAirTableService<ArmorDto> armorAirTable,
            IAirTableService<WeaponDto> weaponAirTable)
        {
            _config = config;
            _itemAirTable = itemAirTable;
            _armorAirTable = armorAirTable;
            _weaponAirTable = weaponAirTable;
        }

        public async Task<IEnumerable<AirTableSyncResult>> SyncTables(AirTableSyncRequest syncRequest)
        {
            var results = new List<AirTableSyncResult>();
            if(syncRequest.All || syncRequest.Item)
            {
                var result = await SyncTable<ItemDto, Item>(
                    _itemAirTable, 
                    (dto, item) =>  dto.Id == item.Id, 
                    (dto) => dto.Update);
                results.Add(result);
            }

            if(syncRequest.All || syncRequest.Armor)
            {
                var result = await SyncTable<ArmorDto, Armor>(
                    _armorAirTable,
                    (dto, armor) => dto.ItemId == armor.ItemId,
                    (dto) => dto.Update);
                results.Add(result);
            }

            if (syncRequest.All || syncRequest.Weapon)
            {
                var result = await SyncTable<WeaponDto, Weapon>(
                    _weaponAirTable,
                    (dto, weapon) => dto.ItemId == weapon.ItemId,
                    (dto) => dto.Update);
                results.Add(result);
            }

            return results;
        }

        private async Task<AirTableSyncResult> SyncTable<T, K>(IAirTableService<T> airTableService, Func<T, K, bool> keyComparison, Func<T, Action<K>> updateMethod) where K : class
        {
            var airTableAttribute = GetAirTableAttribute(typeof(T));
            var result = new AirTableSyncResult
            {
                TableName = airTableAttribute.AirTableName,
                Success = false,
                ErrorMessage = null,
                InnerErrorMessage = null
            };

            try
            {
                var dbSetName = typeof(K).Name;
                var dbSetProperty = typeof(DiscordRpgThreadedContext).GetProperty(dbSetName);

                if (dbSetProperty == null)
                {
                    throw new Exception($"DbSet {dbSetName} does not exist on the DB Context!");
                }

                using (var context = new DiscordRpgThreadedContext(_config))
                {
                    using(var transaction = context.Database.BeginTransaction())
                    {
                        var airTableRecords = await airTableService.GetRecords(airTableAttribute.AirTableName);
                        var dbSet = dbSetProperty.GetValue(context) as DbSet<K>;

                        foreach (var record in airTableRecords)
                        {
                            CreateOrUpdate(record, dbSet, keyComparison, updateMethod);
                        }
                        try
                        {
                            await SaveChanges(context, airTableAttribute);
                            result.Success = true;
                            transaction.Commit();
                        }
                        catch(Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex) {
                result.ErrorMessage = ex.Message;
                result.InnerErrorMessage = ex.InnerException?.Message;
            }

            return result;
        }

        private async Task SaveChanges(DiscordRpgThreadedContext context, AirTableNameAttribute airTableAttribute)
        {
            if (airTableAttribute.IdentityInsert)
            {
                await context.Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT dbo.{airTableAttribute.DbTableName} ON");
            }

            await context.SaveChangesAsync();

            if (airTableAttribute.IdentityInsert)
            {
                await context.Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT dbo.{airTableAttribute.DbTableName} OFF");
            }
        }

        private AirTableNameAttribute GetAirTableAttribute(Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(AirTableNameAttribute), true);
            if(attributes.Length == 0)
            {
                throw new Exception($"Type {type.Name} does not have AirTableNameAttribute applied!");
            }

            return attributes.FirstOrDefault() as AirTableNameAttribute;
        }

        private void CreateOrUpdate<T, K>(T airTableRecord, DbSet<K> dbSet, Func<T, K, bool> keyComparison, Func<T, Action<K>> updateMethod) where K : class
        {
            var dbRecord = dbSet.ToList().FirstOrDefault(r => keyComparison(airTableRecord, r));
            if (dbRecord != null)
            {
                updateMethod(airTableRecord)(dbRecord);
                dbSet.Update(dbRecord);
            }
            else
            {
                K item = (K)Activator.CreateInstance(typeof(K));
                updateMethod(airTableRecord)(item);
                dbSet.Add(item);
            }
        }
    }
}
