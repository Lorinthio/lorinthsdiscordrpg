﻿-- 8 Tin
-- 9 Copper
-- 10 Iron

INSERT INTO dbo.ResourceNode ([SubLocationId], [GatherProfessionId], [ToolLevelRequired], [LootTableId], [LootTableCritId]) VALUES
-- ORES
(8, 4, 1, 3, 4),  -- Tin
(9, 4, 1, 1, 2),  -- Copper
(10, 4, 5, 5, 6), --

-- WOOD
(5, 5, 1, 7, 8),	--Birch
(6, 5, 1, 9, 10),	--Pine
(7, 5, 5, 11, 12),	--Oak
(11, 5, 10, 13, 14), -- Maple

-- HERBS
(14, 2, 1, 15, 16), -- North Pelemere Woods
(13, 2, 5, 17, 18), -- South Pelemere Woods
(15, 2, 1, 19, 20),  -- Whitegrass (Sinsevain Grasslands)

-- HIDES
(18, 3, 1, 21, 22),	-- Cows (Sinsevain Grasslands)
(19, 3, 5, 23, 24),	-- North Pelemere Woods - Deer
(20, 3, 5, 25, 26)	-- North Pelemere Woods - Elk

-- Fish
