﻿-- ==========================
-- Weapons
-- ==========================

SET IDENTITY_INSERT dbo.Item ON
INSERT INTO dbo.Item ([Id], [Name], [Description], [Value], [Weight]) VALUES
(1501, 'Bronze Dagger', '', 1, 0.5),
(1502, 'Bronze Shortsword', '', 1, 1.5),
(1503, 'Bronze Longsword', '', 1, 3),
(1504, 'Bronze Broadsword', '', 1, 5),
(1505, 'Bronze Greatsword', '', 1, 8),
(1506, 'Bronze Claymore', '', 1, 6),
(1507, 'Bronze Warhammer', '', 1, 4),
(1508, 'Bronze Morningstar', '', 1, 3),
(1509, 'Bronze Greathammer', '', 1, 8),
(1510, 'Bronze Quarterstaff', '', 1, 1),
(1511, 'Bronze Handaxe', '', 1, 1.5),
(1512, 'Bronze Battleaxe', '', 1, 4),
(1513, 'Bronze Greataxe', '', 1, 8),
(1514, 'Bronze Spear', '', 1, 3),
(1515, 'Bronze Trident', '', 1, 2),
(1516, 'Bronze Halberd', '', 1, 7),
(1517, 'Birch Shortbow', '', 1, 2),
(1518, 'Birch Longbow', '', 1, 4),
(1519, 'Bronze Crossbow', '', 1, 6),
(1520, 'Bronze Dart', '', 1, 0.5),
(1521, 'Bronze Buckler', '', 1, 2),
(1522, 'Bronze Kite Shield', '', 1, 3),
(1523, 'Bronze Tower Shield', '', 1, 5),
(1524, 'Birch Fire Staff', '', 1, 4),
(1525, 'Birch Air Staff', '', 1, 4),
(1526, 'Birch Earth Staff', '', 1, 4),
(1527, 'Birch Water Staff', '', 1, 4),
(1528, 'Birch Water Staff', '', 1, 4),
(1529, 'Birch Water Staff', '', 1, 4)
SET IDENTITY_INSERT dbo.Item OFF

-- ==========================
-- Armor
-- ==========================



-- ==========================
-- Starting Equipment
-- ==========================