﻿SET IDENTITY_INSERT dbo.BuildingType ON
INSERT INTO [dbo].[BuildingType] ([Id], [Name], [IsActive]) VALUES
(1, 'Tavern', 1),
(2, 'Shop', 1)
SET IDENTITY_INSERT dbo.BuildingType OFF

SET IDENTITY_INSERT dbo.LocationType ON
INSERT INTO dbo.LocationType ([Id], [Name], [IsActive]) VALUES
(1, 'POI', 1),
(2, 'City', 1),
(3, 'Village', 1),
(4, 'Dungeon', 1),
(5, 'Field', 1),
(6, 'Forest', 1),
(7, 'Ocean', 1),
(8, 'Underground', 1),
(9, 'Outpost', 1),
(10, 'Mine', 1),
(11, 'Graveyard', 1)
SET IDENTITY_INSERT dbo.LocationType OFF

SET IDENTITY_INSERT dbo.QuestStepObjectiveType ON
INSERT INTO dbo.QuestStepObjectiveType ([Id], [Name]) VALUES
(1, 'Kill'),
(2, 'Gather'),
(3, 'Talk'),
(4, 'Explore'),
(5, 'Craft')
SET IDENTITY_INSERT dbo.QuestStepObjectiveType OFF