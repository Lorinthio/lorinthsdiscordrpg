﻿SET IDENTITY_INSERT dbo.Continent ON
INSERT INTO dbo.Continent ([Id], [Name], [Description]) VALUES
(1, 'Argost', 'A newly discovered continent which is primarily unmapped and begging to be explored. 
Many travellers head to this continent with the hope of treasure and riches.'),
(2, 'Estelon', 'The mainland where strife and conflict erupt everywhere. 
Military force is present everywhere as nations battle over the remaining land which is settled by smaller tribes.')
SET IDENTITY_INSERT dbo.Continent OFF

SET IDENTITY_INSERT dbo.Region ON
insert into dbo.Region ([Id], [Name], [Description], [ContinentId]) VALUES
(1, 'Odrain', 'Named after the dragonkin explorer, Sratren Odrain, this region is largely undiscovered.
Most people speak of untouched riches and archaeic ruins to be explored and claimed', 1),
(2, 'Keprain', 'Keprain is the most dominant nation in Estelon.
Home to corrupt nobles and poor common folk, shadey business is not uncommon here', 2)
SET IDENTITY_INSERT dbo.Region OFF

SET IDENTITY_INSERT dbo.[Location] ON
INSERT INTO dbo.[Location] ([Id], [Name], [Description], [LocationTypeId], [RegionId]) VALUES
(1, 'Ember Camp', 'Ember camp was settled within **Argost** as the first primary outpost and launch pad for explorers to base their expeditions.',
9, 1),
(2, 'Silentfall', '**Silentfall** was named after the nearby massive waterfall which was enchanted with a beautiful but eerie spell of silence.
The town itself is large in size to accomidate the amount of tourism that comes to see the enchanted falls, and with the large number of folks is a hub to information and many a task.',
2, 2),
(3, 'Hollow Woodlands', 'A calm and gentle forest with a subtle humming noise coming from the trees as wind catches their hollow interiors. Many small critters can be found here', 6, 1),
(4, 'Foxtail Pastures', 'There were fields stretching over great distances, their mosaic marked further by the picket fences around them. All around you sheep and cows frolicked and foraged in the tranquil pastures, and passing field after field ran an overgrown, cobblestone road.
The road made its way to a tremendous estate eclipsed by the stunning pagoda in the back of the courtyard. The estate was showing signs of wear and tear, and could generally do with a little upkeep. A large barn housed livestock durinng harsher weather, piles and piles of logs were stacked against the walls of the farm, and a small plot of land was used for a private vegetable garden. The farm had a cozy feel to it, there was just something about the farm that felt very intimate and welcoming.',
1, 2),
(5, 'South Pelemere Woods', 'The woods that dominate the land surrounding Silentfall to the south.',
6, 2),
(6, 'North Pelemere Woods', 'The woods that dominate the land surrounding Silentfall to the north.',
6, 2),
(7, 'Elder Mine', 'Output of Copper ore, Tin ore, Iron Ore. The oldest and still running mine in the Kingdom.', 
10, 2),
(8, 'Elder Village', '',
3, 2),
(9, 'Port Adder Town', 'A flourishing peaceful fishing town, filled with fishers and fishmongers alike. This town is known for its fresh caught sea salmon and cod. Merchants take the fish upriver then it is delivered to the Capital.',
3, 2),
(10, 'The Late Tankard Inn', '',
1, 2),
(11, 'Sinsevain Grasslands', 'For gathering of alchemy ingredients and grazing of livestock', 5, 2)
SET IDENTITY_INSERT dbo.[Location] OFF

SET IDENTITY_INSERT dbo.[RaceStartingLocation] ON
INSERT INTO dbo.[RaceStartingLocation] ([Id], [RaceId], [LocationId]) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2)
SET IDENTITY_INSERT dbo.[RaceStartingLocation] OFF

SET IDENTITY_INSERT dbo.[SubLocation] ON
INSERT INTO dbo.[SubLocation] ([Id], [Name], [Description], [LocationId], [CanTravel], [IsStartingSubLocation]) VALUES
(1, 'Town Square', 'The town square is bustling with a number of travelers and townsfolk making it to wherever they are going.', 2, 1, 1),
(2, 'Market District', 'This mercantile district is home to a number of small merchants and different shops ranging from combat equipment to basic trade goods.', 2, 0, 0),
(3, 'Noble District', 'The rich nobles of the town live in this clean and marble lain portion of town. Guards are posted around the walled in district ensuring no ragamuffins make it in.', 2, 0, 0),
(4, 'Guild District', 'The guilds of Silentfall are located here. Any craftsmen of worth come here to hone their craft and gain posterity', 2, 1, 1),
(5, 'Birch Logging Camp', '', 5, 1, 1),
(6, 'Pine Logging Camp', '', 5, 1, 0),
(7, 'Oak Logging Camp', '', 5, 1, 0),
(8, 'Tin Deposit', '', 7, 1, 0),
(9, 'Copper Deposit', '', 7, 1, 1),
(10, 'Iron Deposit', '', 7, 1, 0),
(11, 'Maple Logging Camp', '', 6, 1, 0),
(12, 'Wildlife Reserve', '', 6, 1, 1),
(13, 'Herblands', '', 5, 1, 0),
(14, 'Herblands', '', 6, 1, 0),
(15, 'Herblands', '', 11, 1, 0),
(16, 'Open Pastures', '', 11, 1, 1),
(17, 'Lakefront', 'The lower section of Silentfall boasts a nice lake view and dock area allowing for water passage to Port Adder', 2, 0, 0),
(18, 'Cow Farm', '', 11, 0, 0),
(19, 'Deer Woods', '', 6, 1, 0),
(20, 'Elk Woods', '', 6, 1, 0)
SET IDENTITY_INSERT dbo.[SubLocation] OFF


INSERT INTO dbo.[LocationDistance] ([FirstLocationId], [SecondLocationId], [DistanceInMiles]) VALUES
(1, 3, 1),
(2, 4, 6),
(2, 5, 18),
(2, 8, 25),
(8, 7, 5),
(4, 6, 11),
(2, 6, 21),
(10, 9, 12),
(4, 9, 27),
(10, 4, 15),
(10, 6, 11),
(5, 8, 8)