﻿SET IDENTITY_INSERT dbo.Player ON
INSERT INTO dbo.Player ([Id], [UserId], [Username])
values 
--(1, '113898143745179652', 'Lorinthio'), -- Lor
(2, '254231108785274880', 'Saxorum'), -- Blake
(3, '122453514025828355', 'Fillosopher')  -- Fillosopher
SET IDENTITY_INSERT dbo.Player OFF

SET IDENTITY_INSERT dbo.CoreAttributes ON
INSERT INTO dbo.CoreAttributes (Id, Strength, Endurance, Dexterity, Intelligence, Charisma) VALUES
--(6, 11, 10, 10, 10, 11),
(7, 11, 10, 10, 10, 11),
(8, 11, 10, 10, 10, 11)
SET IDENTITY_INSERT dbo.CoreAttributes OFF

INSERT INTO dbo.[Character] ([Id], [PlayerId], [CharacterName], [Bio], [RaceId], [ClassId], [CraftProfessionId], [GatherProfessionId], [Created], [CoreAttributesId], [CreatedDate], [CurrentLocationId], [CurrentSubLocationId]) VALUES
--('6DDB40DF-4080-4EAF-9342-03A3C303C669', 1, 'Lorinth', '!bio command', 1, 1, 5, 4, 1, 6, GETDATE(), 7, 8),
('0CD64815-EA5B-4F9B-91C0-EE08F96FBF19', 2, 'Sax', '!bio command', 1, 1, 1, 2, 1, 7, GETDATE(), 2, 1),
('8462A16F-6F9B-4EBE-8683-2A81978F2D2F', 3, 'Fillosopher', '!bio command', 1, 2, 4, 3, 1, 8, GETDATE(), 2, 1)

--update dbo.Player
--set CurrentCharacterId = '6DDB40DF-4080-4EAF-9342-03A3C303C669'
--where Id = 1
update dbo.Player
set CurrentCharacterId = '0CD64815-EA5B-4F9B-91C0-EE08F96FBF19'
where Id = 2
update dbo.Player
set CurrentCharacterId = '8462A16F-6F9B-4EBE-8683-2A81978F2D2F'
where Id = 3