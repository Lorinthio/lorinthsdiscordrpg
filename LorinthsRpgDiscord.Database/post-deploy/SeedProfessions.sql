﻿SET IDENTITY_INSERT dbo.CraftProfession ON
INSERT INTO dbo.CraftProfession (Id, Name, IsActive) VALUES
(1, 'Alchemist', 1),
(2, 'Carpenter', 1),
(3, 'Cook', 1),
(4, 'Leatherworker', 1),
(5, 'Smith', 1),
(6, 'Weaver', 1)
SET IDENTITY_INSERT dbo.CraftProfession OFF

SET IDENTITY_INSERT dbo.GatherProfession ON
INSERT INTO dbo.GatherProfession (Id, Name, IsActive) VALUES
(1, 'Fisher', 1),
(2, 'Herbalist', 1),
(3, 'Hunter', 1),
(4, 'Miner', 1),
(5, 'Woodcutter', 1)
SET IDENTITY_INSERT dbo.GatherProfession OFF