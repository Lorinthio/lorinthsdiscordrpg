﻿SET IDENTITY_INSERT dbo.CoreAttributes ON
INSERT INTO dbo.CoreAttributes (Id, Strength, Endurance, Dexterity, Intelligence, Charisma) VALUES
(1, 11, 10, 10, 10, 11),
(2, 9, 9, 12, 12, 10),
(3, 12, 12, 9, 10, 9),
(4, 10, 10, 11, 10, 11),
(5, 11, 11, 10, 10, 10)
SET IDENTITY_INSERT dbo.CoreAttributes OFF

SET IDENTITY_INSERT dbo.Race ON
INSERT INTO dbo.Race (Id, Name, IsActive, ShortDescription, LongDescription, StartingCoreAttributesId, DefaultUnlocked) VALUES
(1, 'Human', 1, 'Humans are pretty common in the realm, they are pretty well rounded but don''t really excel like other races do.', 'Humans are pretty common in the realm, they are pretty well rounded but don''t really excel like other races do.', 1, 1),
(2, 'Evari', 0, 'Evari are born in the forest and as a result are much more dexterous and at one with the nature around them.', 'Evari are born in the forest and as a result are much more dexterous and at one with the nature around them.', 2, 1),
(3, 'Dwani', 0, 'Dwani come from the mountains and live a hardened but hearty social life. As a result their bodies are tougher than others.', 'Dwani come from the mountains and live a hardened but hearty social life. As a result their bodies are tougher than others.', 3, 1),
(4, 'Half-Evari', 0, '', '', 4, 1),
(5, 'Half-Dwani', 0, '', '', 5, 1)
SET IDENTITY_INSERT dbo.Race OFF

SET IDENTITY_INSERT dbo.RaceClasses ON
INSERT INTO dbo.RaceClasses (Id, RaceId, ClassId) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 2, 3),
(6, 3, 1),
(7, 3, 2),
(8, 4, 1),
(9, 4, 2),
(10, 4, 3),
(11, 5, 1),
(12, 5, 2)
SET IDENTITY_INSERT dbo.RaceClasses OFF