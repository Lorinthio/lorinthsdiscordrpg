﻿
:r .\SeedSecurity.sql
:r .\SeedLookups.sql
:r .\SeedProfessions.sql
:r .\SeedClasses.sql
:r .\SeedRaces.sql
:r .\SeedLocations.sql
:r .\SeedItems.sql
:r .\SeedArmor.sql
:r .\SeedTools.sql
:r .\SeedWeapons.sql
:r .\SeedEquipment.sql
:r .\SeedLootTables.sql
:r .\SeedResourceNodes.sql
:r .\SeedTestCharacters.sql