﻿SET IDENTITY_INSERT dbo.LootTable ON
INSERT INTO dbo.LootTable ([Id], [Name]) VALUES
(1, 'Copper Node'),
(2, 'Copper Node *'),
(3, 'Tin Node'),
(4, 'Tin Node *'),
(5, 'Iron Node'),
(6, 'Iron Node *'),
(7, 'Birch Node'),
(8, 'Birch Node *'),
(9, 'Pine Node'),
(10, 'Pine Node *'),
(11, 'Oak Node'),
(12, 'Oak Node *'),
(13, 'Maple Node'),
(14, 'Maple Node *'),
(15, 'North Pelemere Woods Herbs'),
(16, 'North Pelemere Woods Herbs *'),
(17, 'South Pelemere Woods Herbs'),
(18, 'South Pelemere Woods Herbs *'),
(19, 'Sinsevain Grasslands Herbs'),
(20, 'Sinsevain Grasslands Herbs *'),
(21, 'Sinsevain Grasslands Hides'),
(22, 'Sinsevain Grasslands Hides *'),
(23, 'North Pelemere - Deer Woods Hides'),
(24, 'North Pelemere - Deer Woods Hides *'),
(25, 'North Pelemere - Elk Woods Hides'),
(26, 'North Pelemere - Elk Woods Hides *')
SET IDENTITY_INSERT dbo.LootTable OFF

INSERT INTO dbo.LootTableEntry ([LootTableId], [Chance], [TargetItemId], [TargetItemAmount]) VALUES
-- Copper Node
(1, 90.0, 501, 1), -- Copper Ore
(1, 10.0, 503, 1), -- Tin Ore
-- Copper Node *
(2, 75.0, 501, 2), -- Copper Ore x 2
(2, 25.0, 502, 1), -- Copper Ore *
-- Tin Node
(3, 90.0, 503, 1), -- Tin Ore
(3, 10.0, 501, 1), -- Copper Ore
-- Tin Node *
(4, 75.0, 503, 2), -- Tin Ore x 2
(4, 25.0, 504, 1), -- Tin Ore *
-- Iron Node
(5, 80.0, 505, 1), -- Iron Ore
(5, 10.0, 501, 1), -- Copper Ore
(5, 10.0, 503, 1), -- Tin Ore
-- Iron Node *
(6, 80.0, 505, 2), -- Iron Ore x 2
(6, 20.0, 506, 1), -- Iron Ore *

-- Birch Node
(7, 90.0, 601, 1), -- Birch Log
(7, 10.0, 607, 1), -- Pine Log
-- Birch Node *
(8, 75.0, 601, 2), -- Birch Log x 2
(8, 25.0, 602, 1), -- Birch Log *
-- Pine Node 
(9, 90.0, 607, 1), -- Pine Log
(9, 10.0, 601, 1), -- Birch Log
-- Pine Node *
(10, 75.0, 607, 2), -- Pine Log x 2
(10, 25.0, 608, 1), -- Pine Log *
-- Oak Node
(11, 90.0, 603, 1), -- Oak Log
(11, 10.0, 601, 1), -- Birch Log
-- Oak Node *
(12, 75.0, 603, 2), -- Oak Log x 2
(12, 25.0, 604, 1), -- Oak Log *
-- Maple Node
(13, 100.0, 605, 1), -- Maple Log
-- Maple Node *
(14, 80.0, 605, 2), -- Maple Log x 2
(14, 20.0, 606, 1), -- Maple Log *


-- North Pelemere Woods Herbalism
(15, 35.0, 701, 1), -- Ebinut
(15, 35.0, 706, 1), -- Forest Chervill
(15, 15.0, 708, 1), -- Kallowfrass
(15, 15.0, 711, 1), -- Uzumin
-- North Pelemere Woods Herbalism *
(16, 35.0, 701, 2), -- Ebinut
(16, 35.0, 706, 2), -- Forest Chervil
(16, 15.0, 708, 2), -- Kallowfrass
(16, 15.0, 711, 2), -- Uzumin

-- South Pelemere Woods Herbalism
(17, 50.0, 710, 1),	-- Autumn Mint
(17, 40.0, 704, 1), -- Dusk Blossom
(17, 10.0, 705, 1), -- Star Ivy
-- South Pelemere Woods Herbalism *
(18, 45.0, 710, 2), -- Autumn Mint
(18, 40.0, 704, 2),	-- Dusk Blossom
(18, 15.0, 705, 2),	-- Star Ivy

-- Grasslands
(19, 100.0, 702, 2), -- Whitegrass x 2
-- Grasslands *
(20, 100.0, 702, 3), -- Whitegrass x 3

-- Sinsevain Grasslands - Cows
(21, 100.0, 801, 1), -- Cow Hide
(22, 100.0, 801, 2), -- Cow Hide x 2

-- North Pelemere - Deer Woods - Hunting
(23, 90.0, 802, 1), -- Deer Hide
(23, 10.0, 803, 1), -- Elk Hide

(24, 100.0, 802, 2), -- Deer Hide x 2 

-- North Pelemere - Elk Woods - Hunting
(25, 90.0, 803, 1), -- Elk Hide
(25, 10.0, 802, 1),	-- Deer Hide

(26, 100.0, 803, 2) -- Elk Hide x 2
