﻿SET IDENTITY_INSERT dbo.Trait ON
INSERT INTO dbo.Trait (Id, Name) VALUES
(1, 'Versatile'),
(2, 'Fire Aspect'),
(3, 'Water Aspect'),
(4, 'Earth Aspect'),
(5, 'Air Aspect'),
(6, 'Light Aspect'),
(7, 'Dark Aspect'),
(8, 'Silvered'),
(9, 'Throwable'),
(10, 'Poison'),
(11, 'Broken'), -- 50% damage/defense rating, Reach = 1
(12, 'Oversized'), --
(13, 'Two Handed'),
(14, 'Cursed'),
(15, 'Leeching'),
(16, 'Serrated'),
(17, 'Fortified'), --
(18, 'Light'), -- Move 3 squares
(19, 'Heavy'), -- 1 AP - 1 squares
(20, 'Blunt'),
(21, 'Pierce'),
(22, 'Ammo')   -- Uses ammo
SET IDENTITY_INSERT dbo.Trait OFF

SET IDENTITY_INSERT dbo.Item ON
INSERT INTO dbo.Item ([Id], [Name], [Description], [Value], [Weight]) VALUES
-- ==========================
-- Tools 1-500
-- ==========================
(1, 'Birch Fishing Rod', 'Profession: Fishing \n', 1, 5),
(2, 'Pine Fishing Rod', 'Profession: Fishing \n', 75, 5),
(3, 'Oak Fishing Rod', 'Profession: Fishing \n', 150, 5),
(4, 'Maple Fishing Rod', 'Profession: Fishing \n', 300, 5),
(5, 'Bronze Sickle', 'Profession: Herbalism \n', 1, 5),
(6, 'Iron Sickle', 'Profession: Herbalism \n', 75, 5),
(7, 'Invar Sickle', 'Profession: Herbalism \n', 150, 5),
(8, 'Steel Sickle', 'Profession: Herbalism \n', 300, 5),
(9, 'Bronze Skinning Knife', 'Profession: Hunting \n', 1, 5),
(10, 'Iron Skinning Knife', 'Profession: Hunting \n', 75, 5),
(11, 'Invar Skinning Knife', 'Profession: Hunting \n', 150, 5),
(12, 'Steel Skinning Knife', 'Profession: Hunting \n', 300, 5),
(13, 'Bronze Pickaxe', 'Profession: Mining \n', 1, 5),
(14, 'Iron Pickaxe', 'Profession: Mining \n', 1, 5),
(15, 'Invar Pickaxe', 'Profession: Mining \n', 1, 5),
(16, 'Steel Pickaxe', 'Profession: Mining \n', 1, 5),
(17, 'Bronze Felling Axe', 'Profession: Woodcutting \n', 50, 5),
(18, 'Iron Felling Axe', 'Profession: Woodcutting \n', 1, 5),
(19, 'Invar Felling Axe', 'Profession: Woodcutting \n', 1, 5),
(20, 'Steel Felling Axe', 'Profession: Woodcutting \n', 1, 5),
(21, 'Masterwork Birch Fishing Rod', 'Profession: Fishing \n', 100, 5),
(22, 'MasterworkPine Fishing Rod', 'Profession: Fishing \n', 75, 5),
(23, 'Masterwork Oak Fishing Rod', 'Profession: Fishing \n', 200, 5),
(24, 'Masterwork Maple Fishing Rod', 'Profession: Fishing \n', 500, 5),
(25, 'Masterwork Bronze Sickle', 'Profession: Herbalism \n', 100, 5),
(26, 'Masterwork Iron Sickle', 'Profession: Herbalism \n', 150, 5),
(27, 'Masterwork Invar Sickle', 'Profession: Herbalism \n', 300, 5),
(28, 'Masterwork Steel Sickle', 'Profession: Herbalism \n', 600, 5),
(29, 'Masterwork Bronze Skinning Knife', 'Profession: Hunting \n', 100, 5),
(30, 'Masterwork Iron Skinning Knife', 'Profession: Hunting \n', 150, 5),
(31, 'Masterwork Invar Skinning Knife', 'Profession: Hunting \n', 300, 5),
(32, 'Masterwork Steel Skinning Knife', 'Profession: Hunting \n', 600, 5),
(33, 'Masterwork Bronze Pickaxe', 'Profession: Mining \n', 100, 5),
(34, 'Masterwork Iron Pickaxe', 'Profession: Mining \n', 150, 5),
(35, 'Masterwork Invar Pickaxe', 'Profession: Mining \n', 300, 5),
(36, 'Masterwork Steel Pickaxe', 'Profession: Mining \n', 600, 5),
(37, 'Masterwork Bronze Felling Axe', 'Profession: Woodcutting \n', 100, 5),
(38, 'Masterwork Iron Felling Axe', 'Profession: Woodcutting \n', 150, 5),
(39, 'Masterwork Invar Felling Axe', 'Profession: Woodcutting \n', 300, 5),
(40, 'Masterwork Steel Felling Axe', 'Profession: Woodcutting \n', 600, 5),

-- ==========================
-- Resources
-- ==========================

-- ORE 501-600

(501, 'Copper Ore', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 10, 10),
(502, 'Copper Ore *', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 25, 10),
(503, 'Tin Ore', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 10, 5),
(504, 'Tin Ore *', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 25, 5),
(505, 'Iron Ore', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 25, 5),
(506, 'Iron Ore *', 'A common mineral found in caves and mountains. Used in the production of lower-tier metal working. Requires a high-temperature Furnace to smelt the ore.', 50, 5),
(507, 'Lead Ore', '', 25, 10),
(508, 'Lead Ore *', '', 50, 10),
(509, 'Gold Ore', '', 50, 15),
(510, 'Gold Ore *', '', 100, 15),
(511, 'Platinum Ore', '', 50, 15),
(512, 'Platinum Ore *', '', 100, 15),
(513, 'Mythril Ore', '', 25, 15),
(514, 'Mythril Ore *', '', 50, 15),
(515, 'Adamantite Ore', '', 25, 15),
(516, 'Adamantite Ore *', '', 50, 15),
(517, 'Silver Ore', '', 25, 10),
(518, 'Silver Ore *', '', 50, 10),

-- WOOD 601-700

(601, 'Birch Log', 'Wooden Log cut from a Birch Tree', 10, 5),
(602, 'Birch Log *', 'Wooden Log cut from a Birch Tree', 25, 5),
(603, 'Oak Log', 'Wooden Log cut from a Oak Tree', 10, 5),
(604, 'Oak Log *', 'Wooden Log cut from a Oak Tree', 25, 5),
(605, 'Maple Log', 'Wooden Log cut from a Maple Tree', 10, 5),
(606, 'Maple Log *', 'Wooden Log cut from a Maple Tree', 25, 5),
(607, 'Pine Log', 'Wooden Log cut from a Pine Tree', 10, 5),
(608, 'Pine Log *', 'Wooden Log cut from a Pine Tree', 25, 5),
(609, 'Elm Log', 'Wooden Log cut from a Elm Tree', 10, 5),
(610, 'Elm Log *', 'Wooden Log cut from a Elm Tree', 25, 5),
(611, 'Spruce Log', 'Wooden Log cut from a Spruce Tree', 10, 5),
(612, 'Spruce Log *', 'Wooden Log cut from a Spruce Tree', 25, 5),
(613, 'Ash Log', 'Wooden Log cut from a Ash Tree', 10, 5),
(614, 'Ash Log *', 'Wooden Log cut from a Ash Tree', 25, 5),
(615, 'Hickory Log', 'Wooden Log cut from a Hickory Tree', 10, 5),
(616, 'Hickory Log *', 'Wooden Log cut from a Hickory Tree', 25, 5),
(617, 'Mahogany Log', 'Wooden Log cut from a Mahogany Tree', 10, 5),
(618, 'Mahogany Log *', 'Wooden Log cut from a Mahogany Tree', 25, 5),
(619, 'Ifant Log', 'Wooden Log cut from a Ifant Tree', 10, 5),
(620, 'Ifant Log *', 'Wooden Log cut from a Ifant Tree', 25, 5),

-- Herbalism 701-800

(701, 'Ebinut', '', 0, 1),			-- North Pelemere Woods
(702, 'Whitegrass', '', 0, 1),		-- Grasslands
(703, 'Aquaseed', '', 0, 1),		-- Coast, rivers
(704, 'Dusk Blossom', '', 0, 1),	-- South Pelemere Woods
(705, 'Star Ivy', '', 0, 1),		-- South Pelemere Woods
(706, 'Forest Chervil', '', 0, 1),  -- North Pelemere Woods
(707, 'Ocean Hyssop', '', 0, 1),	-- Coast
(708, 'Kallowfrass', '', 0, 1),		-- North Pelemere Woods
(709, 'Snowy Oxunel', '', 0, 1),	-- Mountains
(710, 'Autumn Mint', '', 0, 1),		-- South Pelemere Woods
(711, 'Uzumin', '', 0, 1),			-- North Pelemere Woods
(712, 'Nightspore', '', 0, 1),		-- Hills

-- Hunter 801-900

(801, 'Cow Hide', '', 0, 5),		-- Grasslands
(802, 'Deer Pelt', '', 0, 5),		-- North Pelemere Woods 
(803, 'Elk Pelt', '', 0, 5),		-- North Pelemere Woods
(804, 'Wolf Pelt', '', 0, 5),		
(805, 'Boar Pelt', '', 0, 5),
(806, 'Bear Pelt', '', 0, 5),
(807, 'Bobcat Pelt', '', 0, 5),

-- Fishing 901-1000
(901, 'Cod', '', 0, 5), -- Port Adder
(902, 'Trout', '', 0, 5), -- Port Adder
(903, 'Tuna', '', 0, 5), -- Silentfall Port
(904, 'Salmon', '', 0, 5), -- Silentfall Port
(905, 'Catfish', '', 0, 5),
(906, 'Swordfish', '', 0, 5),
(907, 'Octopus', '', 0, 5),
(908, 'Eel', '', 0, 5),
(909, 'Bass', '', 0, 5),
(910, 'Sea bass', '', 0, 5),
(911, 'Pike (fish)', '', 0, 5),
(912, 'Barracuda', '', 0, 5),

-- Weapons 1501-3000

-- Armor 3001-4500

-- Misc 4501-10000
(4501, 'Copper Coin (Human)', 'A simple copper piece with the profile view of a court jester', 1, 0.1),
(4502, 'Copper Coin (Dwani)', 'A rustic octagonal copper piece with a mug of ale etched into its face', 1, 0.1),
(4503, 'Copper Coin (Evari)', 'A diamond shaped copper piece with the silhouette tree of life rising from its copper surface', 1, 0.1),
(4504, 'Silver Coin (Human)', 'A shiny silver piece with the profile view of a Queen, her head donning a circlet with a jewel', 10, 0.5),
(4505, 'Silver Coin (Dwani)', 'An octagonal silver piece with the shape of an anvil etched into the surface', 1, 0.5),
(4506, 'Silver Coin (Evari)', 'A diamond shaped silver piece with the silhouette of the ancient goddess', 1, 0.5)

-- Crafting Materials 10001+

SET IDENTITY_INSERT dbo.Item OFF