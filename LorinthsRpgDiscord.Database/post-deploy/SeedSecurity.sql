﻿INSERT INTO dbo.[Role] ([Id], [Name]) VALUES
('715382414829944913', 'RPG World Builder'),
('735623449191645215', 'RPG News Poster')

SET IDENTITY_INSERT [WebsitePermission] ON
INSERT INTO dbo.[WebsitePermission] ([Id], [Name], [IsActive]) VALUES
(1, 'World Builder', 1),
(2, 'News Poster', 1)
SET IDENTITY_INSERT [WebsitePermission] OFF

INSERT INTO dbo.RoleWebsitePermission ([RoleId], [WebsitePermissionId]) VALUES
('715382414829944913', 1),
('735623449191645215', 2)