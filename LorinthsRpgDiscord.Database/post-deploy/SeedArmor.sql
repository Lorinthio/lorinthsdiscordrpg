﻿SET IDENTITY_INSERT [dbo].[ArmorSlot] ON

insert into [dbo].[ArmorSlot] ([Id], [Name])
VALUES
(1,	'Head'),
(2,	'Body'),
(3,	'Hands'),
(4,	'Feet'),
(5,	'Back'),
(6,	'Necklace'),
(7,	'Ring1'),
(8,	'Ring2'),
(9,	'Misc')

SET IDENTITY_INSERT [dbo].[ArmorSlot] OFF