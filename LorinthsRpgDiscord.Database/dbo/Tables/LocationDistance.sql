﻿CREATE TABLE [dbo].[LocationDistance]
(
	[FirstLocationId]	int				NOT NULL,
	[SecondLocationId]	int				NOT NULL,
	[DistanceInMiles]	decimal(10,2)   NULL		DEFAULT(1.0),
	CONSTRAINT [FK_LocationDistance_FirstLocationId] FOREIGN KEY ([FirstLocationId]) REFERENCES [dbo].[Location]([Id]),
	CONSTRAINT [FK_LocationDistance_SecondLocationId] FOREIGN KEY ([SecondLocationId]) REFERENCES [dbo].[Location]([Id])
)
