﻿CREATE TABLE [dbo].[QuestStepObjectiveType]
(
	[Id]	INT				NOT NULL	IDENTITY(1,1),
	[Name]	VARCHAR(25)		NOT NULL,
	CONSTRAINT [PK_QuestStepObjectiveType]	PRIMARY KEY CLUSTERED ([Id] ASC),
)
