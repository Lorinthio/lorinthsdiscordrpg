﻿CREATE TABLE [dbo].[GameSessionPlayer]
(
	[GameSessionId]		uniqueidentifier	NOT NULL,
	[PlayerId]			int					NOT NULL,
	CONSTRAINT [PK_GameSessionPlayer] PRIMARY KEY ([GameSessionId], [PlayerId]),
	CONSTRAINT [FK_GameSessionPlayer_GameSession_GameSessionId] FOREIGN KEY ([GameSessionId]) REFERENCES dbo.GameSession ([Id]),
	CONSTRAINT [FK_GameSessionPlayer_Player_PlayerId] FOREIGN KEY ([PlayerId]) REFERENCES dbo.Player ([Id])
)
