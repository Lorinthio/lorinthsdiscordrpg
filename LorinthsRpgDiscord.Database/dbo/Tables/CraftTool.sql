﻿CREATE TABLE [dbo].[CraftTool]
(
	[ItemId]				INT				NOT NULL	PRIMARY KEY,
	[CraftProfessionId]		INT				NOT NULL,
	[SkillLevelRequired]	INT				NOT NULL	DEFAULT(1),
	[CriticalChance]		DECIMAL(10,2)	NOT NULL,
	CONSTRAINT [FK_CraftTool_CraftProfession_CraftProfessionId] FOREIGN KEY ([CraftProfessionId]) REFERENCES [dbo].[CraftProfession]([Id]),
	CONSTRAINT [FK_CraftTool_Item_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item]([Id]),
)
