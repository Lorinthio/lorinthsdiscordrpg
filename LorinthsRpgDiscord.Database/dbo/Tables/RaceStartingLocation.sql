﻿CREATE TABLE [dbo].[RaceStartingLocation]
(
	[Id]			INT		NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[RaceId]		INT		NOT NULL,
	[LocationId]	INT		NOT NULL,
	CONSTRAINT [FK_RaceStartingLocation_Race_RaceId] FOREIGN KEY([RaceId]) 
		REFERENCES [dbo].[Race] ([Id]),
	CONSTRAINT [FK_RaceStartingLocation_Location_LocationId] FOREIGN KEY(LocationId) 
		REFERENCES [dbo].[Location] ([Id]),
)
