﻿CREATE TABLE [dbo].[ResourceNode]
(
	[Id]						INT		NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[SubLocationId]				INT		NOT NULL	UNIQUE,
	[GatherProfessionId]		INT		NOT NULL,
	[ToolLevelRequired]			INT		NOT NULL	DEFAULT(1),
	[LootTableId]				INT		NOT NULL,
	[LootTableCritId]			INT		NOT NULL, 
    CONSTRAINT [FK_ResourceNode_SubLocation_SubLocationId] FOREIGN KEY([SubLocationId]) 
		REFERENCES [dbo].[SubLocation] ([Id]),
	CONSTRAINT [FK_ResourceNode_GatherProfession_GatherProfessionId] FOREIGN KEY([GatherProfessionId]) 
		REFERENCES [dbo].[GatherProfession] ([Id]),
	CONSTRAINT [FK_ResourceNode_LootTable_LootTableId] FOREIGN KEY([LootTableId]) 
		REFERENCES [dbo].[LootTable] ([Id]),
	CONSTRAINT [FK_ResourceNode_LootTable_LootTableCritId] FOREIGN KEY([LootTableCritId]) 
		REFERENCES [dbo].[LootTable] ([Id]),
)
