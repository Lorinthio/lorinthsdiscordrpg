﻿CREATE TABLE [dbo].[WebSession]
(
	[Id]			UNIQUEIDENTIFIER	PRIMARY KEY,
	[PlayerId]		INT NOT NULL,
	[IpAddress]		VARCHAR(50) NOT NULL,
	[Expiration]	DATE NOT NULL,
	CONSTRAINT [FK_WebSession_Player_PlayerId] FOREIGN KEY ([PlayerId]) REFERENCES dbo.[Player] ([Id])
)
