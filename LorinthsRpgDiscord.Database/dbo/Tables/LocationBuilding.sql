﻿CREATE TABLE [dbo].[LocationBuildings]
(
	[LocationId]	int		NOT NULL,
	[BuildingId]	int		NOT NULL
	CONSTRAINT [FK_LocationBuildings_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([Id]),
	CONSTRAINT [FK_LocationBuildings_BuildingId] FOREIGN KEY ([BuildingId]) REFERENCES [dbo].[Building] ([Id]),
)
