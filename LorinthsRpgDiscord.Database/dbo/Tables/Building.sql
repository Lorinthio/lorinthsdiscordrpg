﻿CREATE TABLE [dbo].[Building]
(
	[Id]			int				NOT NULL	IDENTITY(1,1),
	[Name]			nvarchar(max)	NOT NULL,
	[BuildingType]  int				NOT NULL,
	[IsActive]		bit				NOT NULL,
	CONSTRAINT [PK_Building] PRIMARY KEY CLUSTERED ([Id] ASC),
)
