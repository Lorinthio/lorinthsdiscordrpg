﻿CREATE TABLE [dbo].[GameSession]
(
	[Id]			uniqueidentifier	NOT NULL	PRIMARY KEY,
	[MapName]		varchar(100)		NOT NULL,
	[EncounterName]	varchar(100)		NOT NULL,
)
