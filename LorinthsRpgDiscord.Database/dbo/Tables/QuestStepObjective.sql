﻿CREATE TABLE [dbo].[QuestStepObjective]
(
	[Id]				INT				NOT NULL	IDENTITY(1,1),
	[Description]		nvarchar(1000)	NOT NULL,
	[QuestStepObjectiveTypeId]	INT				NOT NULL,
	[TargetId]			INT				NOT NULL,
	[Quantity]			INT				NOT NULL,
	[QuestStepId]		INT				NOT NULL,
	[Order]				INT				NOT NULL,
	CONSTRAINT [PK_QuestStepObjective]	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Objective_QuestStepId] FOREIGN KEY ([QuestStepId]) REFERENCES [dbo].[QuestStep]([Id]),
	CONSTRAINT [FK_Objective_QuestStepObjectiveTypeId] FOREIGN KEY ([QuestStepObjectiveTypeId]) REFERENCES [dbo].[QuestStepObjectiveType]([Id])
)
