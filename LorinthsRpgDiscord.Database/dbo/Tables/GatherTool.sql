﻿CREATE TABLE [dbo].[GatherTool]
(
	[ItemId]				INT				NOT NULL	PRIMARY KEY,
	[GatherProfessionId]	INT				NOT NULL,
	[SkillLevelRequired]	INT				NOT NULL	DEFAULT(1),
	[CriticalChance]		DECIMAL(10,2)	NOT NULL,
	CONSTRAINT [FK_GatherTool_GatherProfession_GatherProfessionId] FOREIGN KEY ([GatherProfessionId]) REFERENCES [dbo].[GatherProfession]([Id]),
	CONSTRAINT [FK_GatherTool_Item_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item]([Id]),
)
