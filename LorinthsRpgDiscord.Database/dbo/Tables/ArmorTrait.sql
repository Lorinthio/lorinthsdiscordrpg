﻿CREATE TABLE [dbo].[ArmorTrait]
(
	[ArmorItemId]	INT		NOT NULL PRIMARY KEY,
	[TraitId]		INT		NOT NULL,
	CONSTRAINT [FK_ArmorTrait_Armor_ArmorId] FOREIGN KEY ([ArmorItemId]) REFERENCES [dbo].[Armor]([ItemId]),
	CONSTRAINT [FK_ArmorTrait_Trait_TraitId] FOREIGN KEY ([TraitId]) REFERENCES [dbo].[Trait]([Id]),
)