﻿CREATE TABLE [dbo].[BuildingType]
(
	[Id]		int				NOT NULL	IDENTITY(1,1),
	[Name]		nvarchar(max)	NOT NULL,
	[IsActive]	bit				NOT NULL,
	CONSTRAINT [PK_BuildingType] PRIMARY KEY CLUSTERED ([Id] ASC),
)
