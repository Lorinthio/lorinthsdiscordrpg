﻿CREATE TABLE [dbo].[GatherProfession]
(
	[Id]		INT				NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[Name]		VARCHAR(50)		NOT NULL,
	[IsActive]	BIT				NOT NULL
)
