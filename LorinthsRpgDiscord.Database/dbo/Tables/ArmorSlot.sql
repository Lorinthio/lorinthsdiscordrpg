﻿CREATE TABLE [dbo].[ArmorSlot]
(
	[Id]				int 			NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[Name]				nvarchar(max)	NOT NULL
)