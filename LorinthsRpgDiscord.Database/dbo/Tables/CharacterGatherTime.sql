﻿CREATE TABLE [dbo].[CharacterGatherTime]
(
	[CharacterId]		UNIQUEIDENTIFIER		NOT NULL	PRIMARY KEY,
	[ResourceNodeId]	INT						NOT NULL,
	[FinishTime]		DATETIME				NOT NULL,
	CONSTRAINT [FK_CharacterGatherTimes_Character_CharacterId] FOREIGN KEY ([CharacterId]) REFERENCES [dbo].[Character]([Id]),
)
