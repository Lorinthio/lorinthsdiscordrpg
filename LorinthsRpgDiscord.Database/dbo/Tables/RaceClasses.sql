﻿CREATE TABLE [dbo].[RaceClasses]
(
	[Id]		INT		NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[RaceId]	INT		NOT NULL,
	[ClassId]	INT		NOT NULL,
	CONSTRAINT [FK_RaceClasses_Race_RaceId] FOREIGN KEY([RaceId]) 
		REFERENCES [dbo].[Race] ([Id]),
	CONSTRAINT [FK_RaceClasses_Class_ClassId] FOREIGN KEY([ClassId]) 
		REFERENCES [dbo].[Class] ([Id]),
)
