﻿CREATE TABLE [dbo].[LootTableEntry]
(
	[Id]				INT				NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[LootTableId]		INT				NOT NULL,
	[Chance]			DECIMAL(10,2)	NOT NULL	DEFAULT(100.0),
	[TargetItemId]		INT				NULL,
	[TargetItemAmount]	INT				NULL,
	CONSTRAINT [FK_LootTableEntry_LootTable_LootTableId] FOREIGN KEY([LootTableId]) 
		REFERENCES [dbo].[LootTable] ([Id]),
	CONSTRAINT [FK_LootTableEntry_Item_TargetItemId] FOREIGN KEY([TargetItemId]) 
		REFERENCES [dbo].[Item] ([Id])
)
