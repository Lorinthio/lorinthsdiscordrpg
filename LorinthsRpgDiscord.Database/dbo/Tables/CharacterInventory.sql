﻿CREATE TABLE [dbo].[CharacterInventory]
(
	[CharacterId]	UNIQUEIDENTIFIER	NOT NULL,
	[ItemId]		INT					NOT NULL,
	[Amount]		INT					NOT NULL		DEFAULT(0),
	CONSTRAINT [PK_CharacterInventory] PRIMARY KEY CLUSTERED ([CharacterId], [ItemId] ASC),
	CONSTRAINT [FK_CharacterInventory_Character_CharacterId] FOREIGN KEY([CharacterId]) 
		REFERENCES [dbo].[Character] ([Id]),
	CONSTRAINT [FK_CharacterInventory_Item_ItemId] FOREIGN KEY([ItemId]) 
		REFERENCES [dbo].[Item] ([Id]),

)
