﻿CREATE TABLE [dbo].[CharacterTravelTime]
(
	[CharacterId] uniqueidentifier NOT NULL,
	[LocationId] int NOT NULL,
	[ArrivalTime] datetime NOT NULL,
	CONSTRAINT [PK_CharacterTravelTimes] PRIMARY KEY ([CharacterId]),
	CONSTRAINT [FK_CharacterTravelTimes_Character_CharacterId] FOREIGN KEY([CharacterId]) 
		REFERENCES [dbo].[Character] ([Id]),
	CONSTRAINT [FK_CharacterTravelTimes_Location_LocationId] FOREIGN KEY([LocationId]) 
		REFERENCES [dbo].[Location] ([Id])
)
