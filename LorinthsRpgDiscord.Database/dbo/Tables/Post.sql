﻿CREATE TABLE [dbo].[Post]
(
	[Id]				INT				NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[CreatorId]			VARCHAR(20)		NOT NULL, -- Discord Id of creator of the post
	[Title]				VARCHAR(200)	NOT NULL,
	[Content]			VARCHAR(MAX)	NOT NULL,
	[UploadDate]		DATETIME		NOT NULL,
	[LastUpdateDate]	DATETIME		NULL,
	[EditingMode]		BIT				NOT NULL
)
