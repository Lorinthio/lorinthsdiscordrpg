﻿CREATE TABLE [dbo].[QuestStep]
(
	[Id]			INT				NOT NULL	IDENTITY(1,1),
	[Name]			NVARCHAR(100)	NOT NULL,
	[Description]	NVARCHAR(1000)	NOT NULL,
	[QuestId]		INT				NOT NULL,
	[Order]			INT				NOT NULL,
	CONSTRAINT [PK_QuestStep]	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_QuestStep_QuestId] FOREIGN KEY ([QuestId]) REFERENCES [dbo].[Quest]([Id]),
)