﻿CREATE TABLE [dbo].[SubLocation](
	[Id]					int IDENTITY(1,1)	NOT NULL,
	[Name]					nvarchar(max)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[LocationId]			int					NOT NULL,
	[IsStartingSubLocation]	bit					NOT NULL	DEFAULT(0),
	[CanTravel]				bit					NOT NULL	DEFAULT(0),
    [IsActive]				BIT					NOT NULL	DEFAULT(1), 
    CONSTRAINT [PK_SubLocation] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_SubLocation_Location_LocationId] FOREIGN KEY([LocationId]) 
		REFERENCES [dbo].[Location] ([Id])
)