﻿CREATE TABLE [dbo].[Region]
(
	[Id]			INT				NOT NULL	IDENTITY(1,1),
	[Name]			VARCHAR(50)		NOT NULL,
	[Description]	VARCHAR(2000)	NOT NULL,
	[ContinentId]	INT				NOT NULL,
	[IsActive]		BIT				NOT NULL	DEFAULT(1),
	CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Region_Continent_ContinentId] FOREIGN KEY([ContinentId]) 
		REFERENCES [dbo].[Continent] ([Id]),
)
