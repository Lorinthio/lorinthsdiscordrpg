﻿CREATE TABLE [dbo].[RoleWebsitePermission]
(
	[RoleId]				VARCHAR(20) NOT NULL,
	[WebsitePermissionId]	INT			NOT NULL,
	CONSTRAINT [FK_PlayerRoleWebsitePermission_Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Role]([Id]),
	CONSTRAINT [FK_PlayerRoleWebsitePermission_WebsitePermission_WebsitePermissionId] FOREIGN KEY ([WebsitePermissionId]) REFERENCES [WebsitePermission]([Id])
)
