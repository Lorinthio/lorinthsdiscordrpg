﻿CREATE TABLE [dbo].[WebsitePermission]
(
	[Id]		INT			NOT NULL	PRIMARY KEY		IDENTITY(1,1),
	[Name]		VARCHAR(20)	NOT NULL,
	[IsActive]	BIT			NOT NULL	DEFAULT(1)
)
