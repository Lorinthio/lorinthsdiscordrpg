﻿CREATE TABLE [dbo].[PlayerRole]
(
	[PlayerId]		INT			NOT NULL,
	[RoleId]		VARCHAR(20)	NOT NULL,
	[IsOverride]	BIT			NOT NULL,
	CONSTRAINT [PK_PlayerRoles] PRIMARY KEY ([PlayerId], [RoleId]),
	CONSTRAINT [FK_PlayerRoles_Player_PlayerId] FOREIGN KEY([PlayerId]) 
		REFERENCES [dbo].[Player] ([Id]),
	CONSTRAINT [FK_PlayerRoles_Role_RoleId] FOREIGN KEY([RoleId]) 
		REFERENCES [dbo].[Role] ([Id]),
)
