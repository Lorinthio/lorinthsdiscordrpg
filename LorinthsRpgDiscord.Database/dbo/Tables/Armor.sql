﻿CREATE TABLE [dbo].[Armor]
(
	[ItemId]		INT		NOT NULL	PRIMARY KEY,
	[Defense]		INT		NOT NULL	DEFAULT(0),
	[MagicDefense]	INT		NOT NULL	DEFAULT(0),
	[Dodge]			INT		NOT NULL	DEFAULT(0),
	[ArmorSlotId]	INT		NOT NULL	DEFAULT(0),
	CONSTRAINT [FK_Armor_Item_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item]([Id]),
	CONSTRAINT [FK_Armor_ArmorSlot_ArmorSlotId] FOREIGN KEY ([ArmorSlotId]) REFERENCES [dbo].[ArmorSlot]([Id])
)
