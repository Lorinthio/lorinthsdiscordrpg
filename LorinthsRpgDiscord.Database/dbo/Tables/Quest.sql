﻿CREATE TABLE [dbo].[Quest]
(
	[Id]				INT				NOT NULL	IDENTITY(1,1),
	[Name]				NVARCHAR(100)	NOT NULL,
	[Description]		NVARCHAR(1000)	NOT NULL,
	[IsActive]			BIT				NOT NULL,
	[LevelRequired]		INT				NULL,
	[ClassIdRequired]	INT				NULL,
	[RaceIdRequired]	INT				NULL,
	CONSTRAINT [PK_Quest]	PRIMARY KEY CLUSTERED ([Id] ASC),
)
