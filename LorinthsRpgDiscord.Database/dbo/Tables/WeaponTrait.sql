﻿CREATE TABLE [dbo].[WeaponTrait]
(
	[WeaponItemId]	INT		NOT NULL	PRIMARY KEY,
	[TraitId]		INT		NOT NULL,
	CONSTRAINT [FK_WeaponTrait_Weapon_WeaponId] FOREIGN KEY ([WeaponItemId]) REFERENCES [dbo].[Weapon]([ItemId]),
	CONSTRAINT [FK_WeaponTrait_Trait_TraitId] FOREIGN KEY ([TraitId]) REFERENCES [dbo].[Trait]([Id]),
)
