﻿CREATE TABLE [dbo].[Weapon]
(
	[ItemId]			INT				NOT NULL	PRIMARY KEY,
	[Power]				INT				NOT NULL	DEFAULT(1),
	[Parry]				DECIMAL(10,2)	NOT NULL	DEFAULT(0),
	[Reach]				INT				NOT NULL	DEFAULT(1),
	[ActionCost]		INT				NOT NULL	DEFAULT(2),
	CONSTRAINT [FK_Weapon_Item_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item]([Id]),
)
