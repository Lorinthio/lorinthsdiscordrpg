﻿CREATE TABLE [dbo].[GatheringProfessionStartingTool]
(
	[Id]					INT		IDENTITY(1,1)	PRIMARY KEY,
	[GatherProfessionId]	INT		NOT NULL	UNIQUE,
	[StartingItemId]		INT		NOT NULL,
	CONSTRAINT [FK_GatheringProfessionStartingTool_GatherProfession_GatherProfessionId] FOREIGN KEY ([GatherProfessionId]) REFERENCES [dbo].[GatherProfession]([Id]),
	CONSTRAINT [FK_GatheringProfessionStartingTool_Item_StartingItemId] FOREIGN KEY ([StartingItemId]) REFERENCES [dbo].[Item]([Id]),
)
