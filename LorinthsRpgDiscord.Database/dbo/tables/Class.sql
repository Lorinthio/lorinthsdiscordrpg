﻿CREATE TABLE [dbo].[Class](
	[Id]				int IDENTITY(1,1)	NOT NULL,
	[Name]				nvarchar(max)		NOT NULL,
	[IsActive]			bit					NOT NULL,
	[ShortDescription]	nvarchar(max)		NULL,
	[LongDescription]	nvarchar(max)		NULL,
	[DefaultUnlocked]	bit default(1)		NOT NULL,
	CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED ([Id] ASC)
 )