﻿CREATE TABLE [dbo].[Location](
	[Id]					int IDENTITY(1,1)	NOT NULL,
	[Name]					nvarchar(max)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[LocationTypeId]		int					NOT NULL,
	[RegionId]				int					NOT NULL,
	[IsActive]				bit					NOT NULL	DEFAULT(1),
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Location_LocationType_LocationTypeId] FOREIGN KEY([LocationTypeId]) 
		REFERENCES [dbo].[LocationType] ([Id]),
	CONSTRAINT [FK_Location_Location_RegionId] FOREIGN KEY([RegionId]) 
		REFERENCES [dbo].[Region] ([Id])
)