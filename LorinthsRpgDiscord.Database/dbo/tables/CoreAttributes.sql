﻿CREATE TABLE [dbo].[CoreAttributes](
	[Id]			int IDENTITY(1,1)	NOT NULL,
	[Strength]		int					NOT NULL,
	[Endurance]		int					NOT NULL,
	[Dexterity]		int					NOT NULL,	
	[Intelligence]	int					NOT NULL,
	[Charisma]		int					NOT NULL,
	CONSTRAINT [PK_CoreAttributes] PRIMARY KEY CLUSTERED ([Id] ASC)
 )