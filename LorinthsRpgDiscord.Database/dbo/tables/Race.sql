﻿CREATE TABLE [dbo].[Race](
	[Id]						int					NOT NULL	IDENTITY(1,1),
	[Name]						nvarchar(max)		NOT NULL,
	[IsActive]					bit					NOT NULL	DEFAULT(1),
	[ShortDescription]			nvarchar(max)		NULL,
	[LongDescription]			nvarchar(max)		NULL,
	[StartingCoreAttributesId] int					NOT NULL,
	[DefaultUnlocked]			bit default(0)		NOT NULL,
	CONSTRAINT [PK_Race] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Race_CoreAttributes_StartingCoreAttributeId] FOREIGN KEY([StartingCoreAttributesId]) 
		REFERENCES [dbo].[CoreAttributes] ([Id])
)