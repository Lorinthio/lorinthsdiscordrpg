﻿CREATE TABLE [dbo].[Player](
	[Id]					int 				NOT NULL	IDENTITY(1,1),
	[UserId]				varchar(20)			NOT NULL,
	[Username]				varchar(50)			NOT NULL,
	[Avatar]				varchar(50)			NULL,
	[CurrentCharacterId]	uniqueidentifier	NULL,
	CONSTRAINT [PK_Players]	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Players_Character_CurrentCharacterId] FOREIGN KEY([CurrentCharacterId])
		REFERENCES [dbo].[Character] ([Id])
)
GO