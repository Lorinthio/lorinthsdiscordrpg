﻿CREATE TABLE [dbo].[LocationType](
	[Id]		int				NOT NULL	IDENTITY(1,1),
	[Name]		nvarchar(100)	NULL,
	[IsActive]	bit				NOT NULL	DEFAULT(1),
	CONSTRAINT [PK_LocationTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
)