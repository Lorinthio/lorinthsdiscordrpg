﻿CREATE TABLE [dbo].[Character](
	[Id]					uniqueidentifier	NOT NULL,
	[PlayerId]				int					NOT NULL,
	[CharacterName]			nvarchar(50)		NULL,
	[Bio]					nvarchar(max)		NULL,
	[RaceId]				int					NOT NULL	default(1),
	[ClassId]				int					NOT NULL	default(1),
	[CraftProfessionId]		int					NOT NULL	default(1),
	[GatherProfessionId]	int					NOT NULL	default(1),
	[Created]				bit					NOT NULL	default(0),
	[CoreAttributesId]		int					NULL,
	[CreatedDate]			datetime2(7)		NULL,
	[CurrentLocationId]		int					NULL,
	[CurrentSubLocationId]	INT					NULL, 
    CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Character_Player_PlayerId] FOREIGN KEY([PlayerId]) 
		REFERENCES [dbo].[Player] ([Id]),
	CONSTRAINT [FK_Character_Class_ClassId] FOREIGN KEY([ClassId]) 
		REFERENCES [dbo].[Class] ([Id]),
	CONSTRAINT [FK_Characters_Race_RaceId] FOREIGN KEY([RaceId]) 
		REFERENCES [dbo].[Race] ([Id]),
	CONSTRAINT [FK_Character_CoreAttributes_CoreAttributesId] FOREIGN KEY([CoreAttributesId]) 
		REFERENCES [dbo].[CoreAttributes] ([Id]),
	CONSTRAINT [FK_Character_Location_CurrentLocationId] FOREIGN KEY([CurrentLocationId]) 
		REFERENCES [dbo].[Location] ([Id]),
	CONSTRAINT [FK_Character_SubLocation_CurrentSubLocationId] FOREIGN KEY([CurrentSubLocationId]) 
		REFERENCES [dbo].[SubLocation] ([Id]),
	CONSTRAINT [FK_Character_CraftProfression_CraftProfessionId] FOREIGN KEY([CraftProfessionId]) 
		REFERENCES [dbo].[CraftProfession] ([Id]),
	CONSTRAINT [FK_Character_SubLocation_GatherProfessionId] FOREIGN KEY([GatherProfessionId]) 
		REFERENCES [dbo].[GatherProfession] ([Id])
)