﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Data.Enums
{
    public enum CreationStateEnum
    {
        Name,
        Race,
        Class,
        CraftProfession,
        GatherProfession,
        Confirmation,
    }
}
