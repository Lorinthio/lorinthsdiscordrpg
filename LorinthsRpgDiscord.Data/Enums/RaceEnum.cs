﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Data.Enums
{
    public enum RaceEnum
    {
        Human = 1,
        Elf = 2,
        Dwarf = 3
    }
}
