﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class QuestObjective
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int GoalTypeId { get; set; }
        public int TargetId { get; set; }
        public int Quantity { get; set; }

        public virtual QuestGoalType GoalType { get; set; }
    }
}
