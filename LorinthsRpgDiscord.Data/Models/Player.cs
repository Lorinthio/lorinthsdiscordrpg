﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Player
    {
        public Player()
        {
            Character = new HashSet<Character>();
            GameSessionPlayer = new HashSet<GameSessionPlayer>();
            PlayerRole = new HashSet<PlayerRole>();
            WebSession = new HashSet<WebSession>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string UserId { get; set; }
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [StringLength(50)]
        public string Avatar { get; set; }
        public Guid? CurrentCharacterId { get; set; }

        [ForeignKey(nameof(CurrentCharacterId))]
        [InverseProperty("PlayerNavigation")]
        public virtual Character CurrentCharacter { get; set; }
        [InverseProperty("Player")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("Player")]
        public virtual ICollection<GameSessionPlayer> GameSessionPlayer { get; set; }
        [InverseProperty("Player")]
        public virtual ICollection<PlayerRole> PlayerRole { get; set; }
        [InverseProperty("Player")]
        public virtual ICollection<WebSession> WebSession { get; set; }
    }
}
