﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CraftTool
    {
        [Key]
        public int ItemId { get; set; }
        public int CraftProfessionId { get; set; }
        public int SkillLevelRequired { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal CriticalChance { get; set; }

        [ForeignKey(nameof(CraftProfessionId))]
        [InverseProperty("CraftTool")]
        public virtual CraftProfession CraftProfession { get; set; }
        [ForeignKey(nameof(ItemId))]
        [InverseProperty("CraftTool")]
        public virtual Item Item { get; set; }
    }
}
