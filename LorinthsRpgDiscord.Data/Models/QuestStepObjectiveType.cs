﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class QuestStepObjectiveType
    {
        public QuestStepObjectiveType()
        {
            QuestStepObjective = new HashSet<QuestStepObjective>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }

        [InverseProperty("QuestStepObjectiveType")]
        public virtual ICollection<QuestStepObjective> QuestStepObjective { get; set; }
    }
}
