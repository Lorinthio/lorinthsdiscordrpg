﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class GatherProfession
    {
        public GatherProfession()
        {
            Character = new HashSet<Character>();
            GatherTool = new HashSet<GatherTool>();
            ResourceNode = new HashSet<ResourceNode>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; }

        [InverseProperty("GatherProfession")]
        public virtual GatheringProfessionStartingTool GatheringProfessionStartingTool { get; set; }
        [InverseProperty("GatherProfession")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("GatherProfession")]
        public virtual ICollection<GatherTool> GatherTool { get; set; }
        [InverseProperty("GatherProfession")]
        public virtual ICollection<ResourceNode> ResourceNode { get; set; }
    }
}
