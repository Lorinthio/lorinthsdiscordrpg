﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class PlayerRole
    {
        [Key]
        public int PlayerId { get; set; }
        [Key]
        [StringLength(20)]
        public string RoleId { get; set; }
        public bool IsOverride { get; set; }

        [ForeignKey(nameof(PlayerId))]
        [InverseProperty("PlayerRole")]
        public virtual Player Player { get; set; }
        [ForeignKey(nameof(RoleId))]
        [InverseProperty("PlayerRole")]
        public virtual Role Role { get; set; }
    }
}
