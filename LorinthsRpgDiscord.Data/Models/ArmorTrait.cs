﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class ArmorTrait
    {
        [Key]
        public int ArmorItemId { get; set; }
        public int TraitId { get; set; }

        [ForeignKey(nameof(ArmorItemId))]
        [InverseProperty(nameof(Armor.ArmorTrait))]
        public virtual Armor ArmorItem { get; set; }
        [ForeignKey(nameof(TraitId))]
        [InverseProperty("ArmorTrait")]
        public virtual Trait Trait { get; set; }
    }
}
