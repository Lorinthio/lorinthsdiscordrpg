﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Data.Models
{
    public class DiscordRpgThreadedContext : DiscordRpgContext
    {

        public DiscordRpgThreadedContext(IConfiguration config) : base(new DbContextOptionsBuilder<DiscordRpgContext>().UseSqlServer(config["DiscordRpgContext"])
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking).Options)
        {

        }

    }
}
