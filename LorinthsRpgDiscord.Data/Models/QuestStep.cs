﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class QuestStep
    {
        public QuestStep()
        {
            QuestStepObjective = new HashSet<QuestStepObjective>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        public int QuestId { get; set; }
        public int Order { get; set; }

        [ForeignKey(nameof(QuestId))]
        [InverseProperty("QuestStep")]
        public virtual Quest Quest { get; set; }
        [InverseProperty("QuestStep")]
        public virtual ICollection<QuestStepObjective> QuestStepObjective { get; set; }
    }
}
