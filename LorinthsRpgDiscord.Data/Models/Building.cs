﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Building
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int BuildingType { get; set; }
        public bool IsActive { get; set; }
    }
}
