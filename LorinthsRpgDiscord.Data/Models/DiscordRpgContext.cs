﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class DiscordRpgContext : DbContext
    {
        public DiscordRpgContext()
        {
        }

        public DiscordRpgContext(DbContextOptions<DiscordRpgContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Armor> Armor { get; set; }
        public virtual DbSet<ArmorSlot> ArmorSlot { get; set; }
        public virtual DbSet<ArmorTrait> ArmorTrait { get; set; }
        public virtual DbSet<Building> Building { get; set; }
        public virtual DbSet<BuildingType> BuildingType { get; set; }
        public virtual DbSet<Character> Character { get; set; }
        public virtual DbSet<CharacterEquipment> CharacterEquipment { get; set; }
        public virtual DbSet<CharacterGatherTime> CharacterGatherTime { get; set; }
        public virtual DbSet<CharacterInventory> CharacterInventory { get; set; }
        public virtual DbSet<CharacterTravelTime> CharacterTravelTime { get; set; }
        public virtual DbSet<Class> Class { get; set; }
        public virtual DbSet<ClassStartingEquipment> ClassStartingEquipment { get; set; }
        public virtual DbSet<Continent> Continent { get; set; }
        public virtual DbSet<CoreAttributes> CoreAttributes { get; set; }
        public virtual DbSet<CraftProfession> CraftProfession { get; set; }
        public virtual DbSet<CraftTool> CraftTool { get; set; }
        public virtual DbSet<GameSession> GameSession { get; set; }
        public virtual DbSet<GameSessionPlayer> GameSessionPlayer { get; set; }
        public virtual DbSet<GatherProfession> GatherProfession { get; set; }
        public virtual DbSet<GatherTool> GatherTool { get; set; }
        public virtual DbSet<GatheringProfessionStartingTool> GatheringProfessionStartingTool { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationBuildings> LocationBuildings { get; set; }
        public virtual DbSet<LocationDistance> LocationDistance { get; set; }
        public virtual DbSet<LocationType> LocationType { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<LootTable> LootTable { get; set; }
        public virtual DbSet<LootTableEntry> LootTableEntry { get; set; }
        public virtual DbSet<Player> Player { get; set; }
        public virtual DbSet<PlayerRole> PlayerRole { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<Quest> Quest { get; set; }
        public virtual DbSet<QuestStep> QuestStep { get; set; }
        public virtual DbSet<QuestStepObjective> QuestStepObjective { get; set; }
        public virtual DbSet<QuestStepObjectiveType> QuestStepObjectiveType { get; set; }
        public virtual DbSet<Race> Race { get; set; }
        public virtual DbSet<RaceClasses> RaceClasses { get; set; }
        public virtual DbSet<RaceStartingLocation> RaceStartingLocation { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<ResourceNode> ResourceNode { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RoleWebsitePermission> RoleWebsitePermission { get; set; }
        public virtual DbSet<SubLocation> SubLocation { get; set; }
        public virtual DbSet<Trait> Trait { get; set; }
        public virtual DbSet<Weapon> Weapon { get; set; }
        public virtual DbSet<WeaponTrait> WeaponTrait { get; set; }
        public virtual DbSet<WebSession> WebSession { get; set; }
        public virtual DbSet<WebsitePermission> WebsitePermission { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=DiscordRpg;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Armor>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK__Armor__727E838B4EF7C81C");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.HasOne(d => d.ArmorSlot)
                    .WithMany(p => p.Armor)
                    .HasForeignKey(d => d.ArmorSlotId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.Armor)
                    .HasForeignKey<Armor>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ArmorTrait>(entity =>
            {
                entity.HasKey(e => e.ArmorItemId)
                    .HasName("PK__ArmorTra__4BBB309E3E8FA9F4");

                entity.Property(e => e.ArmorItemId).ValueGeneratedNever();

                entity.HasOne(d => d.ArmorItem)
                    .WithOne(p => p.ArmorTrait)
                    .HasForeignKey<ArmorTrait>(d => d.ArmorItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArmorTrait_Armor_ArmorId");

                entity.HasOne(d => d.Trait)
                    .WithMany(p => p.ArmorTrait)
                    .HasForeignKey(d => d.TraitId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ClassId).HasDefaultValueSql("((1))");

                entity.Property(e => e.CraftProfessionId).HasDefaultValueSql("((1))");

                entity.Property(e => e.GatherProfessionId).HasDefaultValueSql("((1))");

                entity.Property(e => e.RaceId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Character)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CraftProfession)
                    .WithMany(p => p.Character)
                    .HasForeignKey(d => d.CraftProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Character_CraftProfression_CraftProfessionId");

                entity.HasOne(d => d.GatherProfession)
                    .WithMany(p => p.Character)
                    .HasForeignKey(d => d.GatherProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Character_SubLocation_GatherProfessionId");

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.Character)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Race)
                    .WithMany(p => p.Character)
                    .HasForeignKey(d => d.RaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Characters_Race_RaceId");
            });

            modelBuilder.Entity<CharacterEquipment>(entity =>
            {
                entity.HasIndex(e => e.CharacterId)
                    .HasName("UQ__Characte__757BC9A1CCF7F7A1")
                    .IsUnique();

                entity.HasOne(d => d.Character)
                    .WithOne(p => p.CharacterEquipment)
                    .HasForeignKey<CharacterEquipment>(d => d.CharacterId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CharacterGatherTime>(entity =>
            {
                entity.HasKey(e => e.CharacterId)
                    .HasName("PK__Characte__757BC9A069DD3F41");

                entity.Property(e => e.CharacterId).ValueGeneratedNever();

                entity.HasOne(d => d.Character)
                    .WithOne(p => p.CharacterGatherTime)
                    .HasForeignKey<CharacterGatherTime>(d => d.CharacterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CharacterGatherTimes_Character_CharacterId");
            });

            modelBuilder.Entity<CharacterInventory>(entity =>
            {
                entity.HasKey(e => new { e.CharacterId, e.ItemId });

                entity.HasOne(d => d.Character)
                    .WithMany(p => p.CharacterInventory)
                    .HasForeignKey(d => d.CharacterId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.CharacterInventory)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CharacterTravelTime>(entity =>
            {
                entity.HasKey(e => e.CharacterId)
                    .HasName("PK_CharacterTravelTimes");

                entity.Property(e => e.CharacterId).ValueGeneratedNever();

                entity.HasOne(d => d.Character)
                    .WithOne(p => p.CharacterTravelTime)
                    .HasForeignKey<CharacterTravelTime>(d => d.CharacterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CharacterTravelTimes_Character_CharacterId");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.CharacterTravelTime)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CharacterTravelTimes_Location_LocationId");
            });

            modelBuilder.Entity<Class>(entity =>
            {
                entity.Property(e => e.DefaultUnlocked).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<ClassStartingEquipment>(entity =>
            {
                entity.HasIndex(e => e.ClassId)
                    .HasName("UQ__ClassSta__CB1927C181252853")
                    .IsUnique();

                entity.HasOne(d => d.Class)
                    .WithOne(p => p.ClassStartingEquipment)
                    .HasForeignKey<ClassStartingEquipment>(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassStartingEquipment_Character_ClassId");
            });

            modelBuilder.Entity<Continent>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<CraftProfession>(entity =>
            {
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<CraftTool>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK__CraftToo__727E838B8BC955A1");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.Property(e => e.SkillLevelRequired).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CraftProfession)
                    .WithMany(p => p.CraftTool)
                    .HasForeignKey(d => d.CraftProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.CraftTool)
                    .HasForeignKey<CraftTool>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GameSession>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.EncounterName).IsUnicode(false);

                entity.Property(e => e.MapName).IsUnicode(false);
            });

            modelBuilder.Entity<GameSessionPlayer>(entity =>
            {
                entity.HasKey(e => new { e.GameSessionId, e.PlayerId });

                entity.HasOne(d => d.GameSession)
                    .WithMany(p => p.GameSessionPlayer)
                    .HasForeignKey(d => d.GameSessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.GameSessionPlayer)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GatherProfession>(entity =>
            {
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<GatherTool>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK__GatherTo__727E838B6A2C0196");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.Property(e => e.SkillLevelRequired).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.GatherProfession)
                    .WithMany(p => p.GatherTool)
                    .HasForeignKey(d => d.GatherProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.GatherTool)
                    .HasForeignKey<GatherTool>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GatheringProfessionStartingTool>(entity =>
            {
                entity.HasIndex(e => e.GatherProfessionId)
                    .HasName("UQ__Gatherin__4713729C2EE3489F")
                    .IsUnique();

                entity.HasOne(d => d.GatherProfession)
                    .WithOne(p => p.GatheringProfessionStartingTool)
                    .HasForeignKey<GatheringProfessionStartingTool>(d => d.GatherProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.StartingItem)
                    .WithMany(p => p.GatheringProfessionStartingTool)
                    .HasForeignKey(d => d.StartingItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('TODO')");

                entity.Property(e => e.MaxDurability).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Value).HasDefaultValueSql("((1.0))");

                entity.Property(e => e.Weight).HasDefaultValueSql("((1.0))");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.LocationType)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.LocationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Location_Location_RegionId");
            });

            modelBuilder.Entity<LocationBuildings>(entity =>
            {
                entity.HasNoKey();

                entity.HasOne(d => d.Building)
                    .WithMany()
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LocationBuildings_BuildingId");

                entity.HasOne(d => d.Location)
                    .WithMany()
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LocationBuildings_LocationId");
            });

            modelBuilder.Entity<LocationDistance>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.DistanceInMiles).HasDefaultValueSql("((1.0))");

                entity.HasOne(d => d.FirstLocation)
                    .WithMany()
                    .HasForeignKey(d => d.FirstLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LocationDistance_FirstLocationId");

                entity.HasOne(d => d.SecondLocation)
                    .WithMany()
                    .HasForeignKey(d => d.SecondLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LocationDistance_SecondLocationId");
            });

            modelBuilder.Entity<LocationType>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<LootTable>(entity =>
            {
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<LootTableEntry>(entity =>
            {
                entity.Property(e => e.Chance).HasDefaultValueSql("((100.0))");

                entity.HasOne(d => d.LootTable)
                    .WithMany(p => p.LootTableEntry)
                    .HasForeignKey(d => d.LootTableId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.Property(e => e.Avatar).IsUnicode(false);

                entity.Property(e => e.UserId).IsUnicode(false);

                entity.Property(e => e.Username).IsUnicode(false);

                entity.HasOne(d => d.CurrentCharacter)
                    .WithMany(p => p.PlayerNavigation)
                    .HasForeignKey(d => d.CurrentCharacterId)
                    .HasConstraintName("FK_Players_Character_CurrentCharacterId");
            });

            modelBuilder.Entity<PlayerRole>(entity =>
            {
                entity.HasKey(e => new { e.PlayerId, e.RoleId })
                    .HasName("PK_PlayerRoles");

                entity.Property(e => e.RoleId).IsUnicode(false);

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.PlayerRole)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerRoles_Player_PlayerId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.PlayerRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerRoles_Role_RoleId");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.Content).IsUnicode(false);

                entity.Property(e => e.CreatorId).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);
            });

            modelBuilder.Entity<QuestStep>(entity =>
            {
                entity.HasOne(d => d.Quest)
                    .WithMany(p => p.QuestStep)
                    .HasForeignKey(d => d.QuestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestStep_QuestId");
            });

            modelBuilder.Entity<QuestStepObjective>(entity =>
            {
                entity.HasOne(d => d.QuestStep)
                    .WithMany(p => p.QuestStepObjective)
                    .HasForeignKey(d => d.QuestStepId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Objective_QuestStepId");

                entity.HasOne(d => d.QuestStepObjectiveType)
                    .WithMany(p => p.QuestStepObjective)
                    .HasForeignKey(d => d.QuestStepObjectiveTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Objective_QuestStepObjectiveTypeId");
            });

            modelBuilder.Entity<QuestStepObjectiveType>(entity =>
            {
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Race>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.StartingCoreAttributes)
                    .WithMany(p => p.Race)
                    .HasForeignKey(d => d.StartingCoreAttributesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Race_CoreAttributes_StartingCoreAttributeId");
            });

            modelBuilder.Entity<RaceClasses>(entity =>
            {
                entity.HasOne(d => d.Class)
                    .WithMany(p => p.RaceClasses)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Race)
                    .WithMany(p => p.RaceClasses)
                    .HasForeignKey(d => d.RaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RaceStartingLocation>(entity =>
            {
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.RaceStartingLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Race)
                    .WithMany(p => p.RaceStartingLocation)
                    .HasForeignKey(d => d.RaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasOne(d => d.Continent)
                    .WithMany(p => p.Region)
                    .HasForeignKey(d => d.ContinentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ResourceNode>(entity =>
            {
                entity.HasIndex(e => e.SubLocationId)
                    .HasName("UQ__Resource__2D3D2E033E2D3F01")
                    .IsUnique();

                entity.Property(e => e.ToolLevelRequired).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.GatherProfession)
                    .WithMany(p => p.ResourceNode)
                    .HasForeignKey(d => d.GatherProfessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.LootTableCrit)
                    .WithMany(p => p.ResourceNodeLootTableCrit)
                    .HasForeignKey(d => d.LootTableCritId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.LootTable)
                    .WithMany(p => p.ResourceNodeLootTable)
                    .HasForeignKey(d => d.LootTableId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.SubLocation)
                    .WithOne(p => p.ResourceNode)
                    .HasForeignKey<ResourceNode>(d => d.SubLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<RoleWebsitePermission>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.RoleId).IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany()
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerRoleWebsitePermission_Role_RoleId");

                entity.HasOne(d => d.WebsitePermission)
                    .WithMany()
                    .HasForeignKey(d => d.WebsitePermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerRoleWebsitePermission_WebsitePermission_WebsitePermissionId");
            });

            modelBuilder.Entity<SubLocation>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.SubLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Trait>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Weapon>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK__Weapon__727E838B5967EDA5");

                entity.Property(e => e.ItemId).ValueGeneratedNever();

                entity.Property(e => e.ActionCost).HasDefaultValueSql("((2))");

                entity.Property(e => e.Power).HasDefaultValueSql("((1))");

                entity.Property(e => e.Reach).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Item)
                    .WithOne(p => p.Weapon)
                    .HasForeignKey<Weapon>(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<WeaponTrait>(entity =>
            {
                entity.HasKey(e => e.WeaponItemId)
                    .HasName("PK__WeaponTr__47B2F7D66CD9A525");

                entity.Property(e => e.WeaponItemId).ValueGeneratedNever();

                entity.HasOne(d => d.Trait)
                    .WithMany(p => p.WeaponTrait)
                    .HasForeignKey(d => d.TraitId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.WeaponItem)
                    .WithOne(p => p.WeaponTrait)
                    .HasForeignKey<WeaponTrait>(d => d.WeaponItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WeaponTrait_Weapon_WeaponId");
            });

            modelBuilder.Entity<WebSession>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IpAddress).IsUnicode(false);

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.WebSession)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<WebsitePermission>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
