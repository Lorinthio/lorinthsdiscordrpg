﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class GameSession
    {
        public GameSession()
        {
            GameSessionPlayer = new HashSet<GameSessionPlayer>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(100)]
        public string MapName { get; set; }
        [Required]
        [StringLength(100)]
        public string EncounterName { get; set; }

        [InverseProperty("GameSession")]
        public virtual ICollection<GameSessionPlayer> GameSessionPlayer { get; set; }
    }
}
