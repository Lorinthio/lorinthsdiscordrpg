﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CharacterInventory
    {
        [Key]
        public Guid CharacterId { get; set; }
        [Key]
        public int ItemId { get; set; }
        public int Amount { get; set; }

        [ForeignKey(nameof(CharacterId))]
        [InverseProperty("CharacterInventory")]
        public virtual Character Character { get; set; }
        [ForeignKey(nameof(ItemId))]
        [InverseProperty("CharacterInventory")]
        public virtual Item Item { get; set; }
    }
}
