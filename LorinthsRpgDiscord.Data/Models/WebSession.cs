﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class WebSession
    {
        [Key]
        public Guid Id { get; set; }
        public int PlayerId { get; set; }
        [Required]
        [StringLength(50)]
        public string IpAddress { get; set; }
        [Column(TypeName = "date")]
        public DateTime Expiration { get; set; }

        [ForeignKey(nameof(PlayerId))]
        [InverseProperty("WebSession")]
        public virtual Player Player { get; set; }
    }
}
