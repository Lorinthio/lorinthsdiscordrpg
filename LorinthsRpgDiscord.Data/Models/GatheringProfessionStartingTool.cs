﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class GatheringProfessionStartingTool
    {
        [Key]
        public int Id { get; set; }
        public int GatherProfessionId { get; set; }
        public int StartingItemId { get; set; }

        [ForeignKey(nameof(GatherProfessionId))]
        [InverseProperty("GatheringProfessionStartingTool")]
        public virtual GatherProfession GatherProfession { get; set; }
        [ForeignKey(nameof(StartingItemId))]
        [InverseProperty(nameof(Item.GatheringProfessionStartingTool))]
        public virtual Item StartingItem { get; set; }
    }
}
