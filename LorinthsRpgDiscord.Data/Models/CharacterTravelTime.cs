﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CharacterTravelTime
    {
        [Key]
        public Guid CharacterId { get; set; }
        public int LocationId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ArrivalTime { get; set; }

        [ForeignKey(nameof(CharacterId))]
        [InverseProperty("CharacterTravelTime")]
        public virtual Character Character { get; set; }
        [ForeignKey(nameof(LocationId))]
        [InverseProperty("CharacterTravelTime")]
        public virtual Location Location { get; set; }
    }
}
