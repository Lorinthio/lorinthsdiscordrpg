﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Class
    {
        public Class()
        {
            Character = new HashSet<Character>();
            RaceClasses = new HashSet<RaceClasses>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        [Required]
        public bool? DefaultUnlocked { get; set; }

        [InverseProperty("Class")]
        public virtual ClassStartingEquipment ClassStartingEquipment { get; set; }
        [InverseProperty("Class")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("Class")]
        public virtual ICollection<RaceClasses> RaceClasses { get; set; }
    }
}
