﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Race
    {
        public Race()
        {
            Character = new HashSet<Character>();
            RaceClasses = new HashSet<RaceClasses>();
            RaceStartingLocation = new HashSet<RaceStartingLocation>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int StartingCoreAttributesId { get; set; }
        public bool DefaultUnlocked { get; set; }

        [ForeignKey(nameof(StartingCoreAttributesId))]
        [InverseProperty(nameof(CoreAttributes.Race))]
        public virtual CoreAttributes StartingCoreAttributes { get; set; }
        [InverseProperty("Race")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("Race")]
        public virtual ICollection<RaceClasses> RaceClasses { get; set; }
        [InverseProperty("Race")]
        public virtual ICollection<RaceStartingLocation> RaceStartingLocation { get; set; }
    }
}
