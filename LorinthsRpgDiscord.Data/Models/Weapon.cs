﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Weapon
    {
        [Key]
        public int ItemId { get; set; }
        public int Power { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Parry { get; set; }
        public int Reach { get; set; }
        public int ActionCost { get; set; }

        [ForeignKey(nameof(ItemId))]
        [InverseProperty("Weapon")]
        public virtual Item Item { get; set; }
        [InverseProperty("WeaponItem")]
        public virtual WeaponTrait WeaponTrait { get; set; }
    }
}
