﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class LocationDistance
    {
        public int FirstLocationId { get; set; }
        public int SecondLocationId { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal? DistanceInMiles { get; set; }

        [ForeignKey(nameof(FirstLocationId))]
        public virtual Location FirstLocation { get; set; }
        [ForeignKey(nameof(SecondLocationId))]
        public virtual Location SecondLocation { get; set; }
    }
}
