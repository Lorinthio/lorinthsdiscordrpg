﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Character
    {
        public Character()
        {
            CharacterInventory = new HashSet<CharacterInventory>();
            PlayerNavigation = new HashSet<Player>();
        }

        [Key]
        public Guid Id { get; set; }
        public int PlayerId { get; set; }
        [StringLength(50)]
        public string CharacterName { get; set; }
        public string Bio { get; set; }
        public int RaceId { get; set; }
        public int ClassId { get; set; }
        public int CraftProfessionId { get; set; }
        public int GatherProfessionId { get; set; }
        public bool Created { get; set; }
        public int? CoreAttributesId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CurrentLocationId { get; set; }
        public int? CurrentSubLocationId { get; set; }

        [ForeignKey(nameof(ClassId))]
        [InverseProperty("Character")]
        public virtual Class Class { get; set; }
        [ForeignKey(nameof(CoreAttributesId))]
        [InverseProperty("Character")]
        public virtual CoreAttributes CoreAttributes { get; set; }
        [ForeignKey(nameof(CraftProfessionId))]
        [InverseProperty("Character")]
        public virtual CraftProfession CraftProfession { get; set; }
        [ForeignKey(nameof(CurrentLocationId))]
        [InverseProperty(nameof(Location.Character))]
        public virtual Location CurrentLocation { get; set; }
        [ForeignKey(nameof(CurrentSubLocationId))]
        [InverseProperty(nameof(SubLocation.Character))]
        public virtual SubLocation CurrentSubLocation { get; set; }
        [ForeignKey(nameof(GatherProfessionId))]
        [InverseProperty("Character")]
        public virtual GatherProfession GatherProfession { get; set; }
        [ForeignKey(nameof(PlayerId))]
        [InverseProperty("Character")]
        public virtual Player Player { get; set; }
        [ForeignKey(nameof(RaceId))]
        [InverseProperty("Character")]
        public virtual Race Race { get; set; }
        [InverseProperty("Character")]
        public virtual CharacterEquipment CharacterEquipment { get; set; }
        [InverseProperty("Character")]
        public virtual CharacterGatherTime CharacterGatherTime { get; set; }
        [InverseProperty("Character")]
        public virtual CharacterTravelTime CharacterTravelTime { get; set; }
        [InverseProperty("Character")]
        public virtual ICollection<CharacterInventory> CharacterInventory { get; set; }
        [InverseProperty("CurrentCharacter")]
        public virtual ICollection<Player> PlayerNavigation { get; set; }
    }
}
