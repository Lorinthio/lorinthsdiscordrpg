﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class RoleWebsitePermission
    {
        [Required]
        [StringLength(20)]
        public string RoleId { get; set; }
        public int WebsitePermissionId { get; set; }

        [ForeignKey(nameof(RoleId))]
        public virtual Role Role { get; set; }
        [ForeignKey(nameof(WebsitePermissionId))]
        public virtual WebsitePermission WebsitePermission { get; set; }
    }
}
