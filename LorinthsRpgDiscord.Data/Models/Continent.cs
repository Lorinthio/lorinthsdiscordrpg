﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Continent
    {
        public Continent()
        {
            Region = new HashSet<Region>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [InverseProperty("Continent")]
        public virtual ICollection<Region> Region { get; set; }
    }
}
