﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class LocationBuildings
    {
        public int LocationId { get; set; }
        public int BuildingId { get; set; }

        [ForeignKey(nameof(BuildingId))]
        public virtual Building Building { get; set; }
        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }
    }
}
