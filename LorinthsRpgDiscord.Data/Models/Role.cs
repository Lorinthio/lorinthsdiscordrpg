﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Role
    {
        public Role()
        {
            PlayerRole = new HashSet<PlayerRole>();
        }

        [Key]
        [StringLength(20)]
        public string Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<PlayerRole> PlayerRole { get; set; }
    }
}
