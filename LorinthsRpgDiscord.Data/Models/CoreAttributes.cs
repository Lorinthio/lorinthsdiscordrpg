﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CoreAttributes
    {
        public CoreAttributes()
        {
            Character = new HashSet<Character>();
            Race = new HashSet<Race>();
        }

        [Key]
        public int Id { get; set; }
        public int Strength { get; set; }
        public int Endurance { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Charisma { get; set; }

        [InverseProperty("CoreAttributes")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("StartingCoreAttributes")]
        public virtual ICollection<Race> Race { get; set; }
    }
}
