﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class PointOfInterestType
    {
        public PointOfInterestType()
        {
            Location = new HashSet<Location>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Location> Location { get; set; }
    }
}
