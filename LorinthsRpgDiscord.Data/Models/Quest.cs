﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Quest
    {
        public Quest()
        {
            QuestStep = new HashSet<QuestStep>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int? LevelRequired { get; set; }
        public int? ClassIdRequired { get; set; }
        public int? RaceIdRequired { get; set; }

        [InverseProperty("Quest")]
        public virtual ICollection<QuestStep> QuestStep { get; set; }
    }
}
