﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class QuestStepObjective
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        public int QuestStepObjectiveTypeId { get; set; }
        public int TargetId { get; set; }
        public int Quantity { get; set; }
        public int QuestStepId { get; set; }
        public int Order { get; set; }

        [ForeignKey(nameof(QuestStepId))]
        [InverseProperty("QuestStepObjective")]
        public virtual QuestStep QuestStep { get; set; }
        [ForeignKey(nameof(QuestStepObjectiveTypeId))]
        [InverseProperty("QuestStepObjective")]
        public virtual QuestStepObjectiveType QuestStepObjectiveType { get; set; }
    }
}
