﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class LootTableEntry
    {
        [Key]
        public int Id { get; set; }
        public int LootTableId { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Chance { get; set; }
        public int? TargetItemId { get; set; }
        public int? TargetItemAmount { get; set; }

        [ForeignKey(nameof(LootTableId))]
        [InverseProperty("LootTableEntry")]
        public virtual LootTable LootTable { get; set; }
        [ForeignKey(nameof(TargetItemId))]
        [InverseProperty(nameof(Item.LootTableEntry))]
        public virtual Item TargetItem { get; set; }
    }
}
