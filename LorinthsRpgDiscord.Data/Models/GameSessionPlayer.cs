﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class GameSessionPlayer
    {
        [Key]
        public Guid GameSessionId { get; set; }
        [Key]
        public int PlayerId { get; set; }

        [ForeignKey(nameof(GameSessionId))]
        [InverseProperty("GameSessionPlayer")]
        public virtual GameSession GameSession { get; set; }
        [ForeignKey(nameof(PlayerId))]
        [InverseProperty("GameSessionPlayer")]
        public virtual Player Player { get; set; }
    }
}
