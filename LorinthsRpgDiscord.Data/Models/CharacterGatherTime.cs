﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CharacterGatherTime
    {
        [Key]
        public Guid CharacterId { get; set; }
        public int ResourceNodeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FinishTime { get; set; }

        [ForeignKey(nameof(CharacterId))]
        [InverseProperty("CharacterGatherTime")]
        public virtual Character Character { get; set; }
    }
}
