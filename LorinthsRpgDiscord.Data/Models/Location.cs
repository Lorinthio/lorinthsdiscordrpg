﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Location
    {
        public Location()
        {
            Character = new HashSet<Character>();
            CharacterTravelTime = new HashSet<CharacterTravelTime>();
            RaceStartingLocation = new HashSet<RaceStartingLocation>();
            SubLocation = new HashSet<SubLocation>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public int LocationTypeId { get; set; }
        public int RegionId { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [ForeignKey(nameof(LocationTypeId))]
        [InverseProperty("Location")]
        public virtual LocationType LocationType { get; set; }
        [ForeignKey(nameof(RegionId))]
        [InverseProperty("Location")]
        public virtual Region Region { get; set; }
        [InverseProperty("CurrentLocation")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("Location")]
        public virtual ICollection<CharacterTravelTime> CharacterTravelTime { get; set; }
        [InverseProperty("Location")]
        public virtual ICollection<RaceStartingLocation> RaceStartingLocation { get; set; }
        [InverseProperty("Location")]
        public virtual ICollection<SubLocation> SubLocation { get; set; }
    }
}
