﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class SubLocation
    {
        public SubLocation()
        {
            Character = new HashSet<Character>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public int LocationId { get; set; }
        public bool IsStartingSubLocation { get; set; }
        public bool CanTravel { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [ForeignKey(nameof(LocationId))]
        [InverseProperty("SubLocation")]
        public virtual Location Location { get; set; }
        [InverseProperty("SubLocation")]
        public virtual ResourceNode ResourceNode { get; set; }
        [InverseProperty("CurrentSubLocation")]
        public virtual ICollection<Character> Character { get; set; }
    }
}
