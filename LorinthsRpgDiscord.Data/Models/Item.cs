﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Item
    {
        public Item()
        {
            CharacterEquipmentBodyItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentFeetItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentHelmetItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentLeftHandItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentNeckItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentRightHandItem = new HashSet<CharacterEquipment>();
            CharacterEquipmentRing1Item = new HashSet<CharacterEquipment>();
            CharacterEquipmentRing2Item = new HashSet<CharacterEquipment>();
            CharacterInventory = new HashSet<CharacterInventory>();
            ClassStartingEquipmentBodyItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentFeetItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentHelmetItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentLeftHandItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentNeckItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentRightHandItem = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentRing1Item = new HashSet<ClassStartingEquipment>();
            ClassStartingEquipmentRing2Item = new HashSet<ClassStartingEquipment>();
            GatheringProfessionStartingTool = new HashSet<GatheringProfessionStartingTool>();
            LootTableEntry = new HashSet<LootTableEntry>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Value { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Weight { get; set; }
        public int MaxDurability { get; set; }

        [InverseProperty("Item")]
        public virtual Armor Armor { get; set; }
        [InverseProperty("Item")]
        public virtual CraftTool CraftTool { get; set; }
        [InverseProperty("Item")]
        public virtual GatherTool GatherTool { get; set; }
        [InverseProperty("Item")]
        public virtual Weapon Weapon { get; set; }
        [InverseProperty(nameof(CharacterEquipment.BodyItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentBodyItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.FeetItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentFeetItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.HelmetItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentHelmetItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.LeftHandItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentLeftHandItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.NeckItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentNeckItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.RightHandItem))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentRightHandItem { get; set; }
        [InverseProperty(nameof(CharacterEquipment.Ring1Item))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentRing1Item { get; set; }
        [InverseProperty(nameof(CharacterEquipment.Ring2Item))]
        public virtual ICollection<CharacterEquipment> CharacterEquipmentRing2Item { get; set; }
        [InverseProperty("Item")]
        public virtual ICollection<CharacterInventory> CharacterInventory { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.BodyItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentBodyItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.FeetItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentFeetItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.HelmetItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentHelmetItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.LeftHandItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentLeftHandItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.NeckItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentNeckItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.RightHandItem))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentRightHandItem { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.Ring1Item))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentRing1Item { get; set; }
        [InverseProperty(nameof(ClassStartingEquipment.Ring2Item))]
        public virtual ICollection<ClassStartingEquipment> ClassStartingEquipmentRing2Item { get; set; }
        [InverseProperty("StartingItem")]
        public virtual ICollection<GatheringProfessionStartingTool> GatheringProfessionStartingTool { get; set; }
        [InverseProperty("TargetItem")]
        public virtual ICollection<LootTableEntry> LootTableEntry { get; set; }
    }
}
