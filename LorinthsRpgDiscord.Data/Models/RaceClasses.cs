﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class RaceClasses
    {
        [Key]
        public int Id { get; set; }
        public int RaceId { get; set; }
        public int ClassId { get; set; }

        [ForeignKey(nameof(ClassId))]
        [InverseProperty("RaceClasses")]
        public virtual Class Class { get; set; }
        [ForeignKey(nameof(RaceId))]
        [InverseProperty("RaceClasses")]
        public virtual Race Race { get; set; }
    }
}
