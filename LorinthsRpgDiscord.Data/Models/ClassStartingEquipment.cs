﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class ClassStartingEquipment
    {
        [Key]
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int? LeftHandItemId { get; set; }
        public int? RightHandItemId { get; set; }
        public int? HelmetItemId { get; set; }
        public int? BodyItemId { get; set; }
        public int? FeetItemId { get; set; }
        public int? Ring1ItemId { get; set; }
        public int? Ring2ItemId { get; set; }
        public int? NeckItemId { get; set; }

        [ForeignKey(nameof(BodyItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentBodyItem))]
        public virtual Item BodyItem { get; set; }
        [ForeignKey(nameof(ClassId))]
        [InverseProperty("ClassStartingEquipment")]
        public virtual Class Class { get; set; }
        [ForeignKey(nameof(FeetItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentFeetItem))]
        public virtual Item FeetItem { get; set; }
        [ForeignKey(nameof(HelmetItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentHelmetItem))]
        public virtual Item HelmetItem { get; set; }
        [ForeignKey(nameof(LeftHandItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentLeftHandItem))]
        public virtual Item LeftHandItem { get; set; }
        [ForeignKey(nameof(NeckItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentNeckItem))]
        public virtual Item NeckItem { get; set; }
        [ForeignKey(nameof(RightHandItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentRightHandItem))]
        public virtual Item RightHandItem { get; set; }
        [ForeignKey(nameof(Ring1ItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentRing1Item))]
        public virtual Item Ring1Item { get; set; }
        [ForeignKey(nameof(Ring2ItemId))]
        [InverseProperty(nameof(Item.ClassStartingEquipmentRing2Item))]
        public virtual Item Ring2Item { get; set; }
    }
}
