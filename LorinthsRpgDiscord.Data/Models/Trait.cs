﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Trait
    {
        public Trait()
        {
            ArmorTrait = new HashSet<ArmorTrait>();
            WeaponTrait = new HashSet<WeaponTrait>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [InverseProperty("Trait")]
        public virtual ICollection<ArmorTrait> ArmorTrait { get; set; }
        [InverseProperty("Trait")]
        public virtual ICollection<WeaponTrait> WeaponTrait { get; set; }
    }
}
