﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Armor
    {
        [Key]
        public int ItemId { get; set; }
        public int Defense { get; set; }
        public int MagicDefense { get; set; }
        public int Dodge { get; set; }
        public int ArmorSlotId { get; set; }

        [ForeignKey(nameof(ArmorSlotId))]
        [InverseProperty("Armor")]
        public virtual ArmorSlot ArmorSlot { get; set; }
        [ForeignKey(nameof(ItemId))]
        [InverseProperty("Armor")]
        public virtual Item Item { get; set; }
        [InverseProperty("ArmorItem")]
        public virtual ArmorTrait ArmorTrait { get; set; }
    }
}
