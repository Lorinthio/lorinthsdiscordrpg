﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Region
    {
        public Region()
        {
            Location = new HashSet<Location>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        public int ContinentId { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [ForeignKey(nameof(ContinentId))]
        [InverseProperty("Region")]
        public virtual Continent Continent { get; set; }
        [InverseProperty("Region")]
        public virtual ICollection<Location> Location { get; set; }
    }
}
