﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class ResourceNode
    {
        [Key]
        public int Id { get; set; }
        public int SubLocationId { get; set; }
        public int GatherProfessionId { get; set; }
        public int ToolLevelRequired { get; set; }
        public int LootTableId { get; set; }
        public int LootTableCritId { get; set; }

        [ForeignKey(nameof(GatherProfessionId))]
        [InverseProperty("ResourceNode")]
        public virtual GatherProfession GatherProfession { get; set; }
        [ForeignKey(nameof(LootTableId))]
        [InverseProperty("ResourceNodeLootTable")]
        public virtual LootTable LootTable { get; set; }
        [ForeignKey(nameof(LootTableCritId))]
        [InverseProperty("ResourceNodeLootTableCrit")]
        public virtual LootTable LootTableCrit { get; set; }
        [ForeignKey(nameof(SubLocationId))]
        [InverseProperty("ResourceNode")]
        public virtual SubLocation SubLocation { get; set; }
    }
}
