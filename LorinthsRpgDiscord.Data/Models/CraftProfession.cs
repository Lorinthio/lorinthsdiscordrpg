﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CraftProfession
    {
        public CraftProfession()
        {
            Character = new HashSet<Character>();
            CraftTool = new HashSet<CraftTool>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; }

        [InverseProperty("CraftProfession")]
        public virtual ICollection<Character> Character { get; set; }
        [InverseProperty("CraftProfession")]
        public virtual ICollection<CraftTool> CraftTool { get; set; }
    }
}
