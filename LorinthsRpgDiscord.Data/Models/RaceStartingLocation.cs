﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class RaceStartingLocation
    {
        [Key]
        public int Id { get; set; }
        public int RaceId { get; set; }
        public int LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        [InverseProperty("RaceStartingLocation")]
        public virtual Location Location { get; set; }
        [ForeignKey(nameof(RaceId))]
        [InverseProperty("RaceStartingLocation")]
        public virtual Race Race { get; set; }
    }
}
