﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class StepObjective
    {
        public int StepId { get; set; }
        public int ObjectiveId { get; set; }
        public int Order { get; set; }

        public virtual Objective Objective { get; set; }
        public virtual Step Step { get; set; }
    }
}
