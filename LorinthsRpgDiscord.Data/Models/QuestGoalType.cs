﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class QuestGoalType
    {
        public QuestGoalType()
        {
            QuestStepObjective = new HashSet<QuestStepObjective>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<QuestStepObjective> QuestStepObjective { get; set; }
    }
}
