﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class Step
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
