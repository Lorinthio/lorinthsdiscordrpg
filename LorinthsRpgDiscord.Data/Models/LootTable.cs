﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class LootTable
    {
        public LootTable()
        {
            LootTableEntry = new HashSet<LootTableEntry>();
            ResourceNodeLootTable = new HashSet<ResourceNode>();
            ResourceNodeLootTableCrit = new HashSet<ResourceNode>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [InverseProperty("LootTable")]
        public virtual ICollection<LootTableEntry> LootTableEntry { get; set; }
        [InverseProperty(nameof(ResourceNode.LootTable))]
        public virtual ICollection<ResourceNode> ResourceNodeLootTable { get; set; }
        [InverseProperty(nameof(ResourceNode.LootTableCrit))]
        public virtual ICollection<ResourceNode> ResourceNodeLootTableCrit { get; set; }
    }
}
