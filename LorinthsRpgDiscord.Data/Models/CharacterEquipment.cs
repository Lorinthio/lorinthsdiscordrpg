﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class CharacterEquipment
    {
        [Key]
        public int Id { get; set; }
        public Guid CharacterId { get; set; }
        public int? LeftHandItemId { get; set; }
        public int? RightHandItemId { get; set; }
        public int? HelmetItemId { get; set; }
        public int? BodyItemId { get; set; }
        public int? FeetItemId { get; set; }
        public int? Ring1ItemId { get; set; }
        public int? Ring2ItemId { get; set; }
        public int? NeckItemId { get; set; }

        [ForeignKey(nameof(BodyItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentBodyItem))]
        public virtual Item BodyItem { get; set; }
        [ForeignKey(nameof(CharacterId))]
        [InverseProperty("CharacterEquipment")]
        public virtual Character Character { get; set; }
        [ForeignKey(nameof(FeetItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentFeetItem))]
        public virtual Item FeetItem { get; set; }
        [ForeignKey(nameof(HelmetItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentHelmetItem))]
        public virtual Item HelmetItem { get; set; }
        [ForeignKey(nameof(LeftHandItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentLeftHandItem))]
        public virtual Item LeftHandItem { get; set; }
        [ForeignKey(nameof(NeckItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentNeckItem))]
        public virtual Item NeckItem { get; set; }
        [ForeignKey(nameof(RightHandItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentRightHandItem))]
        public virtual Item RightHandItem { get; set; }
        [ForeignKey(nameof(Ring1ItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentRing1Item))]
        public virtual Item Ring1Item { get; set; }
        [ForeignKey(nameof(Ring2ItemId))]
        [InverseProperty(nameof(Item.CharacterEquipmentRing2Item))]
        public virtual Item Ring2Item { get; set; }
    }
}
