﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class GatherTool
    {
        [Key]
        public int ItemId { get; set; }
        public int GatherProfessionId { get; set; }
        public int SkillLevelRequired { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal CriticalChance { get; set; }

        [ForeignKey(nameof(GatherProfessionId))]
        [InverseProperty("GatherTool")]
        public virtual GatherProfession GatherProfession { get; set; }
        [ForeignKey(nameof(ItemId))]
        [InverseProperty("GatherTool")]
        public virtual Item Item { get; set; }
    }
}
