﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class WeaponTrait
    {
        [Key]
        public int WeaponItemId { get; set; }
        public int TraitId { get; set; }

        [ForeignKey(nameof(TraitId))]
        [InverseProperty("WeaponTrait")]
        public virtual Trait Trait { get; set; }
        [ForeignKey(nameof(WeaponItemId))]
        [InverseProperty(nameof(Weapon.WeaponTrait))]
        public virtual Weapon WeaponItem { get; set; }
    }
}
