﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LorinthsRpgDiscord.Data.Models
{
    public partial class ArmorSlot
    {
        public ArmorSlot()
        {
            Armor = new HashSet<Armor>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [InverseProperty("ArmorSlot")]
        public virtual ICollection<Armor> Armor { get; set; }
    }
}
