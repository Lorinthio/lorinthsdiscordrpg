﻿using LorinthsRpgDiscord.Data.Interfaces;
using LorinthsRpgDiscord.Data.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Data
{
    public class CachedRepository<T> : ICachedRepository<T>
    {
        private MemoryCache _memoryCache;
        public CachedRepository()
        {
            _memoryCache = new MemoryCache(new MemoryCacheOptions());
        }

        public async Task<T> Get(object[] keyValues)
        {
            var key = keyValues.Aggregate((a, b) => a + "-" + b);
            if(!_memoryCache.TryGetValue(key, out T result))
            {
                using(var context = new DiscordRpgContext())
                {
                    result = (T) await context.FindAsync(typeof(T), keyValues);
                }

                _memoryCache.Set(key, result);
            }
            return result; 
        }
    }
}
