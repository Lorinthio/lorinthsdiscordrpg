﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LorinthsRpgDiscord.Data.Interfaces
{
    public interface ICachedRepository<T>
    {
        Task<T> Get(object[] keyValues);
    }
}
