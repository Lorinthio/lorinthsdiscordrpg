﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsRpgDiscord.Game
{
    public class GameSessionManager
    {
        public List<GameSession> GameSessions { get; set; }

        public GameSessionManager()
        {
            GameSessions = new List<GameSession>();
        }

        public GameSession Create()
        {
            var session = new GameSession
            {
                SessionId = Guid.NewGuid()
            };
            GameSessions.Add(session);

            return session;
        }
    }
}
