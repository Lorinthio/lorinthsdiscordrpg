﻿using System;
using System.Collections.Generic;

namespace LorinthsRpgDiscord.Game
{
    public class GameSession
    {
        public Guid SessionId { get; set; }
        public List<int> PlayerIds { get; set; }
    }
}
